<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('client_companies')->insert([
            'company_name' => 'Bank Of Ceylon',
            'mother_Company' => 'Peoples Bank',
            'company_type' => 'Financial',
            'owner_full_name' => 'Mahinda Rajapaksha',
            'director_name' => 'Namal Rajapaksha',
            'occupation' => 'Owner',
            'no' => '1',
            'street_1' => 'Main St',
            'street_2' => 'Second Floor',
            'city' => 'Colombo',
            'contact_1' => '0714578458',
            'contact_2' => '0114587956',
            'email' => 'boc@gmail.com',
            'br_number' => 'CD1000001',
            'web_url' => 'https://laravel.com/docs/9.x/seeding#writing-seeders',
            'is_active' => '1',
            'office_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('client_companies')->insert([
            'company_name' => 'IOC',
            'mother_Company' => 'Indian Oil Mill',
            'company_type' => 'Fuel',
            'owner_full_name' => 'Mahinda Rajapaksha',
            'director_name' => 'Namal Rajapaksha',
            'occupation' => 'Owner',
            'no' => '1',
            'street_1' => 'Main St',
            'street_2' => 'Second Floor',
            'city' => 'Colombo',
            'contact_1' => '0334578458',
            'contact_2' => '0174587956',
            'email' => 'ioc@gmail.com',
            'br_number' => 'CD10001221',
            'web_url' => 'https://laravel.com/docs/9.x/seeding#writing-seeders',
            'is_active' => '1',
            'office_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
