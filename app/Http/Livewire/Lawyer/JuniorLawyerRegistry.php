<?php

namespace App\Http\Livewire\Lawyer;

use App\Models\JuniorLawyer;
use App\Models\LawyerCategory;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class JuniorLawyerRegistry extends Component
{
    public $selected_junior_lawyer_id;
    public $fname;
    public $mname;
    public $lname;
    public $surname;
    public $license_no;
    public $lawyer_type;
    public $nic;
    public $gender;
    public $marriage_status;
    public $email;
    public $contact1;
    public $contact2;
    public $no;
    public $street1;
    public $street2;
    public $city;
    public $image;
    public $url;
    public $j_lw_search_key = '';
    use WithPagination;
    use WithFileUploads;

    protected $paginationTheme = 'bootstrap';

// startSaveJuniorLawyer
    public function JuniorLawyerInsert()
    {

        $this->validate([
//            'office'=> 'required',
            'image' => 'image|mimes:png,jpg,pdf|max:1024', // 1MB Max
        ]);

        $this->url =  Storage::disk('public')->put( 'uploads/user', $this->image);

        $lawyer_category = LawyerCategory::where('category_name', $this->lawyer_type)->first();
        $junior_lawyer = new JuniorLawyer();
        $junior_lawyer->f_name = $this->fname;
        $junior_lawyer->m_name = $this->mname;
        $junior_lawyer->l_name = $this->lname;
        $junior_lawyer->surname = $this->surname;
        $junior_lawyer->nic = $this->nic;
        $junior_lawyer->license_no = $this->license_no;
        $junior_lawyer->contact_1 = $this->contact1;
        $junior_lawyer->contact_2 = $this->contact2;
        $junior_lawyer->email = $this->email;
        $junior_lawyer->no = $this->no;
        $junior_lawyer->street_1 = $this->street1;
        $junior_lawyer->street_2 = $this->street2;
        $junior_lawyer->city = $this->city;
        $junior_lawyer->gender = $this->gender;
        $junior_lawyer->marriage_status = $this->marriage_status;
        $junior_lawyer->image = $this->url;
        $junior_lawyer->category_id = $lawyer_category['id'];
        $junior_lawyer->user_role_id = 2;
        $junior_lawyer->save();
        $this->ClearJuniorLawyerDataFields();
    }// endSaveJuniorLawyer

// startLoadJuniorLawyerData
    public function loadJuniorLawyerData($selected_junior_lawyer)
    {
        $lawyer_category = LawyerCategory::find($selected_junior_lawyer['category_id']);

        $this->selected_junior_lawyer_id = $selected_junior_lawyer['id'];
        $this->fname = $selected_junior_lawyer['f_name'];
        $this->mname = $selected_junior_lawyer['m_name'];
        $this->lname = $selected_junior_lawyer['l_name'];
        $this->surname = $selected_junior_lawyer['surname'];
        $this->nic = $selected_junior_lawyer['nic'];
        $this->license_no = $selected_junior_lawyer['license_no'];
        $this->contact1 = $selected_junior_lawyer['contact_1'];
        $this->contact2 = $selected_junior_lawyer['contact_2'];
        $this->email = $selected_junior_lawyer['email'];
        $this->no = $selected_junior_lawyer['no'];
        $this->street1 = $selected_junior_lawyer['street_1'];
        $this->street2 = $selected_junior_lawyer['street_2'];
        $this->city = $selected_junior_lawyer['city'];
        $this->gender = $selected_junior_lawyer['gender'];
        $this->marriage_status = $selected_junior_lawyer['marriage_status'];
        $this->image = $selected_junior_lawyer['image'];
        $this->lawyer_type = $lawyer_category['category_name'];
    }// endLoadJuniorLawyerData

// startJuniorLawyerUpdate
    public function JuniorLawyerUpdate()
    {
        $selected_lawyer = JuniorLawyer::find($this->selected_junior_lawyer_id);
        $lawyer_category = LawyerCategory::find($selected_lawyer['category_id']);

        $selected_lawyer->f_name = $this->fname;
        $selected_lawyer->m_name = $this->mname;
        $selected_lawyer->l_name = $this->lname;
        $selected_lawyer->surname = $this->surname;
        $selected_lawyer->nic = $this->nic;
        $selected_lawyer->license_no = $this->license_no;
        $selected_lawyer->contact_1 = $this->contact1;
        $selected_lawyer->contact_2 = $this->contact2;
        $selected_lawyer->email = $this->email;
        $selected_lawyer->no = $this->no;
        $selected_lawyer->street_1 = $this->street1;
        $selected_lawyer->street_2 = $this->street2;
        $selected_lawyer->city = $this->city;
        $selected_lawyer->gender = $this->gender;
        $selected_lawyer->marriage_status = $this->marriage_status;
        $selected_lawyer->image = 'image path';
        $selected_lawyer->category_id = $lawyer_category->id;
        $selected_lawyer->save();
        $this->ClearJuniorLawyerDataFields();
    }// endJuniorLawyerUpdate

// startJuniorLawyerActive
    public function JuniorLawyersActive($selected_junior_lawyer)
    {
//        dd($selected_junior_lawyer);
        $selected_lawyer = JuniorLawyer::find($selected_junior_lawyer);
        $selected_lawyer->is_active = '1';
        $selected_lawyer->save();
        $this->ClearJuniorLawyerDataFields();
    }// endJuniorLawyerActive

// startJuniorLawyerDeactivate
    public function JuniorLawyersDelete($selected_junior_lawyer)
    {
        $selected_lawyer = JuniorLawyer::find($selected_junior_lawyer);
        $selected_lawyer->is_active = '0';
        $selected_lawyer->save();
        $this->ClearJuniorLawyerDataFields();
    }// endJuniorLawyerDeactivate

// startClearFields
    public function ClearJuniorLawyerDataFields()
    {
        $this->fname = '';
        $this->mname = '';
        $this->lname = '';
        $this->surname = '';
        $this->license_no = '';
        $this->lawyer_type = '';
        $this->nic = '';
        $this->gender = '';
        $this->marriage_status = '';
        $this->email = '';
        $this->contact1 = '';
        $this->contact2 = '';
        $this->no = '';
        $this->street1 = '';
        $this->street2 = '';
        $this->city = '';
    }// endClearFields

// startRenderMethod
    public function render()
    {
        $j_lawyers = JuniorLawyer::where('id', 'like', '%' . $this->j_lw_search_key . '%')
            ->orwhere('f_name', 'like', '%' . $this->j_lw_search_key . '%')
            ->orwhere('license_no', 'like', '%' . $this->j_lw_search_key . '%')->paginate(5);
        $lawyer_categories = LawyerCategory::all();
        return view('livewire.lawyer.junior-lawyer-registry', ['lawyer_categories' => $lawyer_categories, 'j_lawyers' => $j_lawyers])->layout('layouts.main');
    }//    endRenderMethod
}
