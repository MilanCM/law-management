<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
        <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Law Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-item menu" data-accordion="true">
                    <a href="{{route('home')}}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('client/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-user-alt"></i>
                        <p>
                            Client Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('client.client_registry')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Client-Person</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('client.client_company')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Client-Company</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('client.client_view')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>View Client</p>
                            </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('user/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('user.emp_management')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Manage Employee</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('user.manage_account')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Employee Account</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('user.user_view')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>junior Lawyer Account</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('lawcase/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-balance-scale"></i>
                        <p>Case Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('lawcase.lawcase_registry')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Case Registry</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawcase.lawcase_upgrade')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Case Upgrade</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawcase.lawcase_view')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Case View</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawcase.manage_attachment')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Manage Attachment</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawcase.manage_category')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Manage Category</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('lawyer/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active ">
                        <i class="nav-icon fas fa-user-graduate"></i>
                        <p>
                            Lawyer Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('lawyer.senior_lawyer_registry')}}" class="nav-link ">
                                <i class="far fa-star"></i>
                                <p>Senior Lawyer Registry</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawyer.junior_lawyer_registry')}}" class="nav-link">
                                <i class="far fa-star"></i>
                                <p>Junior Lawyer Registry</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('lawyer.lawyer_office_registry')}}" class="nav-link">
                                <i class="far fa-star"></i>
                                <p>Law Office Registry</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('appointment/*')  ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active ">
                        <i class="nav-icon fas fa-calendar-check"></i>
                        <p>Appointment Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('appointment.task_management')}}" class="nav-link ">
                                <i class="far fa-star"></i>
                                <p>Task Management</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('appointment.appointment_management')}}" class="nav-link">
                                <i class="far fa-star"></i>
                                <p>Appointment Management</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('document/*')  ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active ">
                        <i class="nav-icon fas fa-file-alt"></i>
                        <p>Document Management<i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('document.template_document')}}" class="nav-link ">
                                <i class="far fa-star"></i>
                                <p>Template Document</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-star"></i>
                                <p>Document 2</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('utilities/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active ">
                        <i class="nav-icon fas fa-folder-open"></i>
                        <p>
                            Utilities Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview ">
                        <li class="nav-item opca ">
                            <a href="{{route('utilities.law_fields')}}"
                               class="nav-link {{\Illuminate\Support\Facades\Request::is('stock/grn') ? 'active opacity:.5' : ''}}">
                                <i class="far fa-star"></i>
                                <p>Law Fields</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('utilities.book_resource')}}"
                               class="nav-link {{\Illuminate\Support\Facades\Request::is('stock/stock_records') ? 'active' : ''}}">
                                <i class="far fa-star"></i>
                                <p>Book Resource</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('utilities.web_resource')}}"
                               class="nav-link {{\Illuminate\Support\Facades\Request::is('stock/stock_records') ? 'active' : ''}}">
                                <i class="far fa-star"></i>
                                <p>Web Resource</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('settings/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-file-code"></i>
                        <p>
                            Settings Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('settings.privileges_management')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Privileges Management</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Settings 2</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Settings 3</p>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item menu {{\Illuminate\Support\Facades\Request::is('account/*') ? 'menu-is-opening menu-open' :'' }}">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-user-edit"></i>
                        <p>
                            Account Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{route('account.client_payment')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Client Payment</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('account.expenses_management')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Expenses Management</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('account.salary_management')}}" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Salary Management</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-star nav-icon"></i>
                                <p>Taxes</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item menu">
                    <a href="{{route('chat.chat')}}" class="nav-link active">
                        <i class="nav-icon fas fa-comments"></i>
                        <p>
                            Chat
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
