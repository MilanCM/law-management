<?php

namespace App\Http\Livewire\Lawcase;

use App\Http\Controllers\UploadController;
use Illuminate\Http\Request;
use Livewire\Component;
use Livewire\WithFileUploads;

class ManageAttachment extends Component
{
    use WithFileUploads;
    public $avatar;

    public function submitProfileUpdate()
    {
        $data = $this->validate([
//            'first_name' => 'required',
//            'last_name' => 'required',
            'avatar' => 'nullable|image|max:2048', // 2MB Max
        ]);

        $image = $this->avatar;
        dd($image->getRealPath());
        $avatarName = substr(uniqid(rand(), true), 8, 8) . '.' . $image->getClientOriginalExtension();
        $img = Image::make($image->getRealPath())->encode('jpg', 65)->fit(760, null, function ($c) {
            $c->aspectRatio();
            $c->upsize();
        });
        $img->stream(); // <-- Key point
        dd($img);
//        Storage::disk('local')->put('public/images/avatars' . '/' . $avatarName, $img, 'public');

        //SAVE FILE NAME TO DB
//        $userUpdate = Auth::user()->update([
//            'first_name' => $this->first_name,
//            'last_name' => $this->last_name,
//            'avatar' => $avatarName,
//        ]);
    }


//    public function updateProfile(): void
//    {
//
//    }

//    public function submit(Request $request)
//    {
//        dd($request);
//        dd(app(UploadController::class)->folder);
//}
    public function render()
    {
        return view('livewire.lawcase.manage-attachment')->layout('layouts.main');
    }
}
