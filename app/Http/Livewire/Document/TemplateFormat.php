<?php

namespace App\Http\Livewire\Document;

use Livewire\Component;

class TemplateFormat extends Component
{
    public function render()
    {
        return view('livewire.document.template-format')->layout('layouts.main');
    }
}
