<div class="avatar-file">
    <label class = "text-bh-navy-500">Choose Image</label>
    <input type = "file" wire:model = "avatar">

    <button wire:click="submitProfileUpdate()"> save</button>
</div>
{{--@section('header')--}}
{{--    <div class="row mb-2">--}}
{{--        <div class="col-sm-6">--}}
{{--            <h1 class="m-0">Case Registry</h1>--}}
{{--        </div><!-- /.col -->--}}
{{--        <div class="col-sm-6">--}}
{{--            <ol class="breadcrumb float-sm-right">--}}
{{--                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>--}}
{{--                <li class="breadcrumb-item active">Case Registry</li>--}}
{{--            </ol>--}}
{{--        </div><!-- /./col -->--}}
{{--    </div><!-- /row -->--}}
{{--@endsection--}}

{{--<div class="card card-secondary">--}}
{{--    <div class="card-header">--}}
{{--        <h3 class="card-title">Case Registry Form</h3>--}}
{{--        <div class="card-tools">--}}
{{--            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">--}}
{{--                <i class="fas fa-minus"></i>--}}
{{--            </button>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <form wire:submit.prevent="submit">--}}
{{--        <input type="text" wire:model="name">--}}

{{--        <div class="form-group" wire:ignore>--}}
{{--            <input type="file" name="avatar" id="avatar" multiple>--}}
{{--        </div>--}}

{{--        <button type="submit" class="transition bg-keppel py-2 rounded px-4 text-white font-semibold tracking-wide">--}}
{{--            Save--}}
{{--        </button>--}}
{{--    </form>--}}

{{--    --}}{{-- Uploaded images --}}
{{--    <div class="form-group">--}}
{{--        <label class="text-muted">Previously Uploaded Images</label><br>--}}
{{--        <div class="d-flex flex-wrap">--}}
{{--            @foreach($post->getMedia('post') as $key => $postImage)--}}
{{--                <div class="card mr-2 d-flex flex-column justify-content-between">--}}
{{--                    <img src="{{asset('storage/uploads/law_case/8/633448d697c24-1664370902.jpg')}}" class="align-self-center"--}}
{{--                         alt="post_image">--}}
{{--                    <a href="#" data-mediaId="1"--}}
{{--                       class="badge badge-danger text-center media-delete-btn">DELETE</a>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--        </div>--}}

{{--    </div>--}}

{{--</div>--}}
{{--@push('styles')--}}
{{--    @once--}}
{{--        <!-- file fond css -->--}}
{{--        <link href="{{ asset('vendor/filepond/filepond.min.css') }}" rel="stylesheet">--}}
{{--        <link href="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.css') }}" rel="stylesheet">--}}
{{--    --}}{{--        <style>--}}
{{--    --}}{{--            .details {--}}
{{--    --}}{{--                display: flex;--}}
{{--    --}}{{--                align-items: center;--}}
{{--    --}}{{--            }--}}

{{--    --}}{{--            .detail-col {--}}
{{--    --}}{{--                border-right: 1px solid rgba(222, 226, 230, 0.3);--}}
{{--    --}}{{--            }--}}
{{--    --}}{{--        </style>--}}
{{--    @endonce--}}
{{--@endpush--}}

{{--@push('scripts')--}}
{{--    @once--}}
{{--        <!-- filefond js -->--}}
{{--        <script src="{{ asset('vendor/filepond/filepond.min.js') }}"></script>--}}
{{--        <script src="{{ asset('vendor/filepond/filepond.jquery.js') }}"></script>--}}
{{--        <script src="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.js') }}"></script>--}}
{{--        <script src="{{ asset('vendor/filepond/filepond-plugin-file-validate-size.min.js') }}"></script>--}}
{{--        <script src="{{ asset('vendor/filepond/filepond-plugin-file-encode.min.js') }}"></script>--}}

{{--        <script>--}}
{{--            /**--}}
{{--             * file fond--}}
{{--             */--}}
{{--            $.fn.filepond.registerPlugin(--}}
{{--                // previews dropped images--}}
{{--                // FilePondPluginImagePreview,--}}
{{--                // validates the size of the file--}}
{{--                FilePondPluginFileValidateSize,--}}
{{--                // encodes the file as base64 data--}}
{{--                FilePondPluginFileEncode--}}
{{--            );--}}
{{--            //--}}
{{--            const inputElement = document.querySelector('input[type="file"]');--}}
{{--            // // Create a FilePond instance--}}
{{--            const pond = FilePond.create(inputElement);--}}

{{--            FilePond.setOptions({--}}
{{--                server: {--}}
{{--                    url: '../upload/',--}}
{{--                    revert:'../upload/',--}}
{{--                    headers: {--}}
{{--                        'X-CSRF-TOKEN': '{{csrf_token()}}'--}}
{{--                    }--}}
{{--                }--}}
{{--            });--}}

{{--        </script>--}}
{{--    @endonce--}}
{{--@endpush--}}
