<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LawCase extends Model
{
    use HasFactory;

    public function client_person()
    {
        return $this->hasOne(Customer::class, 'id', 'client_id');
    }

    public function client_company()
    {
        return $this->hasOne(ClientCompany::class, 'id', 'client_company_id');
    }

    public function law_field()
    {
        return $this->hasOne(LawCategory::class, 'id', 'category_id');
    }

    public function registered_attorney()
    {
        return $this->hasOne(JuniorLawyer::class, 'id', 'registered_attorney_id');
    }

    public function assigned_lawyer()
    {
        return $this->hasOne(JuniorLawyer::class, 'id', 'assigned_lawyer_id');
    }

}
