<?php

namespace App\Http\Livewire\Lawcase;

use Livewire\Component;

class AddCategory extends Component
{
    public function render()
    {
        return view('livewire.lawcase.add-category');
    }
}
