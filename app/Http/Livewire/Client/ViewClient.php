<?php

namespace App\Http\Livewire\Client;

use Livewire\Component;

class ViewClient extends Component
{
    public function render()
    {
        return view('livewire.client.view-client')->layout('layouts.main');
    }
}
