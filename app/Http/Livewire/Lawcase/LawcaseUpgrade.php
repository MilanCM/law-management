<?php

namespace App\Http\Livewire\Lawcase;

use App\Models\CaseUpdateDetail;
use App\Models\LawCase;
use App\Models\LawCaseMedia;
use App\Models\TemporaryFile;
use App\Models\Witness;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;

class LawcaseUpgrade extends Component
{
    use WithFileUploads;
    use \Livewire\WithPagination;

    public $searched_case_id;
    public $selected_case_id;
    public $images_upg;
    public $all_cases;
    protected $paginationTheme = 'bootstrap';

    public $location;
    public $date;
    public $next_date;
    public $description;
    public $comment;
    public $witness_f_name;
    public $witness_m_name;
    public $witness_l_name;
    public $witness_family_name;
    public $witness_nic;
    public $witness_relationship;
    public $witness_email;
    public $witness_contact_1;
    public $witness_contact_2;
    public $witness_no;
    public $witness_street_1;
    public $witness_street_2;
    public $witness_city;
    public $arr_witness;
    public $type;

    public function reset_al()
    {
        $this->images_upg = [];
        $this->comment = null;
        $this->description = null;
        $this->location = null;
        $this->next_date = null;
        $this->date = null;
    }

// StartInsertUpgradeCaseDetails
    public
    function insertUpgradeCaseDetails()
    {
        if ($this->searched_case_id !== null) {
            $this->validate([
                'location' => 'required|string|max:20',
                'date' => 'required|date',
                'next_date' => 'required|date',
                'description' => 'required|string',
                'comment' => 'required|string',
            ]);
            $upg_case = new CaseUpdateDetail();
            $upg_case->court_location = $this->location;
            $upg_case->date_of_trial = $this->date;
            $upg_case->next_date_of_trial = $this->next_date;
            $upg_case->description = $this->description;
            $upg_case->comment = $this->comment;
            $upg_case->is_active = 1;
            $upg_case->law_case_id = $this->searched_case_id;
            $upg_case->save();

            // Media save
            $uploaded_images = TemporaryFile::where('folder', 'case_media_folder')->get();
            if ($uploaded_images) {
                foreach ($uploaded_images as $media) {
                    $moved_path = 'uploads/law_case/' . $this->searched_case_id . '/' . $media->filename;
                    Storage::move('avatars/tmp/case_media_folder/' . $media->filename, $moved_path);
                    $law_case_media = new LawCaseMedia();
                    $law_case_media->file_type = $media->extension;
                    $law_case_media->file_path = $media->filename;
                    $law_case_media->is_active = 1;
                    $law_case_media->law_case_id = $this->searched_case_id;
                    $law_case_media->law_case_upgrade_id = $upg_case['id'];
                    $law_case_media->save();
                    $media->delete();
                }
                Storage::deleteDirectory('avatars');
            }
            $this->reset_al();
            return redirect(request()->header('Referer'));
        }
    }//    EndInsertUpgradeCaseDetails

// StartLoadSelectedCaseDetails
    public
    function LoadSelectedCaseDetails($case)
    {
        $this->selected_case_id = $case['id'];
        $this->location = $case['court_location'];
        $this->date = $case['date_of_trial'];
        $this->next_date = $case['next_date_of_trial'];
        $this->description = $case['description'];
        $this->comment = $case['comment'];

        $this->images_upg = LawCaseMedia::where('law_case_id', $case['law_case_id'])
            ->where('law_case_upgrade_id', $this->selected_case_id)->get();
    }//    EndLoadSelectedCaseDetails

// StartActivateSelectedCaseDetails
    public
    function ActivateSelectedCaseDetails($case)
    {
        $caseActivate = CaseUpdateDetail::find($case['id']);
        $caseActivate->is_active = '1';
        $caseActivate->save();
        $this->ClearWitnessFields();
    }//    EndActivateSelectedCaseDetails

// StartDeactivateSelectedCaseDetails
    public
    function DeactivateSelectedCaseDetails($case)
    {
        $caseDeactivate = CaseUpdateDetail::find($case['id']);
        $caseDeactivate->is_active = '0';
        $caseDeactivate->save();
        $this->ClearWitnessFields();
    }//    EndDeactivateSelectedCaseDetails

// startAddWitness
    public
    function AddWitness()
    {
        $this->validate([
            'witness_f_name' => 'required|string',
            'witness_l_name' => 'required|string',
            'witness_m_name' => 'required|string',
            'witness_family_name' => 'required|string',
            'witness_nic' => 'required|string',
            'witness_relationship' => 'required|string',
            'witness_email' => 'required|email',
            'witness_contact_1' => 'required',
//            'witness_contact_2' => 'digits:10',
            'witness_no' => 'required|string',
            'witness_street_1' => 'required|string',
//            'witness_street_2' => 'string',
            'witness_city' => 'required|string',
        ]);

        $arr = ['f_name' => $this->witness_f_name,
            'l_name' => $this->witness_l_name,
            'm_name' => $this->witness_m_name,
            'family_name' => $this->witness_family_name,
            'nic' => $this->witness_nic,
            'relationship' => $this->witness_relationship,
            'email' => $this->witness_email,
            'contact_1' => $this->witness_contact_1,
            'contact_2' => $this->witness_contact_2,
            'no' => $this->witness_no,
            'street_1' => $this->witness_street_1,
            'street_2' => $this->witness_street_2,
            'city' => $this->witness_city];

        if (empty($this->arr_witness)) {
            array_push($this->arr_witness, $arr);
        } else {
            $value = array_search($this->witness_nic, array_column($this->arr_witness, 'nic'));
            if ($value !== false) {
                $this->arr_witness[$value]['f_name'] = $this->witness_f_name;
                $this->arr_witness[$value]['l_name'] = $this->witness_l_name;
                $this->arr_witness[$value]['m_name'] = $this->witness_m_name;
                $this->arr_witness[$value]['family_name'] = $this->witness_family_name;
                $this->arr_witness[$value]['nic'] = $this->witness_nic;
                $this->arr_witness[$value]['relationship'] = $this->witness_relationship;
                $this->arr_witness[$value]['email'] = $this->witness_email;
                $this->arr_witness[$value]['contact_1'] = $this->witness_contact_1;
                $this->arr_witness[$value]['contact_2'] = $this->witness_contact_2;
                $this->arr_witness[$value]['no'] = $this->witness_no;
                $this->arr_witness[$value]['street_1'] = $this->witness_street_1;
                $this->arr_witness[$value]['street_2'] = $this->witness_street_2;
                $this->arr_witness[$value]['city'] = $this->witness_city;
            } else {
                array_push($this->arr_witness, $arr);
            }
        }
        $this->ClearWitnessFields();
    }//    endAddWitness

// startWitnessRemoveArray
    public
    function witnessRemoveArray($id)
    {
        unset($this->arr_witness[$id]);
    }//    endWitnessRemoveArray

// startClearWitnessFields
    public
    function ClearWitnessFields()
    {
        $this->case_witness_f_name = null;
        $this->case_witness_m_name = null;
        $this->case_witness_l_name = null;
        $this->case_witness_family_name = null;
        $this->case_witness_nic = null;
        $this->case_witness_relationship = null;
        $this->case_witness_email = null;
        $this->case_witness_contact_1 = null;
        $this->case_witness_contact_2 = null;
        $this->case_witness_no = null;
        $this->case_witness_street_1 = null;
        $this->case_witness_street_2 = null;
        $this->case_witness_city = null;
    }//    endClearWitnessFields

// startWitnessEditArray
    public
    function witnessEditArray($id)
    {
        $this->witness_f_name = $this->arr_witness[$id]['f_name'];
        $this->witness_m_name = $this->arr_witness[$id]['m_name'];
        $this->witness_l_name = $this->arr_witness[$id]['l_name'];
        $this->witness_family_name = $this->arr_witness[$id]['family_name'];
        $this->witness_nic = $this->arr_witness[$id]['nic'];
        $this->witness_relationship = $this->arr_witness[$id]['relationship'];
        $this->witness_email = $this->arr_witness[$id]['email'];
        $this->witness_contact_1 = $this->arr_witness[$id]['contact_1'];
        $this->witness_contact_2 = $this->arr_witness[$id]['contact_2'];
        $this->witness_no = $this->arr_witness[$id]['no'];
        $this->witness_street_1 = $this->arr_witness[$id]['street_1'];
        $this->witness_street_2 = $this->arr_witness[$id]['street_2'];
        $this->witness_city = $this->arr_witness[$id]['city'];
    }//    endWitnessEditArray

    public
    function mount()
    {
        $this->all_cases = LawCase::where('is_active', '1')->get();
        if (Storage::exists('avatars')) {
            Storage::deleteDirectory('avatars');
            TemporaryFile::where('folder', 'case_media_folder')->delete();
        }
    }

    public function render()
    {
        $searched_law_case = null;
        $upgraded_cases = [];
        $this->arr_witness = [];
        if ($this->searched_case_id) {
            $searched_law_case = LawCase::find($this->searched_case_id);
            $upgraded_cases = CaseUpdateDetail::where('law_case_id', $searched_law_case['id'])->paginate(4);
        }

        $witnesses = Witness::where('law_case_id', $this->searched_case_id)->get();
        foreach ($witnesses as $witness) {
            $arr = [
                'f_name' => $witness['f_name'],
                'l_name' => $witness['l_name'],
                'm_name' => $witness['m_name'],
                'family_name' => $witness['sur_name'],
                'nic' => $witness['nic'],
                'relationship' => $witness['relationship'],
                'email' => $witness['email'],
                'contact_1' => $witness['contact_1'],
                'contact_2' => $witness['contact_2'],
                'no' => $witness['no'],
                'street_1' => $witness['street_1'],
                'street_2' => $witness['street_2'],
                'city' => $witness['city']];

            array_push($this->arr_witness, $arr);
        }

        return view('livewire.lawcase.lawcase-upgrade', ['searched_law_case' => $searched_law_case,
            'upgraded_cases' => $upgraded_cases])->layout('layouts.main');
    }
}
