<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LawyerOfficeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offices')->insert([
            'office_name' => 'Main Office',
            'no' => '443/11',
            'street_1' => 'buwaneka rd',
            'street_2' => 'Third lane',
            'city' => 'Kurunegala',
            'contact_1' => '0714252396',
            'contact_2' => '0714252396',
            'is_active' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('offices')->insert([
            'office_name' => 'Sub Office',
            'no' => '41',
            'street_1' => 'Main rd',
            'street_2' => 'mal mawatha',
            'city' => 'Aththanagalla',
            'contact_1' => '0714252396',
            'contact_2' => '0714252396',
            'is_active' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
