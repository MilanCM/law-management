<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Milan',
            'email' => 'milanmc888@gmail.com',
            'password' => '$2y$10$u64RBaJ3OrPeEWDXW/tdMegLv9X8bueCa.Sa4aXE2ODHSrzfeWhA.',
            'is_active' => '1',
            'user_role_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
