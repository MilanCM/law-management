
@push('scripts')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
{{--    <script src="{{ asset('assets/plugins/tinymce/tinymce.min.js') }}"></script>--}}
    <style>
        .circle-chart {
            width: 160px;

        }

        /*js.blade.php*/

        /*


         tinymce.init({
                    selector: '#description',
                    language: '

        {{ (!empty($editorI18nJson)) ? config('app.locale') : 'en' }}

        ',
                            directionality: '

        {{ (config('lang.direction') == 'rtl') ? 'rtl' : 'ltr' }}

        ',
                            height: 350,
                            menubar: false,
                            statusbar: false,
                            plugins: 'lists link table wordcount',
                            toolbar: 'undo redo | bold italic underline | forecolor backcolor | bullist numlist blockquote table | link unlink | alignleft aligncenter alignright | outdent indent | fontsizeselect',
                              init_instance_callback: function (editor) {
                                    editor.on('KeyUp', function (e) {
                                    console.log('Element clicked:', e.target.nodeName);

                                        var p=(getStats('description').chars/600)*100;
                                        if(p>100)
                                        {
                                           p=100;
                                        }
                                        $('.circlechart-2').circlechart(p,getStats('description').chars);
                                });
                          }
                        });

                        */
        .circle-chart__circle {
            stroke: #00acc1;
            stroke-width: 2;
            stroke-linecap: square;
            fill: none;
            animation: circle-chart-fill 2s reverse;
            /* 1 */
            transform: rotate(-90deg);
            /* 2, 3 */
            transform-origin: center;
            /* 4 */
        }

        /**
     * 1. Rotate by -90 degree to make the starting point of the
     *    stroke the top of the circle.
     * 2. Scaling mirrors the circle to make the stroke move right
     *    to mark a positive chart value.
     * 3. Using CSS transforms on SVG elements is not supported by Internet Explorer
     *    and Edge, use the transform attribute directly on the SVG element as a
     * .  workaround.
     */

        .circle-chart__circle--negative {
            transform: rotate(-90deg) scale(1, -1);
            /* 1, 2, 3 */
        }

        .circle-chart__background {
            stroke: #efefef;
            stroke-width: 2;
            fill: none;
        }

        .circle-chart__info {
            animation: circle-chart-appear 2s forwards;
            opacity: 0;
            transform: translateY(0.3em);
        }

        .circle-chart__percent {
            alignment-baseline: central;
            text-anchor: middle;
            font-size: 8px;
        }

        .circle-chart__subline {
            alignment-baseline: central;
            text-anchor: middle;
            font-size: 3px;
        }

        .success-stroke {
            stroke: #0e84b5;
        }

        .warning-stroke {
            stroke: yellow;
        }

        .danger-stroke {
            stroke: red;
        }

        @keyframes circle-chart-fill {
            to {
                stroke-dasharray: 0 100;
            }
        }

        @keyframes circle-chart-appear {
            to {
                opacity: 1;
                transform: translateY(0);
            }
        }

    </style>

    <script>
        function makesvg(percentage, inner_text = "", detail= "",) {

            if (percentage > 100){
                percentage = 100;
            }
            var abs_percentage = Math.abs(percentage).toString();
            var percentage_str = percentage.toString();
            var classes = ""

            if (percentage < 0) {
                classes = "danger-stroke circle-chart__circle--negative";
            } else if (percentage > 50 && percentage <= 90) {
                classes = "warning-stroke";
            } else if (percentage > 90 && percentage <= 100) {
                classes = "success-stroke";
            }
            else{
                classes = "danger-stroke";
            }

            var svg =
                '<svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" xmlns="http://www.w3.org/2000/svg">' +
                '<circle class="circle-chart__background" cx="16.9" cy="16.9" r="15.9" />' +
                '<circle class="circle-chart__circle ' + classes + '"' +
                'stroke-dasharray="' + abs_percentage + ',100"    cx="16.9" cy="16.9" r="15.9" />' +
                '<g class="circle-chart__info">' +
                '   <text class="circle-chart__percent" x="17.9" y="15.5">' + percentage_str + '%</text>';

            if (inner_text) {
                svg += '<text class="circle-chart__subline" x="16.91549431" y="22">' + inner_text + '</text>'
            }
            if (detail) {
                svg += '<text class="circle-chart__subline" x="16.91549431" y="26">' + detail + '</text>'
            }


            svg += ' </g></svg>';

            return svg
        }

        (function ($) {

            $.fn.circlechart = function (p, length,t) {
                this.each(function () {
                    var percentage = parseInt(p);
                    var inner_text = "#" + length;
                    var detail = t;
                    $(this).html(makesvg(percentage, inner_text,detail));
                });
                return this;
            };

        }(jQuery));

        var title = 'Milan Chathuranga';
        var pic = '2';
        var description ='Milan ChathurangaMilan ChathurangaMilan Chathuranga Milan Chathuranga';
        var contact = 2;

        $('.pending_cases').circlechart(0, title.length,'Title');
        $('.ongoing_cases').circlechart(0, description.length,'Description'); // Initialization
        $('.closed_cases').circlechart(0, pic,'Image'); // Initialization
        $('.next_cases').circlechart(0, contact,'Email & Mobile'); // Initialization
    </script>

    <script type="text/javascript">

        $('.pending_cases').append(function (e) {
            v = title.substring(0, 65);
            v = v.replace("   ", " ");
            v = v.replace("...", " ");
            v = v.replace("___", " ");
            v = v.replace("---", " ");
            v = v.replace("+++", " ");
            v = v.replace(",,,", " ");
            v = v.replace("~~~", " ");
            v = v.replace("@@@", " ");
            v = v.replace("###", " ");
            v = v.replace("#", " ");
            v = v.replace("$$$", " ");
            v = v.replace("%%%", " ");
            v = v.replace("^^^", " ");
            v = v.replace("&&&", " ");
            v = v.replace("***", " ");
            v = v.replace("(((", " ");
            v = v.replace(")))", " ");
            v = v.replace("~~~", " ");
            v = v.replace("```", " ");
            v = v.replace(";;;", " ");
            v = v.replace(":::", " ");
            v = v.replace("...", " ");
            v = v.replace(",,,", " ");
            v = v.replace("???", " ");
            v = v.replace("///", " ");
            v = v.replace("|||", " ");
            v = v.replace("}}}", " ");
            v = v.replace("\{\{\{", " ");

            var p = (v.length / 60) * 100;
            $('.pending_cases').circlechart(p, v.length,'Pending Cases');

        });

        $('.ongoing_cases').append(function (e) {
            v = pic;
            var p = (pic / 2) * 100;
            $('.ongoing_cases').circlechart(p, v,'Ongoing Cases');

        });

        $('.closed_cases').append(function (e) {
            v = contact;
            var p = (contact / 2) * 100;
            $('.closed_cases').circlechart(p, v,'Closed Cases');

        });

        $('.next_cases').append(function (e) {
            v = description.substring(0, 600);
            var p = (v.length / 600) * 100;
            $('.next_cases').circlechart(p, v.length,'Pending Cases');

        });

    </script>
    @endpush
