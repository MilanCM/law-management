@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Office Details</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Office Details</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>


    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="table_search"
                       class="form-control form-control-border border-width-2 bg-transparent"
                       placeholder="Search by Name/ City" wire:model="catchKey">
            </div>
            <div class="col-sm-2">
            </div>

{{--            <div class="col-sm-2">--}}
{{--                <div class="custom-control custom-checkbox">--}}
{{--                    <input class="custom-control-input"--}}
{{--                           type="checkbox" wire:click="changeCustomerState" wire:model="active"--}}
{{--                           id="customCheckbox1" value="1">--}}
{{--                    <label for="customCheckbox1" class="custom-control-label">Active</label>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-sm-2">--}}

{{--                <div class="custom-control custom-checkbox">--}}
{{--                    <input class="custom-control-input" wire:click="changeCustomerState" type="checkbox"--}}
{{--                           id="customCheckbox2" value="2" wire:model="deActive">--}}
{{--                    <label for="customCheckbox2" class="custom-control-label">Deactive</label>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="row">
    </div>

    <div class="card mt-2 card-info mt-3">
        <div class="card-header">
            <h3 class="card-title">Lawyer Office List</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="max-height: 200px;">
            <table class="table-striped table-sm table">
                <thead style="background-color: rgba(185,202,209,0.62)">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($lawyer_offices as $lawyer_office)
                    <tr wire:click="loadLawyerOfficeDetail({{$lawyer_office}})">
                        <td>{{$lawyer_office->id}}</td>
                        <td>{{$lawyer_office->office_name}}</td>
                        <td>{{$lawyer_office->city}}</td>
                        <td>{{$lawyer_office->contact_1}}</td>
                        <td>{{$lawyer_office->no}}, {{$lawyer_office->street_1}}, {{$lawyer_office->street_2}}
                            , {{$lawyer_office->city}}</td>
                        @if($lawyer_office->is_active == 1)
                            <td><span class="badge badge-success">Active</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-xs "
                                        wire:click="LawyerOfficeDeactivate({{$lawyer_office->id}})"><i class="fa fa-lock"
                                                                                                       aria-hidden="true"></i>
                                </button>
                            </td>
                        @else
                            <td><span class="badge badge-danger">Deactive</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-success btn-xs "
                                        wire:click="LawyerOfficeActivate({{$lawyer_office->id}})"><i
                                        class="fa fa-lock-open"
                                        aria-hidden="true"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="mt-2 card-footer right">
            {{$lawyer_offices->links()}}
        </div>
        <!-- /.card-body -->
    </div>


    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Office Registry Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body">

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Office Name</label>
                        <input type="text"
                               class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="asd- Colombo" wire:model="office_name">
                        @error('office_name') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label class="mb-sm-0">Email</label>
                        <input type="email" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter email" wire:model="office_email">
                        @error('office_email') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Contact</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 1"
                                           wire:model="office_contact1">
                                </label>
                                @error('office_contact1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 2"
                                           wire:model="office_contact2">
                                </label>
                                @error('office_contact2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Address</label>
                        <div class="row">
                            <div class="col-sm-1">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="no."
                                       wire:model="office_no">
                                @error('no') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street1"
                                       wire:model="office_street1">
                                @error('office_street1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street2"
                                       wire:model="office_street2">
                                @error('office_street2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="city"
                                       wire:model="office_city">
                                @error('office_city') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-primary" wire:click.prevent="LawyerOfficeDetailsInsert()">Insert</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="LawyerOfficeDetailsUpdate()">Update</button>
                </div>
            </div>
        </div>

    </div>
</main>
