<?php

namespace App\Http\Livewire\User;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ManageAccount extends Component
{
    public $email;
    public $password;
    public $confirm_password;
    public $users;
    public $select_user_id;
    public $is_set;
    public $user_f_name;

// startCreateUserAccount
    public function createUserAccount()
    {
        if ($this->select_user_id !== null) {
            if ($this->is_set) {
                dd('already add user account');
            } else {
                $this->validate([
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                ]);
                if ($this->password === ($this->confirm_password)) {
                    $user = new User();
                    $user->name = $this->user_f_name;
                    $user->email = $this->email;
                    $user->password = Hash::make($this->password);
                    $user->user_role_id = 3;
                    $user->is_active = 1;
                    $user->save();

                    $employee = Employee::find($this->select_user_id);
                    $employee->user_id = $user['id'];
                    $employee->save();
                    $this->ClearFieldUserAccountsEmployee();
                } else {
                    dd('confirmed password not match');
                }
            }
        } else {
            dd('please select an user');
        }
    }//    endCreateUserAccount

    public function mount()
    {
        $this->users = Employee::where('is_active', '1')->get();
    }
// startClearFieldUserAccountsEmployee
    public function ClearFieldUserAccountsEmployee()
    {
        $this->email = null;
        $this->password = null;
        $this->confirm_password = null;
        $this->users = null;
        $this->select_user_id = null;
        $this->is_set = null;
        $this->user_f_name = null;
    }//    endClearFieldUserAccountsEmployee

    public function render()
    {
        $this->users = Employee::where('is_active', '1')->get();
        $selected_user = Employee::find($this->select_user_id);
        if ($selected_user !== null) {
            $this->user_f_name = $selected_user['f_name'];
            if ($selected_user['user_id'] !== null) {
                $this->is_set = true;
            } else {
                $this->is_set = false;
            }
        }
        return view('livewire.user.manage-account', ['selected_user' => $selected_user])->layout('layouts.main');
    }
}
