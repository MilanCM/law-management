<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JuniorLawyer extends Model
{
    use HasFactory;

    /**
     * The one to many inverse relation with lawyerCategory
     */
    public function lawyerCategory()
    {
        return $this->hasOne(LawyerCategory::class,'id', 'category_id');
    }
    public function user_role()
    {
        return $this->hasOne(UserRole::class, 'id', 'user_role_id');
    }
}
