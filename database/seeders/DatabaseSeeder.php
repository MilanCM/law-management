<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
//        $this->call(LawCategorySeeder::class);
//        $this->call(LawyerCategorySeeder::class);
//        $this->call(UserRoleSeeder::class);
//        $this->call(UserSeeder::class);


        $this->call([
            LawCategorySeeder::class,
            LawyerCategorySeeder::class,
            UserRoleSeeder::class,
            UserSeeder::class,
            SeniorLawyerSeeder::class,
            JuniorLawyerSeeder::class,
            LawyerOfficeSeeder::class,
            EmployeeSeeder::class,
            ClientPersonSeeder::class,
            ClientCompanySeeder::class
        ]);
    }
}
