<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaseUpdateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('case_update_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('court_location');
            $table->date('next_date_of_trial');
            $table->string('description');
            $table->string('comment');
            $table->date('date_of_trial');
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('law_case_id');

            $table->foreign('law_case_id')->references('id')->on('law_cases');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('case_update_details');
    }
}
