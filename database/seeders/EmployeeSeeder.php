<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'f_name' => 'Milan',
            'm_name' => 'Chathuranga',
            'l_name' => 'Madiwela',
            'nic' => '940790123V',
            'etf_number' => '6463',
            'email' => 'milam@gmail.com',
            'gender' => 'Male',
            'occupation' => 'Book Keeper',
            'contact_1' => '0741524589',
            'contact_2' => '0714578485',
            'no' => '487',
            'street_1' => 'main st',
            'street_2' => 'Kurunegala rd',
            'city' => 'Kurunegala',
            'image' => 'uploads/user/contact image.jpg',
            'marriage_status' => 'Single',
            'is_active' => '1',
            'office_id' => '1',
            'user_role_id' => '3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('employees')->insert([
            'f_name' => 'Nihal',
            'm_name' => 'Nelsan',
            'l_name' => 'Perepa',
            'nic' => '914520123V',
            'etf_number' => '6443',
            'email' => 'nehal@gmail.com',
            'gender' => 'Male',
            'occupation' => 'Clerk',
            'contact_1' => '0741524578',
            'contact_2' => '0714545781',
            'no' => '4',
            'street_1' => 'main st',
            'street_2' => 'Kurunegala rd',
            'city' => 'Kurunegala',
            'image' => 'uploads/user/contact image.jpg',
            'marriage_status' => 'Single',
            'is_active' => '1',
            'office_id' => '2',
            'user_role_id' => '3',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
