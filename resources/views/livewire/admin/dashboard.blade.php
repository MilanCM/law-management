{{--@extends('layouts.main')--}}
@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div><!-- /.col -->

    </div><!-- /.row -->
@endsection

@section('content')

    <!--Dashboard Main Lables -->

    <div class="col-md-8">
        <div class="percentage-div">
            @include('livewire.account.inc.percentage')
            <div class="pending_cases"
                 data-percentage="0"></div>
            <div class="ongoing_cases"
                 data-percentage="0"></div>
            <div class="closed_cases"
                 data-percentage="0"></div>
            <div class="next_cases"
                 data-percentage="0"></div>
        </div>
    </div>

{{--    <div class="row">--}}


{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-info">--}}
{{--                <div class="inner">--}}
{{--                    <h4>InComes</h4>--}}
{{--                    <h3 class="text-center">250000/:</h3>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-bag"></i>--}}
{{--                </div>--}}
{{--                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-success">--}}
{{--                <div class="inner">--}}
{{--                    <h4>Suppliers</h4>--}}
{{--                    <h3 class="text-center">45</h3>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-stats-bars"></i>--}}
{{--                </div>--}}
{{--                <a href="#" class="small-box-footer">More info <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-warning">--}}
{{--                <div class="inner">--}}
{{--                    <h4>Employees</h4>--}}
{{--                    <h3 class="text-center">05</h3>--}}
{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-person-add"></i>--}}
{{--                </div>--}}
{{--                <a href="#" class="small-box-footer">More info <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="col-lg-3 col-6">--}}
{{--            <!-- small box -->--}}
{{--            <div class="small-box bg-danger">--}}
{{--                <div class="inner">--}}
{{--                    <h4>Credit Customers</h4>--}}
{{--                    <h3 class="text-center">120</h3>--}}

{{--                </div>--}}
{{--                <div class="icon">--}}
{{--                    <i class="ion ion-pie-graph"></i>--}}
{{--                </div>--}}
{{--                <a href="#" class="small-box-footer">More info <i--}}
{{--                        class="fas fa-arrow-circle-right"></i></a>--}}
{{--            </div>--}}

{{--        </div>--}}

{{--    </div>--}}
{{--    <!--Dashboard Main Lables -->--}}

{{--    <div class="row">--}}
{{--        <section class="col-lg-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">--}}
{{--                    <h3 class="card-title">--}}
{{--                        <i class="fas fa-chart-pie mr-1"></i>--}}
{{--                        Sales Charts--}}
{{--                    </h3>--}}
{{--                    <div class="card-tools">--}}
{{--                        <ul class="nav nav-pills ml-auto">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Line</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="#sales-chart" data-toggle="tab">Bar</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="#sales-chart" data-toggle="tab">Pie</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div><!-- /.card-header -->--}}
{{--                <div class="card-body">--}}
{{--                    <div class="tab-content p-0">--}}
{{--                        <!-- Morris chart - Sales -->--}}
{{--                        <div class="chart tab-pane active" id="revenue-chart"--}}
{{--                             style="position: relative; height: 300px;">--}}
{{--                            <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                        </div>--}}
{{--                        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">--}}
{{--                            <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div><!-- /.card-body -->--}}
{{--            </div>--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">--}}
{{--                    <h3 class="card-title">--}}
{{--                        <i class="fas fa-chart-pie mr-1"></i>--}}
{{--                        OutGoing Charts--}}
{{--                    </h3>--}}
{{--                    <div class="card-tools">--}}
{{--                        <ul class="nav nav-pills ml-auto">--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Line</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="#sales-chart" data-toggle="tab">Bar</a>--}}
{{--                            </li>--}}
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" href="#sales-chart" data-toggle="tab">Pie</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div><!-- /.card-header -->--}}
{{--                <div class="card-body">--}}
{{--                    <div class="tab-content p-0">--}}
{{--                        <!-- Morris chart - Sales -->--}}
{{--                        <div class="chart tab-pane active" id="revenue-chart"--}}
{{--                             style="position: relative; height: 300px;">--}}
{{--                            <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                        </div>--}}
{{--                        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">--}}
{{--                            <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            <!-- Low Stock Table -->--}}
{{--            <div class="card">--}}
{{--                <div class="card-header border-transparent">--}}
{{--                    <h3 class="card-title">Low Stock Items</h3>--}}

{{--                    <div class="card-tools">--}}
{{--                        <button type="button" class="btn btn-tool" data-card-widget="collapse">--}}
{{--                            <i class="fas fa-minus"></i>--}}
{{--                        </button>--}}
{{--                        <button type="button" class="btn btn-tool" data-card-widget="remove">--}}
{{--                            <i class="fas fa-times"></i>--}}
{{--                        </button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- /.card-header -->--}}
{{--                <div class="card-body p-0">--}}
{{--                    <div class="table-responsive">--}}
{{--                        <table class="table m-0">--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th>Item ID</th>--}}
{{--                                <th>Item Name</th>--}}
{{--                                <th>Qty</th>--}}
{{--                                <th>Supplier</th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                            <tr>--}}
{{--                                <td>01</td>--}}
{{--                                <td>Melva</td>--}}
{{--                                <td><span class="badge badge-danger">10</span></td>--}}
{{--                                <td>--}}
{{--                                    <span class="badge badge-info">01-D.H.Perera</span>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>02</td>--}}
{{--                                <td>Saylac</td>--}}
{{--                                <td><span class="badge badge-danger">14</span></td>--}}
{{--                                <td>--}}
{{--                                    <span class="badge badge-info">04-H.G.Silva</span>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>03</td>--}}
{{--                                <td>Lanva</td>--}}
{{--                                <td><span class="badge badge-danger">32</span></td>--}}
{{--                                <td>--}}
{{--                                    <span class="badge badge-info">07-S.A Kumara</span>--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>04</td>--}}
{{--                                <td>Tokyo Super</td>--}}
{{--                                <td><span class="badge badge-danger">13</span></td>--}}
{{--                                <td>--}}
{{--                                    <span class="badge badge-info">05-N.Janaka</span>';--}}
{{--                                </td>--}}
{{--                            </tr>--}}
{{--                            <tr>--}}
{{--                                <td>05</td>--}}
{{--                                <td>Harise Brush</td>--}}
{{--                                <td><span class="badge badge-danger">04</span></td>--}}
{{--                                <td>--}}
{{--                                    <span class="badge badge-info">08-K.Ranasinghe</span>--}}
{{--                                </td>--}}
{{--                            </tr>--}}


{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
{{--                    <!-- /.table-responsive -->--}}
{{--                </div>--}}
{{--                <!-- /.card-body -->--}}
{{--                <div class="card-footer clearfix">--}}
{{--                    <a href="javascript:void(0)" class="btn btn-sm btn-info float-left">Place New Order</a>--}}
{{--                    <a href="javascript:void(0)" class="btn btn-sm btn-secondary float-right">View All Orders</a>--}}
{{--                </div>--}}
{{--                <!-- /.card-footer -->--}}
{{--            </div>--}}
{{--            <!-- /.card -->--}}

{{--            <!-- /.col -->--}}

{{--        </section>--}}


{{--        <section class="col-lg-4">--}}

{{--            <div class="info-box mb-3 bg-warning">--}}
{{--                <span class="info-box-icon"><i class="fas fa-tag"></i></span>--}}

{{--                <div class="info-box-content">--}}
{{--                    <span class="info-box-text">Inventory</span>--}}
{{--                    <span class="info-box-number">5,200</span>--}}
{{--                </div>--}}
{{--                <!-- /.info-box-content -->--}}
{{--            </div>--}}
{{--            <!-- /.info-box -->--}}
{{--            <div class="info-box mb-3 bg-success">--}}
{{--                <span class="info-box-icon"><i class="far fa-heart"></i></span>--}}

{{--                <div class="info-box-content">--}}
{{--                    <span class="info-box-text">Wastage</span>--}}
{{--                    <span class="info-box-number">92,050</span>--}}
{{--                </div>--}}
{{--                <!-- /.info-box-content -->--}}
{{--            </div>--}}
{{--            <!-- /.info-box -->--}}
{{--            <div class="info-box mb-3 bg-danger">--}}
{{--                <span class="info-box-icon"><i class="fas fa-cloud-download-alt"></i></span>--}}

{{--                <div class="info-box-content">--}}
{{--                    <span class="info-box-text">Credit GRN Amounts</span>--}}
{{--                    <span class="info-box-number">114,381</span>--}}
{{--                </div>--}}
{{--                <!-- /.info-box-content -->--}}
{{--            </div>--}}
{{--            <!-- /.info-box -->--}}
{{--            <div class="info-box mb-3 bg-info">--}}
{{--                <span class="info-box-icon"><i class="far fa-comment"></i></span>--}}

{{--                <div class="info-box-content">--}}
{{--                    <span class="info-box-text">This Month Expenses</span>--}}
{{--                    <span class="info-box-number">163,921</span>--}}
{{--                </div>--}}
{{--                <!-- /.info-box-content -->--}}
{{--            </div>--}}

{{--        </section>--}}


{{--    </div>--}}






@endsection
