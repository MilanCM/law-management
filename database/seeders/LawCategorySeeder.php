<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LawCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('law_categories')->insert([
            'law_field'=>'Criminal',
            'description'=>'"Lorem ipsum dolor sit amet',
//            'parent_id'=>'NULL',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('law_categories')->insert([
            'law_field'=>'Cyber',
            'description'=>'"Lorem ipsum dolor sit amet',
//            'parent_id'=>'NULL',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('law_categories')->insert([
            'law_field'=>'Cyber sub 01',
            'description'=>'"Lorem ipsum dolor sit amet',
            'parent_id'=>'2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('law_categories')->insert([
            'law_field'=>'Cyber sub 2',
            'description'=>'"Lorem ipsum dolor sit amet',
            'parent_id'=>'2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('law_categories')->insert([
            'law_field'=>'Cyber sub 2.1',
            'description'=>'"Lorem ipsum dolor sit amet',
            'parent_id'=>'4',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
