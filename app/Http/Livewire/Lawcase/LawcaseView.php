<?php

namespace App\Http\Livewire\Lawcase;

use App\Models\CaseUpdateDetail;
use App\Models\LawCase;
use App\Models\LawCaseMedia;
use App\Models\TemporaryFile;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class LawcaseView extends Component
{
    public $all_cases;
    public $searched_case_id;

    public
    function mount()
    {
        $this->all_cases = LawCase::where('is_active', '1')->get();
    }

    public function render()
    {
        $searched_law_case = null;
        $upgraded_cases = [];
        $upgrade_images = [];
        $img_model = [];
        $this->arr_witness = [];
        if ($this->searched_case_id) {
            $searched_law_case = LawCase::find($this->searched_case_id);
            $upgraded_cases = CaseUpdateDetail::where('law_case_id', $searched_law_case['id'])->get();
            foreach ($upgraded_cases as $case) {
                $upgrade_images = LawCaseMedia::where('law_case_id', $searched_law_case['id'])
                    ->where('law_case_upgrade_id', $case['id'])->get();
                foreach ($upgrade_images as $image) {
                    $arr = [
                        'law_case_id' => $image['law_case_id'],
                        'file_type' => $image['file_type'],
                        'file_path' => $image['file_path'],
                        'law_case_upgrade_id' => $image['law_case_upgrade_id'],
                    ];
                    array_push($img_model, $arr);
                }
            }
        }
        return view('livewire.lawcase.lawcase-view', ['searched_law_case' => $searched_law_case,
            'upgraded_cases' => $upgraded_cases, 'img_model' => $img_model])->layout('layouts.main');
    }
}
