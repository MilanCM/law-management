<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inventory | Control</title>

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">

    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">


    <link href="{{ asset('css/pushmenu.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.css')}}">
    <link rel="stylesheet" href="{{asset('backend/plugins/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="resources/sass/app.scss">
    <!-- select2 css -->
    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('select2/select2-bootstrap4.css') }}">
    <!-- file fond css -->
    <link href="{{ asset('vendor/filepond/filepond.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.css') }}" rel="stylesheet">
    @stack('styles')
    @livewireStyles
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

    <!-- Navbar -->
@include('layouts.header')
<!-- /.navbar -->

    <!-- Main Sidebar Container -->
@include('layouts.side-nav')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                @yield('header')

            </div><!-- /.container-fluid -->

        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
            @yield('content')
            {{$slot}}
            <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
            <h5>Title</h5>
            <p>Sidebar content</p>
        </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Main Footer -->
    @include('layouts.footer')
</div>
<!-- ./wrapper -->

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="card card-primary card-outline direct-chat direct-chat-primary">
                <div class="card-header">
                    <h3 class="card-title">Direct Chat</h3>
                    <div class="card-tools">
                        <span title="3 New Messages" class="badge bg-primary">3</span>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" title="Contacts" data-widget="chat-pane-toggle">
                            <i class="fas fa-comments"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>

                <div class="card-body">

                    <div class="direct-chat-messages">

                        <div class="direct-chat-msg">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name float-left">Alexander Pierce</span>
                                <span class="direct-chat-timestamp float-right">23 Jan 2:00 pm</span>
                            </div>
                            <img class="direct-chat-img" src="/avatar/avatar1.png" alt="Message User Image">
                            <div class="direct-chat-text">
                                Is this template really for free? That's unbelievable!
                            </div>
                        </div>

                        <div class="direct-chat-msg right">
                            <div class="direct-chat-infos clearfix">
                                <span class="direct-chat-name float-right">Sarah Bullock</span>
                                <span class="direct-chat-timestamp float-left">23 Jan 2:05 pm</span>
                            </div>

                            <img class="direct-chat-img" src="/avatar/avatar2.png" alt="Message User Image">

                            <div class="direct-chat-text">
                                You better believe it!
                            </div>

                        </div>

                    </div>


                    <div class="direct-chat-contacts">
                        <ul class="contacts-list">
                            <li>
                                <a href="#">
                                    <img class="contacts-list-img" src="/avatar/avatar1.png"
                                         alt="User Avatar">
                                    <div class="contacts-list-info">
                                        <span class="contacts-list-name">Kamal<small
                                                class="contacts-list-date float-right">2/28/2015</small></span>
                                        <span class="contacts-list-msg">How have you been? I was...</span>
                                    </div>

                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="contacts-list-img" src="/avatar/avatar2.png"
                                         alt="User Avatar">
                                    <div class="contacts-list-info">
                                        <span class="contacts-list-name">Chathuranga<small
                                                class="contacts-list-date float-right">2/28/2015</small></span>
                                        <span class="contacts-list-msg">How have you been? I was...</span>
                                    </div>

                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img class="contacts-list-img" src="/avatar/avatar3.png"
                                         alt="User Avatar">
                                    <div class="contacts-list-info">
                                        <span class="contacts-list-name">Milan<small
                                                class="contacts-list-date float-right">2/28/2015</small></span>
                                        <span class="contacts-list-msg">How have you been? I was...</span>
                                    </div>

                                </a>
                            </li>
                        </ul>

                    </div>

                </div>

                <div class="card-footer">
                    <form action="#" method="post">
                        <div class="input-group">
                            <input type="text" name="message" placeholder="Type Message ..." class="form-control">
                            <span class="input-group-append">
<button type="submit" class="btn btn-primary">Send</button>
</span>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
@livewireScripts
<script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>
<script src="{{asset('backend/plugins/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- select2 js -->
<script src="{{ asset('select2/select2.min.js') }}"></script>
<!-- file fond js -->
<script src="{{ asset('vendor/filepond/filepond.min.js') }}"></script>
<script src="{{ asset('vendor/filepond/filepond.jquery.js') }}"></script>
<script src="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.js') }}"></script>
<script src="{{ asset('vendor/filepond/filepond-plugin-file-validate-size.min.js') }}"></script>
<script src="{{ asset('vendor/filepond/filepond-plugin-file-encode.min.js') }}"></script>

<script>

    (function ($) {
        this.MobileNav = function () {
            this.curItem,
                (this.curLevel = 0),
                (this.transitionEnd = _getTransitionEndEventName());

            var defaults = {
                initElem: ".main-menu",
                menuTitle: "Menu"
            };

            // Check if MobileNav was initialized with some options and assign them to the "defaults"
            if (arguments[0] && typeof arguments[0] === "object") {
                this.options = extendDefaults(defaults, arguments[0]);
            }

            // Add to the "defaults" ONLY if the key is already in the "defaults"
            function extendDefaults(source, extender) {
                for (option in extender) {
                    if (source.hasOwnProperty(option)) {
                        source[option] = extender[option];
                    }
                }
            }

            MobileNav.prototype.getCurrentItem = function () {
                return this.curItem;
            };

            MobileNav.prototype.setMenuTitle = function (title) {
                defaults.menuTitle = title;
                _updateMenuTitle(this);
                return title;
            };

            // Init is an anonymous IIFE
            (function (MobileNav) {
                var initElem = $(defaults.initElem).length ? $(defaults.initElem) : false;

                if (initElem) {
                    defaults.initElem = initElem;
                    _clickHandlers(MobileNav);
                    _updateMenuTitle(MobileNav);
                } else {
                    console.log(
                        defaults.initElem + " element doesn't exist, menu not initialized."
                    );
                }
            })(this);

            function _getTransitionEndEventName() {
                var i,
                    undefined,
                    el = document.createElement("div"),
                    transitions = {
                        transition: "transitionend",
                        OTransition: "otransitionend", // oTransitionEnd in very old Opera
                        MozTransition: "transitionend",
                        WebkitTransition: "webkitTransitionEnd"
                    };

                for (i in transitions) {
                    if (transitions.hasOwnProperty(i) && el.style[i] !== undefined) {
                        return transitions[i];
                    }
                }
            }

            function _clickHandlers(menu) {
                defaults.initElem.on("click", ".has-dropdown > a", function (e) {
                    e.preventDefault();
                    menu.curItem = $(this).parent();
                    _updateActiveMenu(menu);
                });

                defaults.initElem.on("click", ".pushnav-toggle", function () {
                    _updateActiveMenu(menu, "back");
                });
            }

            // TODO: Make this DRY (deal with waiting for transitionend event)
            function _updateActiveMenu(menu, direction) {
                _slideMenu(menu, direction);
                if (direction === "back") {
                    /*defaults.initElem.children('ul').one(menu.transitionEnd, function(e) {
                                            menu.curItem.removeClass('nav-dropdown-open nav-dropdown-active');
                                            menu.curItem = menu.curItem.parent().closest('li');
                                            menu.curItem.addClass('nav-dropdown-open nav-dropdown-active');
                                            _updateMenuTitle(menu);
                                    });*/

                    menu.curItem.removeClass("nav-dropdown-open nav-dropdown-active");
                    menu.curItem = menu.curItem.parent().closest("li");
                    menu.curItem.addClass("nav-dropdown-open nav-dropdown-active");
                    _updateMenuTitle(menu);
                } else {
                    menu.curItem.addClass("nav-dropdown-open nav-dropdown-active");
                    _updateMenuTitle(menu);
                }
            }

            // Update main menu title to be the text of the clicked menu item
            function _updateMenuTitle(menu) {
                var title = defaults.menuTitle;
                if (menu.curLevel > 0) {
                    title = menu.curItem.children("a").text();
                    defaults.initElem.find(".pushnav-toggle").addClass("back-visible");
                } else {
                    defaults.initElem.find(".pushnav-toggle").removeClass("back-visible");
                }
                $(".pushnav-title").text(title);
            }

            // Slide the main menu based on current menu depth
            function _slideMenu(menu, direction) {
                if (direction === "back") {
                    menu.curLevel = menu.curLevel > 0 ? menu.curLevel - 1 : 0;
                } else {
                    menu.curLevel += 1;
                }
                defaults.initElem.children(".pushul").css({
                    transform: "translateX(-" + menu.curLevel * 100 + "%)"
                });
            }
        };
    })(jQuery);

    $(document).ready(function () {
        var MobileMenu = new MobileNav({
            initElem: "nav",
            menuTitle: "Category"
        });

        $(".pushjs-nav-toggle").on("click", function (e) {
            e.preventDefault();

            $(".pushnav-wrapper").toggleClass("show-menu");
        });
    });


</script>

@stack('scripts')
</body>
</html>
