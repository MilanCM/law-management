<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class ClientPaymentManagement extends Component
{
    public function render()
    {
        return view('livewire.account.client-payment-management')->layout('layouts.main');
    }
}
