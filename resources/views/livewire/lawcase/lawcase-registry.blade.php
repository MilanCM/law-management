@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Case Registry</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Case Registry</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>
    {{--    startSearchCaseSection--}}
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="table_search"
                       class="form-control form-control-border border-width-2 bg-transparent"
                       placeholder="Search" wire:model="case_search_key">
            </div>
        </div>
    </div>
    <div class="row">
    </div>
    <div class="card card-info mt-3">
        <div class="card-header">
            <h3 class="card-title">Cases List</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
            <table class="table-striped table-sm table">
                <thead style="background-color: rgba(185,202,209,0.62)">
                <tr>
                    {{--                    <th>#</th>--}}
                    <th>Case Number</th>
                    <th>Court</th>
                    <th>Open Date</th>
                    <th>Client</th>
                    <th>Assinged Lawyer</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($cases as $key=>$case)
                    <tr>
                        {{--                        <td>{{$key+1}}</td>--}}
                        <td><small>{{$case->case_number}}</small></td>
                        <td><small>{{$case->court_location}}</small></td>
                        <td><small>{{$case->case_opened_date}}</small></td>
                        @if($case->client_person)
                            <td><small>{{$case->client_person->full_name}}<br>
                                    {{$case->client_person->nic}}</small>
                            </td>
                        @elseif($case->client_company)
                            <td><small>{{$case->client_company->company_name}}<br>
                                    {{$case->client_company->br_number}}
                                </small></td>
                        @endif
                        <td><small>{{$case->assigned_lawyer->f_name}}. {{$case->assigned_lawyer->l_name}}<br>
                                {{$case->assigned_lawyer->license_no}}</small>

                        </td>
                        @if($case->is_active == 1)
                            <td><span class="badge badge-success mt-3">Active</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-xs mt-2"
                                        wire:click="DeactivateSelectedCaseDetails({{$case}})"><i
                                        class="fa fa-trash"
                                        aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-outline-info btn-xs mt-2"
                                        wire:click="LoadSelectedCaseDetails({{$case}})"><i
                                        class="fa fa-edit"
                                        aria-hidden="true"></i>
                                </button>

                            </td>

                        @else
                            <td><span class="badge badge-danger">Deactive</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-success btn-xs mt-2"
                                        wire:click="ActivateSelectedCaseDetails({{$case}})"><i
                                        class="fa fa-trash-restore"
                                        aria-hidden="true"></i>
                                </button>
                                <button type="button" class="btn btn-outline-info btn-xs mt-2"
                                        wire:click="LoadSelectedCaseDetails({{$case}})"><i
                                        class="fa fa-edit"
                                        aria-hidden="true"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <!-- /.card-body -->
    </div>
    <div class="mt-2">
        {{$cases->links()}}
    </div>

    {{--    endSearchCaseSection--}}

    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Case Registry Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        @if (\Session::has('message'))
            <script>
                $(window).scrollTop(10);
            </script>
            {{--        @if ($message = Session::get('success'))--}}
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            {{--        @endif--}}
        @endif

        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Case Number<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2 @error('case_number') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="crim-20220802" wire:model="case_number">
                        @error('case_number') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Court Location<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2 @error('case_location') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="High Court" wire:model="case_location">
                        @error('case_location') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Case Opened Date<code> *</code></label>
                        <input type="date"
                               class="form-control form-control-sm form-control-border border-width-2 @error('case_open_date') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="2022/08/02" wire:model="case_open_date">
                        @error('case_open_date') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group ">
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2 bg-transparent @error('case_law_field') is-invalid @enderror"
                               contenteditable="false"
                               disabled
                               wire:model="case_law_field" placeholder="category">
                        @error('case_law_field') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
                <div class="col-md-4">
                    <label></label>
                    <button type="button" class="btn btn-sm btn-outline-info" data-toggle="modal"
                            data-target="#category_selector_modal">
                        <i
                            class="fa fa-plus"
                            aria-hidden="true"></i> Select Law Field</category>
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="mb-sm-0">Client/ Company Details<code> *</code></label>
                        <div class="col-sm-10" wire:ignore>
                            <select
                                class="custom-select @if(sizeof($arr_model) === 0 || $errors->first('arr_model'))
                                    is-invalid @endif" id="case_assigned_client" data-placeholder="Choose anything"
                                data-allow-clear="1"
                                @if(is_null($arr_model)) disabled @endif>
                                @if(sizeof($arr_model) !== 0)
                                    <option></option>
                                    @foreach($arr_model as $client)
                                        <option
                                            value="{{ $client['id']}}/{{$client['client_type']}}">{{ $client['f_name']}}
                                            -{{$client['nic']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @error('case_assigned_client_id') <span
                            class="text-danger"><small><strong>{{$message}}</strong></small></span> @enderror
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="form-group mt-3">
                        <div class="user-block">
                            @if($case_assigned_client_id !== null)
                                <img class="img-circle" src="{{asset('avatar/avatar2.png')}}"
                                     alt="User Image">
                                <span class="username"><a href="#">{{$selected_client[0]['f_name']}}</a></span>
                                <span
                                    class="description">{{$selected_client[0]['nic']}} -
                                    {{$selected_client[0]['contact_1']}}</span>
                            @else
                                <img class="img-circle" src="{{asset('avatar/avatar2.png')}}"
                                     alt="User Image">
                                <span class="username"><a href="#">S.S.S.L.Lakmal</a></span>
                                <span class="description">940785214V - 0114787589</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="mb-sm-0">Assigned Lawyer<code> *</code></label>
                        <div class="col-sm-10" wire:ignore>
                            <select
                                class="custom-select @if(sizeof($case_assigned_lawyers) === 0 || $errors->first('case_assigned_lawyers'))
                                    is-invalid @endif"
                                id="case_assigned_lawyer" data-placeholder="Choose anything" data-allow-clear="1"
                                @if(is_null($case_assigned_lawyers)) disabled @endif>
                                @if(sizeof($case_assigned_lawyers) !== 0)
                                    <option></option>
                                    @foreach($case_assigned_lawyers as $case_assigned_lawyer)
                                        <option
                                            value="{{ $case_assigned_lawyer['id']}}">{{ $case_assigned_lawyer['f_name']}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @error('case_assigned_lawyer_id') <span
                            class="text-danger"><small><strong>{{$message}}</strong></small></span> @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group mt-3">
                        <div class="user-block">
                            @if($case_assigned_lawyer_id == null)
                                <img class="img-circle" src="{{asset('avatar/avatar1.png')}}"
                                     alt="User Image">
                                <span class="username"><a href="#">S.S.S.L.Lakamal</a></span>
                                <span class="description">A Lawyer - lw20220802</span>
                            @else
                                <img class="img-circle" src="{{asset('avatar/avatar1.png')}}"
                                     alt="User Image">
                                <span class="username"><a
                                        href="#">{{$selected_assigned_lawyer_initial_name}}</a></span>
                                <span class="description">{{$selected_assigned_lawyer->lawyerCategory['category_name']}} -
                                    {{$selected_assigned_lawyer['license_no']}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="mb-sm-0">Registered Attorney<code> *</code></label>
                        <div class="col-sm-10" wire:ignore>
                            <select
                                class="custom-select @if(sizeof($case_assigned_lawyers) === 0 || $errors->first('case_assigned_lawyers'))
                                    is-invalid @endif" id="case_assigned_attorney"
                                data-placeholder="Choose anything"
                                data-allow-clear="1"
                                @if(is_null($case_assigned_lawyers)) disabled @endif>
                                @if(sizeof($case_assigned_lawyers) !== 0)
                                    <option></option>
                                    @foreach($case_assigned_lawyers as $case_assigned_lawyer)
                                        <option
                                            value="{{ $case_assigned_lawyer['id']}}">{{ $case_assigned_lawyer['f_name']}}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @error('case_registered_attorney_id') <span
                            class="text-danger"><small><strong>{{$message}}</strong></small></span> @enderror
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group mt-3">
                        <div class="user-block">
                            @if($case_registered_attorney_id == null)
                                <img class="img-circle" src="{{asset('avatar/avatar3.png')}}"
                                     alt="User Image">
                                <span class="username"><a href="#">S.S.S.L.Lakamal</a></span>
                                <span class="description">A Lawyer - lw20220802</span>
                            @else
                                <img class="img-circle" src="{{asset('avatar/avatar3.png')}}"
                                     alt="User Image">
                                <span class="username"><a
                                        href="#">{{$selected_registered_attorney_initial_name}}</a></span>
                                <span class="description">{{$selected_registered_attorney->lawyerCategory['category_name']}} -
                                    {{$selected_registered_attorney['license_no']}}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="inputDescription">Description</label>
                        <textarea id="inputDescription"
                                  class="form-control form-control-sm form-control-border border-width-2 @error('case_description') is-invalid @enderror"
                                  rows="6"
                                  wire:model="case_description"></textarea>
                        @error('case_description') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputDescription">Comments</label>
                        <textarea id="inputComment"
                                  class="form-control form-control-sm form-control-border border-width-2 @error('case_comment') is-invalid @enderror"
                                  rows="4"
                                  wire:model="case_comment"></textarea>
                        @error('case_comment') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
            </div>


            <div class="form-group" wire:ignore>
                <input type="file" name="avatar" id="avatar" multiple accept="">
            </div>

            @if($images)
                <div class="d-flex flex-wrap">
                    @foreach($images as $image)
                        <div class="card mr-2 d-flex flex-column justify-content-between">
                            @if($image['file_type'] === 'jpg' || $image['file_type'] === 'png' ||$image['file_type'] === 'jpeg')
                                <img style="width: 150px; height: 150px"
                                     src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}"
                                     alt="...">
                                <a class="badge badge-pill badge-danger text-center"
                                   wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>
                            @elseif($image['file_type'] === 'ogg'||$image['file_type'] === 'mp3'|| $image['file_type'] === 'm4a')
                                <audio controls style="width: 150px; height: 150px">
                                    <source
                                        src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}">
                                </audio>
                                <a class="badge badge-pill badge-danger text-center"
                                   wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>
                            @elseif($image['file_type']=='pptx' ||$image['file_type']=='pdf' ||$image['file_type']=='docx' ||$image['file_type']=='txt')
                                <iframe
                                    src="https://drive.google.com/file/d/19ViaXuJzhlIyMMZkgeLx1eFD4-xoAaFk/view"
                                    frameborder="0">
                                </iframe>
                            @endif
                        </div>
                    @endforeach
                </div>
            @endif



            {{--            <div class="form-group">--}}
            {{--                <label class="mb-sm-0">Image Upload</label>--}}
            {{--                <input type="file" wire:model="images" accept="image/*" multiple>--}}
            {{--                @if ($images)--}}
            {{--                    <div class="mt-3 mb-3">--}}
            {{--                        @foreach($images as $image)--}}
            {{--                            <img class="profile-user-img img-fluid"--}}
            {{--                                 style="background-size:cover;width:150px;height:150px;"--}}
            {{--                                 src="{{ $image->temporaryUrl() }}"--}}
            {{--                                 alt="img-fluid">--}}
            {{--                        @endforeach--}}
            {{--                    </div>--}}
            {{--                @endif--}}
            {{--                @error('image') <span class="error">{{ $message }}</span> @enderror--}}
            {{--            </div>--}}
            {{--            --}}
            {{--            <div class="form-group">--}}
            {{--                <label class="mb-sm-0">Document Upload</label>--}}
            {{--                <input type="file" wire:model="documents" multiple accept=".pdf,.doc,.xlsx,.xls,.csv,.txt,.docx">--}}
            {{--                @error('documents') <span class="error">{{ $message }}</span> @enderror--}}
            {{--            </div>--}}

            {{--            <div class="form-group">--}}
            {{--                <label class="mb-sm-0">Audio Upload</label>--}}
            {{--                <input type="file" wire:model="audios" multiple accept=".mp3, .wav">--}}
            {{--                @if($audios)--}}
            {{--                    <div class="mt-3 mb-3">--}}
            {{--                        @foreach( $audios as $audio)--}}
            {{--                            <audio controls>--}}
            {{--                                --}}{{--                                        <source src="horse.ogg" type="audio/ogg">--}}
            {{--                                <source src="{{ $audio->temporaryUrl() }}" >--}}
            {{--                            </audio><br>--}}
            {{--                        @endforeach--}}
            {{--                    </div>--}}
            {{--                @endif--}}
            {{--                @error('audios') <span class="error">{{ $message }}</span> @enderror--}}
            {{--            </div>--}}


            <div class="col-12 d-flex align-items-stretch flex-column mt-3">
                <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0 text-bold">
                        <u> Witnesses Details</u>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">First Name<code> *</code></label>
                                    <input type="text"
                                           class="form-control form-control-sm form-control-border border-width-2 @error('case_witness_f_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Saman" wire:model="case_witness_f_name">
                                    @error('case_witness_f_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">Middle Name / Names</label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_m_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Sampath" wire:model="case_witness_m_name">
                                    @error('case_witness_m_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">Last Name</label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_l_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Werasuriya" wire:model="case_witness_l_name">
                                    @error('case_witness_l_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="mb-sm-0">Family Name</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_family_name') is-invalid @enderror bg-transparent"
                                                   id="exampleInputEmail1"
                                                   placeholder="Werasuriya Arachchilage"
                                                   wire:model="case_witness_family_name">
                                            @error('case_witness_family_name') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="mb-sm-0">NIC Number</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_nic') is-invalid @enderror bg-transparent"
                                                   id="exampleInputPassword1"
                                                   placeholder="Enter NIC" wire:model="case_witness_nic">
                                            @error('case_witness_nic') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="mb-sm-0">Relationship</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_relationship') is-invalid @enderror bg-transparent"
                                                   id="exampleInputPassword1"
                                                   placeholder="Enter NIC" wire:model="case_witness_relationship">
                                            @error('case_witness_relationship') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="mb-sm-0">Email</label>
                                    <input type="email"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_email') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Enter email" wire:model="case_witness_email">
                                    @error('case_witness_email') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                                <div class="form-group">
                                    <label class="mb-sm-0">Contact</label>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>
                                                <input type="text"
                                                       class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_contact_1') is-invalid @enderror bg-transparent"
                                                       placeholder="contact 1"
                                                       wire:model="case_witness_contact_1">
                                            </label>
                                            @error('case_witness_contact_1') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-2">
                                            <label>
                                                <input type="text"
                                                       class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_contact_2') is-invalid @enderror bg-transparent"
                                                       placeholder="contact 2"
                                                       wire:model="case_witness_contact_2">
                                            </label>
                                            @error('case_witness_contact_2') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="mb-sm-0">Address</label>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_no') is-invalid @enderror bg-transparent"
                                                   placeholder="no."
                                                   wire:model="case_witness_no">
                                            @error('case_witness_no') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_street_1') is-invalid @enderror bg-transparent"
                                                   placeholder="street1"
                                                   wire:model="case_witness_street_1">
                                            @error('case_witness_street_1') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_street_2') is-invalid @enderror bg-transparent"
                                                   placeholder="street2"
                                                   wire:model="case_witness_street_2">
                                            @error('case_witness_street_2') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('case_witness_city') is-invalid @enderror bg-transparent"
                                                   placeholder="city"
                                                   wire:model="case_witness_city">
                                            @error('case_witness_city') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <button wire:click.prevent="ClearWitnessFields()" class="btn btn-sm btn-outline-dark">
                                <i class="fas fa-eraser"></i> Reset
                            </button>
                            <button wire:click.prevent="AddWitness()" class="btn btn-sm btn-primary">
                                <i class="fas fa-user"></i> Add Witness
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            @if(sizeof($arr_witness) !== 0)
                <div>
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 30%">
                                Witness Details
                            </th>
                            <th style="width: 20%">
                                Contact Details
                            </th>
                            <th style="width: 15%">
                                Address
                            </th>
                            <th style="width: 8%" class="text-center">
                                NIC
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arr_witness as $key=> $witness)

                            <tr>
                                <td>
                                    {{$key+1}}
                                </td>
                                <td>
                                    <a>
                                        {{$witness['f_name']}} {{$witness['m_name']}} {{$witness['l_name']}}
                                    </a>
                                    <br>
                                    <small>
                                        {{$witness['family_name']}}
                                    </small>
                                </td>
                                <td>
                                    <small> {{$witness['contact_1']}} @if($witness['contact_2'] !== null)
                                            / {{$witness['contact_2']}}@endif</small><br>
                                    <small>
                                        {{$witness['email']}}
                                    </small>
                                </td>
                                <td>
                                    <small>{{$witness['no']}}, {{$witness['street_1']}}, </small><br>
                                    <small></small>@if($witness['street_2'] !== null){{$witness['street_2']}}
                                    ,@endif{{$witness['city']}}</small>
                                </td>
                                <td class="project-state">
                                    <small>
                                        {{$witness['nic']}}
                                    </small>
                                </td>
                                <td class="project-actions text-right">
                                    <button class="btn btn-info btn-sm" wire:click="witnessEditArray({{$key}})"><i
                                            class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-danger btn-sm" wire:click="witnessRemoveArray({{$key}})">
                                        <i
                                            class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            <div class="row">
                <div class="card-footer">
                    <button id="save_case" class="btn btn-primary" wire:click.prevent="InsertCaseDetails()">Add Case
                    </button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="UpdateCaseDetails()">Update</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger" wire:click.prevent="itemDelete()">Delete</button>
                </div>
            </div>

            <!--/ .Category list modal-->
            <div wire:ignore.self class="modal fade show" id="category_selector_modal"
                 style="display: none; padding-right: 17px;"
                 aria-modal="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Select Category</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card-body">
                                <!--/ .category list-->

                                <div class="mt-3 mb-3">
                                    <ul class="list-group">
                                        <li class="list-group-item active">
                                            @if($parent_category)
                                                <i class="fas fa-arrow-circle-left" wire:click="rollBack()"></i>
                                            @endif
                                            {{$parent_category ? $parent_category['law_field'] : 'Category'}}
                                        </li>
                                        @if(count($categories)> 0)
                                            @foreach($categories as $category)

                                                <li class="list-group-item d-flex justify-content-between align-items-center"
                                                    wire:click.prevent="fillMainCategory({{$category}})">

                                                    {{$category['law_field']}}

                                                    @if(count($category->children) >0)
                                                        <i class="fas fa-arrow-circle-right"></i>
                                                    @endif
                                                </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Selected Category:</label>
                                    <input type="text" class="form-control" contenteditable="false" disabled
                                           wire:model="parent_category.law_field">
                                </div>

                                <!--/ .category list-->
                            </div>
                        </div>
                        <div class="modal-footer justify-content-end">
                            <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Done</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!--/ .Category list modal-->
        </div>
    </div>
</main>

@push('styles')
    <style>
        .details {
            display: flex;
            align-items: center;
        }

        .detail-col {
            border-right: 1px solid rgba(222, 226, 230, 0.3);
        }
    </style>
@endpush

@push('scripts')
    <script>
        $('#case_assigned_lawyer').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#case_assigned_lawyer').on('change', function () {
        @this.case_assigned_lawyer_id
            = $(this).val();
        });
        $('#case_assigned_attorney').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#case_assigned_attorney').on('change', function () {
        @this.case_registered_attorney_id
            = $(this).val();
        });
        $('#case_assigned_client').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#case_assigned_client').on('change', function () {
        @this.case_assigned_client_id
            = $(this).val();
        @this.selected_client
            = [];
        });

        /**
         * file fond
         */
        $.fn.filepond.registerPlugin(
            // previews dropped images
            // FilePondPluginImagePreview,
            // validates the size of the file
            FilePondPluginFileValidateSize,
            // encodes the file as base64 data
            FilePondPluginFileEncode
        );
        //
        const inputElement = document.querySelector('input[type="file"]');
        // // Create a FilePond instance
        const pond = FilePond.create(inputElement);

        FilePond.setOptions({
            server: {
                url: '../upload/',
                revert: '../upload/',
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            }
        });

        // $('#save_case').click(function (event) {
        //     pond.removeFiles();
        // });

        // if (Session::has('type')) {
        //     pond.removeFiles();
        // }

    </script>
@endpush


