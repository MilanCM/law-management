<?php

namespace App\Http\Livewire\Appointment;

use Livewire\Component;

class AppointmentManagement extends Component
{
    public function render()
    {
        return view('livewire.appointment.appointment-management')->layout('layouts.main');
    }
}
