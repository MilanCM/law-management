<?php

namespace App\Http\Livewire\Settings;

use Livewire\Component;

class PrivilegesManagement extends Component
{
    public function render()
    {
        return view('livewire.settings.privileges-management')->layout('layouts.main');
    }
}
