<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class ExpensesManagement extends Component
{
    public function render()
    {
        return view('livewire.account.expenses-management')->layout('layouts.main');
    }
}
