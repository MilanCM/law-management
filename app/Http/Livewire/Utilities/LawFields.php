<?php

namespace App\Http\Livewire\Utilities;

use Livewire\Component;

class LawFields extends Component
{
    public function render()
    {
        return view('livewire.utilities.law-fields')->layout('layouts.main');
    }
}
