<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class Employee extends Model
{
    use HasFactory;

    public function employee_office()
    {
        return $this->belongsTo(Office::class);
    }
    public function user_role()
    {
        return $this->belongsTo(UserRole::class, 'user_role_id', 'id');
    }
}



