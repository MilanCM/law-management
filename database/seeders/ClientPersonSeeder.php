<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientPersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'f_name' => 'Milan',
            'm_name' => 'Chathuranga',
            'l_name' => 'Madiwela',
            'sur_name' => 'Palamurayalage',
            'initial_name' => 'p M C Madiwela',
            'full_name' => 'Palamurayalage Milan Chathuranga Madiwela',
            'nic' => '940790123V',
            'no' => '45',
            'street_1' => 'Ranwala',
            'street_2' => 'Beligala',
            'city' => 'Kegalle',
            'occupation' => 'Clerk',
            'nationality' => 'Sinhalese',
            'contact_1' => '0714578458',
            'contact_2' => '0774587956',
            'email' => 'milan@gmail.com',
            'company_name' => 'GSS',
            'company_contact_1' => '0374785458',
            'company_contact_2' => '0715263586',
            'company_email' => 'gss@gmail.com',
            'gender' => 'Male',
            'marriage_status' => 'Single',
            'is_active' => '1',
            'office_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);


        DB::table('customers')->insert([
            'f_name' => 'Kasun',
            'm_name' => 'Jewantha',
            'l_name' => 'Madiwela',
            'sur_name' => 'Palamurayalage',
            'initial_name' => 'p K J Madiwela',
            'full_name' => 'Palamurayalage Kasun Jewantha Madiwela',
            'nic' => '940788823V',
            'no' => '45',
            'street_1' => 'Kudawella',
            'street_2' => 'Galbage',
            'city' => 'Mathara',
            'occupation' => 'Supervisor',
            'nationality' => 'Sinhalese',
            'contact_1' => '0714456458',
            'contact_2' => '0777895956',
            'email' => 'kasun@gmail.com',
            'company_name' => 'IMML',
            'company_contact_1' => '0114785458',
            'company_contact_2' => '0714885586',
            'company_email' => 'imml@gmail.com',
            'gender' => 'Male',
            'marriage_status' => 'Single',
            'is_active' => '1',
            'office_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
