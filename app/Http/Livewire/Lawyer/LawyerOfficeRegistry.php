<?php

namespace App\Http\Livewire\Lawyer;

use App\Models\Office;
use Livewire\Component;
use Livewire\WithPagination;

class LawyerOfficeRegistry extends Component
{
    public $office_name;
    public $office_email;
    public $office_contact1;
    public $office_contact2;
    public $office_no;
    public $office_street1;
    public $office_street2;
    public $office_city;
    public $catchKey;
    public $selected_lawyer_office_id;
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

// startLawyerOfficeDetailsInsert
    public function LawyerOfficeDetailsInsert()
    {
        $lw_office = new Office();
        $lw_office->office_name = $this->office_name;
        $lw_office->contact_1 = $this->office_contact1;
        $lw_office->contact_2 = $this->office_contact2;
        $lw_office->no = $this->office_no;
        $lw_office->street_1 = $this->office_street1;
        $lw_office->street_2 = $this->office_street2;
        $lw_office->city = $this->office_city;
        $lw_office->save();
        $this->clearFieldLawyerOfficeDetails();
    }//    endLawyerOfficeDetailsInsert

// startLawyerOfficeDetailLoad
    public function loadLawyerOfficeDetail($selected_lawyer_office)
    {
        $this->selected_lawyer_office_id = $selected_lawyer_office['id'];
        $this->office_name = $selected_lawyer_office['office_name'];
        $this->office_contact1 = $selected_lawyer_office['contact_1'];
        $this->office_contact2 = $selected_lawyer_office['contact_2'];
        $this->office_no = $selected_lawyer_office['no'];
        $this->office_street1 = $selected_lawyer_office['street_1'];
        $this->office_street2 = $selected_lawyer_office['street_2'];
        $this->office_city = $selected_lawyer_office['city'];
    }//    endLawyerOfficeDetailLoad

// startLawyerOfficeDetailsUpdate
    public function LawyerOfficeDetailsUpdate()
    {
        $selected_lawyer_office = Office::find($this->selected_lawyer_office_id);
        $selected_lawyer_office->office_name = $this->office_name;
        $selected_lawyer_office->contact_1 = $this->office_contact1;
        $selected_lawyer_office->contact_2 = $this->office_contact2;
        $selected_lawyer_office->no = $this->office_no;
        $selected_lawyer_office->street_1 = $this->office_street1;
        $selected_lawyer_office->street_2 = $this->office_street2;
        $selected_lawyer_office->city = $this->office_city;
        $selected_lawyer_office->save();
        $this->clearFieldLawyerOfficeDetails();
    }// endLawyerOfficeDetailsUpdate

// startLawyerOfficeActive
    public function LawyerOfficeActivate($selected_lawyer_office_id)
    {
        $office = Office::find($selected_lawyer_office_id);
        $office->is_active = '1';
        $office->save();
        $this->clearFieldLawyerOfficeDetails();
    }//    endLawyerOfficeActive

// startLawyerOfficeDeactivate
    public function LawyerOfficeDeactivate($selected_lawyer_office_id)
    {
        $office = Office::find($selected_lawyer_office_id);
        $office->is_active = '0';
        $office->save();
        $this->clearFieldLawyerOfficeDetails();
    }//    endLawyerOfficeDeactivate

// startClearFieldLawyerOfficeDetails
    public function clearFieldLawyerOfficeDetails()
    {
        $this->office_name = '';
        $this->office_contact1 = '';
        $this->office_contact2 = '';
        $this->office_no = '';
        $this->office_street1 = '';
        $this->office_contact2 = '';
        $this->office_city = '';
    }//    endClearFieldLawyerOfficeDetails

// startRenderMethod
    public function render()
    {
        $lawyer_offices = Office::where('office_name', 'like', '%' . $this->catchKey . '%')
            ->orwhere('city', 'like', '%' . $this->catchKey . '%')->paginate(4);
        return view('livewire.lawyer.lawyer-office-registry', ['lawyer_offices' => $lawyer_offices])->layout('layouts.main');
    }
}
