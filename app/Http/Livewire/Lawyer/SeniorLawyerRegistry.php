<?php

namespace App\Http\Livewire\Lawyer;

use App\Models\Lawyer;
use App\Models\LawyerCategory;
use Livewire\Component;
use Livewire\WithPagination;

class SeniorLawyerRegistry extends Component
{
    public $fname;
    public $mname;
    public $lname;
    public $surname;
    public $license_no;
    public $lawyer_type;
    public $nic;
    public $gender;
    public $marriage_status;
    public $email;
    public $contact1;
    public $contact2;
    public $no;
    public $street1;
    public $street2;
    public $city;

// startLoadSeniorLawyerData
    public function loadSeniorLawyerData($selected_senior_lawyer)
    {
        $this->fname = $selected_senior_lawyer['f_name'];
        $this->mname = $selected_senior_lawyer['m_name'];
        $this->lname = $selected_senior_lawyer['l_name'];
        $this->surname = $selected_senior_lawyer['surname'];
        $this->nic = $selected_senior_lawyer['nic'];
        $this->license_no = $selected_senior_lawyer['license_no'];
        $this->contact1 = $selected_senior_lawyer['contact_1'];
        $this->contact2 = $selected_senior_lawyer['contact_2'];
        $this->lawyer_type = $selected_senior_lawyer['lawyer_title'];
        $this->email = $selected_senior_lawyer['email'];
        $this->no = $selected_senior_lawyer['no'];
        $this->street1 = $selected_senior_lawyer['street_1'];
        $this->street2 = $selected_senior_lawyer['street_2'];
        $this->city = $selected_senior_lawyer['city'];
        $this->gender = $selected_senior_lawyer['gender'];
        $this->marriage_status = $selected_senior_lawyer['marriage_status'];
    }//    endLoadSeniorLawyerData

// startSeniorLawyerUpdate
    public function SeniorLawyerUpdate()
    {
        $selected_S_lawyer = Lawyer::find(auth()->user()->logged_lawyer->id);
        $this->validate([
            'fname' => ['required', 'string', 'max:255'],
            'mname' => ['required', 'string', 'max:255'],
            'lname' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'license_no' => ['required', 'string', 'max:255', 'unique:lawyers'],
            'nic' => ['required', 'string', 'max:255', 'unique:lawyers'],
//            'contact1' => ['required', 'int', 'max:12'],
        ]);
        $lw_cat = LawyerCategory::where('category_name', 'Cat - 01')->first();

        $selected_S_lawyer->lawyer_title = $this->lawyer_type;
        $selected_S_lawyer->f_name = $this->fname;
        $selected_S_lawyer->m_name = $this->mname;
        $selected_S_lawyer->l_name = $this->lname;
        $selected_S_lawyer->surname = $this->surname;
        $selected_S_lawyer->nic = $this->nic;
        $selected_S_lawyer->license_no = $this->license_no;
        $selected_S_lawyer->contact_1 = $this->contact1;
        $selected_S_lawyer->contact_2 = $this->contact2;
        $selected_S_lawyer->email = $this->email;
        $selected_S_lawyer->gender = $this->gender;
        $selected_S_lawyer->no = $this->no;
        $selected_S_lawyer->street_1 = $this->street1;
        $selected_S_lawyer->street_2 = $this->street2;
        $selected_S_lawyer->city = $this->city;
        $selected_S_lawyer->marriage_status = $this->marriage_status;
        $selected_S_lawyer->image = 'null';
        $selected_S_lawyer->category_id = $lw_cat->id;
        $selected_S_lawyer->save();
        $this->ClearAllFields();
    }//    endSeniorLawyerUpdate

// startClearFields
    public function ClearAllFields()
    {
        $this->fname = '';
        $this->mname = '';
        $this->lname = '';
        $this->surname = '';
        $this->license_no = '';
        $this->lawyer_type = '';
        $this->nic = '';
        $this->gender = '';
        $this->marriage_status = '';
        $this->email = '';
        $this->contact1 = '';
        $this->contact2 = '';
        $this->no = '';
        $this->street1 = '';
        $this->street2 = '';
        $this->city = '';
    }//    endClearFields

// startRenderMethod
    public function render()
    {
        $lawyer_categories = LawyerCategory::all();
        $s_lawyer = Lawyer::find(auth()->user()->logged_lawyer->id);
        $this->loadSeniorLawyerData($s_lawyer);
        return view('livewire.lawyer.senior-lawyer-registry', ['s_lawyer' => $s_lawyer, 'lawyer_categories' => $lawyer_categories])->layout('layouts.main');
    }//    endRenderMethod
}
