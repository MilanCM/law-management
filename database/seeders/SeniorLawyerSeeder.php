<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SeniorLawyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lawyers')->insert([
            'f_name' => 'Milan',
            'license_no' => 'LW2022',
            'email' => 'milanmc888@gmail.com',
            'image' => 'uploads/user/contact image.jpg',
            'is_active' => '1',
            'category_id' => '1',
            'user_id' => '1',
            'user_role_id' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
