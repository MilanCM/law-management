@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Case View</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>
    <div class="card-body">
        <div class="col-sm-10" wire:ignore>
            <select
                class="custom-select @if(sizeof($all_cases) === 0 || $errors->first('all_cases'))
                    is-invalid @endif" id="selected_law_case_tag" data-placeholder="Choose anything"
                data-allow-clear="1"
                @if(is_null($all_cases)) disabled @endif>
                @if(sizeof($all_cases) !== 0)
                    <option></option>
                    @foreach($all_cases as $case)
                        <option
                            value="{{ $case['id']}}">{{ $case['case_number']}} -
                            @if($case->client_person !== null){{$case->client_person['f_name']}}
                            . {{$case->client_person['l_name']}}- {{$case->client_person['nic']}}
                            @elseif($case->client_company !==
                            null){{$case->client_company['company_name']}} -
                            <small>{{$case->client_company['br_number']}}</small>
                            @endif
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="col-12 d-flex align-items-stretch flex-column">
        <div class="card bg-light d-flex flex-fill">
            <div class="card-header text-muted border-bottom-0">
                Case Details
            </div>
            <div class="card-body pt-0">
                {{--                case details--}}
                <div class="row">
                    <div class="col-sm-6">
                        <h2 class="lead"><b>CRIM-20220803</b></h2>
                        <ul class="ml-4 mb-0 fa-ul text-muted">
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-calendar-day"></i></span>
                                <b> Case Open Date: </b>2022/08/01
                            </li>
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-location-arrow"></i></span><b>Court Location: </b>Colombo
                                Court
                            </li>
                            <div class="form-group"></div>
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-user"></i></span><b>Assigned Lawyer: </b>Saman
                                Kumara
                            </li>
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-address-card"></i></span><b> Licence No. : </b>
                                LW-20220803
                            </li>
                            <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                Phone #: + 94 747845856
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 mt-sm-4">
                        <p class="text-muted text-sm mt-2"><b>Client: </b> Saman Kumara</p>
                        <ul class="ml-4 mb-0 fa-ul text-muted">
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-address-card"></i></span> NIC/ BR: 971478524V
                            </li>
                            <li class="small"><span class="fa-li"><i
                                        class="fas fa-sm fa-building"></i></span> Address: 222/2, Hill St,
                                Colombo
                            </li>
                            <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                Phone #: + 94 747845856
                            </li>
                        </ul>
                    </div>
                </div>
                {{--                end case details--}}
                <div class="form-group"><br>
                    {{--                    case timeline--}}
                    <div class="col-md-12">
                        <div class="timeline timeline-inverse mt-2">
                            {{--                           ///////////////////// --}}
                            @foreach($upgraded_cases as $upgraded_case)
                                <div class="time-label">
                                    <span class="form-control-sm text-info bg-secondary">
                                    {{ \Carbon\Carbon::parse($upgraded_case->date)->format( "d M. yy")}}
                                    </span>
                                </div>
                                <div>
                                    <i class="fas fa-newspaper bg-primary"></i>
                                    <div class="timeline-item">
                                        <h3 class="timeline-header"><a href="#">Description </a><small
                                                class="text-muted">
                                                {{$upgraded_case->location}}
                                                - {{ \Carbon\Carbon::parse($upgraded_case->date)->format( "d M. yy")}}</small>
                                        </h3>
                                        <div class="timeline-body">
                                            {{$upgraded_case->description}}
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <i class="fas fa-comments bg-info"></i>
                                    <div class="timeline-item">
                                        <h3 class="timeline-header"><a href="#">comment</a></h3>
                                        <div class="timeline-body">
                                            {{$upgraded_case->comment}}
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <i class="fas fa-camera bg-gradient-cyan"></i>
                                    <div class="timeline-item">
                                        <h3 class="timeline-header"><a href="#">Proof Document</a></h3>
                                        <div class="timeline-body">
                                            @foreach($img_model as $image)
                                                {{--                                                <div class="card mr-2 d-flex flex-column justify-content-between">--}}
                                                @if($image['law_case_upgrade_id'] == $upgraded_case->id)
                                                    @if($image['file_type'] === 'jpg' || $image['file_type'] === 'png' ||$image['file_type'] === 'jpeg')
                                                        <img style="width: 128px; height: 128px"
                                                             src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}"
                                                             alt="...">
                                                        {{--                                                        <a class="badge badge-pill badge-danger text-center"--}}
                                                        {{--                                                           wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>--}}
                                                    @elseif($image['file_type'] === 'ogg'||$image['file_type'] === 'mp3'|| $image['file_type'] === 'm4a')
                                                        <audio controls style="width: 150px; height: 150px">
                                                            <source
                                                                src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}">
                                                        </audio>
                                                        {{--                                                        <a class="badge badge-pill badge-danger text-center"--}}
                                                        {{--                                                           wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>--}}
                                                    @elseif($image['file_type']=='pptx' ||$image['file_type']=='pdf' ||$image['file_type']=='docx' ||$image['file_type']=='txt')
                                                        <iframe
                                                            src="https://drive.google.com/file/d/19ViaXuJzhlIyMMZkgeLx1eFD4-xoAaFk/view"
                                                            frameborder="0">
                                                        </iframe>
                                                    @endif
                                                @endif
                                                {{--                                                </div>--}}
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            {{--//////////////--}}
                            <div class="time-label">
                                <span class="form-control-sm text-info bg-light">10 Aug. 2022</span>
                            </div>
                            <div>
                                <i class="fas fa-newspaper bg-primary"></i>
                                <div class="timeline-item">
                                    <h3 class="timeline-header"><a href="#">Description </a><small class="text-muted">
                                            High Court - 01 Aug. 2022</small></h3>
                                    <div class="timeline-body">
                                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                        quora plaxo ideeli hulu weebly balihoo...
                                    </div>
                                </div>
                            </div>
                            <div>
                                <i class="fas fa-comments bg-info"></i>
                                <div class="timeline-item">
                                    <h3 class="timeline-header"><a href="#">comment</a></h3>
                                    <div class="timeline-body">
                                        Take me to your leader!
                                        Switzerland is small and neutral!
                                        We are more like Germany, ambitious and misunderstood!
                                    </div>
                                </div>
                            </div>
                            <div>
                                <i class="fas fa-camera bg-gradient-cyan"></i>
                                <div class="timeline-item">
                                    <h3 class="timeline-header"><a href="#">Prof Document</a></h3>
                                    <div class="timeline-body">
                                        <img src="{{asset('backend/dist/img/user4-128x128.jpg')}}" alt="...">
                                        <img src="{{asset('backend/dist/img/user3-128x128.jpg')}}" alt="...">
                                        <img src="{{asset('backend/dist/img/user3-128x128.jpg')}}" alt="...">
                                        <img src="{{asset('backend/dist/img/user4-128x128.jpg')}}" alt="...">
                                    </div>
                                </div>
                            </div>
                            <div>
                                <i class="far fa-clock bg-gray"></i>
                            </div>
                        </div>
                    </div>
                    {{--                    end case timeline--}}
                </div>
            </div>
        </div>
    </div>
</main>

@push('scripts')
    <script>
        $('#selected_law_case_tag').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#selected_law_case_tag').on('change', function () {
        @this.searched_case_id
            = $(this).val();
        });
    </script>
@endpush
