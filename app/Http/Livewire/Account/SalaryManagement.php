<?php

namespace App\Http\Livewire\Account;

use Livewire\Component;

class SalaryManagement extends Component
{
    public function render()
    {
        return view('livewire.account.salary-management')->layout('layouts.main');
    }
}
