<?php

namespace App\Http\Livewire\Utilities;

use Livewire\Component;

class BookResources extends Component
{
    public function render()
    {
        return view('livewire.utilities.book-resources')->layout('layouts.main');
    }
}
