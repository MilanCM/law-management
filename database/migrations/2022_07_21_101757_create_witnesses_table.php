<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWitnessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('witnesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name');
            $table->string('m_name');
            $table->string('l_name');
            $table->string('sur_name');
            $table->string('nic');
            $table->integer('contact_1');
            $table->integer('contact_2')->nullable();
            $table->string('email')->nullable();
            $table->string('no');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->string('city');
            $table->string('relationship');
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('law_case_id');
            $table->foreign('law_case_id')->references('id')->on('law_cases');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('witnesses');
    }
}
