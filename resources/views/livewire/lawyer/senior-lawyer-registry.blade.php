@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Senior Lawyer</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Manage Senior Lawyer</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Senior Lawyer Registry Form</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">First Name<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2"
                               placeholder="Saman" wire:model="fname">
                        @error('fname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Middle Name / Names</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Sampath" wire:model="mname">
                        @error('mname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Last Name</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Werasuriya" wire:model="lname">
                        @error('lname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label class="mb-sm-0">Family Name</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Werasuriya Arachchilage" wire:model="surname">
                        @error('surname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Licence Number</label>
                        <input type="text"
                               class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="LW-20220801" wire:model="license_no">
                        @error('license_no') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Lawyer Type </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="lawyer_type">
                            @foreach($lawyer_categories as $lawyer_category)
                                <option>{{$lawyer_category['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">NIC Number</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter NIC" wire:model="nic">
                        @error('nic') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Gender </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="gender">
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Marriage Status </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="marriage_status">
                            <option>Single</option>
                            <option>Married</option>
                            <option>Widow</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label class="mb-sm-0">Email</label>
                        <input type="email" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter email" wire:model="email">
                        @error('email') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Contact</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 1"
                                           wire:model="contact1">
                                </label>
                                @error('contact1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 2"
                                           wire:model="contact2">
                                </label>
                                @error('contact2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Address</label>
                        <div class="row">
                            <div class="col-sm-1">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="no."
                                       wire:model="no">
                                @error('no') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street1"
                                       wire:model="street1">
                                @error('street1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street2"
                                       wire:model="street2">
                                @error('street2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="city"
                                       wire:model="city">
                                @error('city') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="SeniorLawyerUpdate()">Update</button>
                </div>
            </div>
        </div>
    </div>
</main>
