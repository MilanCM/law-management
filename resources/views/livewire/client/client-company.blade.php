@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Client Registry (company)</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Client Registry (company)</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="table_search"
                       class="form-control form-control-border border-width-2 bg-transparent"
                       placeholder="Search" wire:model="client_company_search_key">
            </div>
            <div class="col-sm-2">
            </div>

            <div class="col-sm-2">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input"
                           type="checkbox" wire:click="changeCustomerState" wire:model="active"
                           id="customCheckbox1" value="1">
                    <label for="customCheckbox1" class="custom-control-label">Active</label>
                </div>
            </div>
            <div class="col-sm-2">

                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" wire:click="changeCustomerState" type="checkbox"
                           id="customCheckbox2" value="2" wire:model="deActive">
                    <label for="customCheckbox2" class="custom-control-label">Deactive</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    </div>

    <div class="card mt-2 card-info mt-3">
        <div class="card-header">
            <h3 class="card-title">Client Company List</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="max-height: 200px;">
            <table class="table-striped table-sm table">
                <thead style="background-color: rgba(185,202,209,0.62)">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>BR</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($client_companies as $client_company)


                    <tr wire:click="LoadSelectedClientDetails({{$client_company}})">
                        <td>{{$client_company->id}}</td>
                        <td>{{$client_company->company_name}}</td>
                        <td>{{$client_company->br_number}}</td>
                        <td>{{$client_company->contact_1}}</td>
                        <td>{{$client_company->no}}, {{$client_company->street_1}}, {{$client_company->street_2}}
                            , {{$client_company->city}}</td>
                        @if($client_company->is_active == 1)
                            <td><span class="badge badge-success">Active</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-xs "
                                        wire:click="DeactivateSelectedClientCompanyDetails({{$client_company}})"><i
                                        class="fa fa-trash"
                                        aria-hidden="true"></i>
                                </button>
                            </td>

                        @else
                            <td><span class="badge badge-danger">Deactive</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-success btn-xs "
                                        wire:click="ActivateSelectedClientCompanyDetails({{$client_company}})"><i
                                        class="fa fa-trash-restore"
                                        aria-hidden="true"></i>
                                </button>
                            </td>
                        @endif


                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="mt-2">
        {{--        {{$customers->links()}}--}}
    </div>
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Registry Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            @if (\Session::has('message'))
                <div class="alert alert-success alert-block" role="alert">
                    <ul>
                        <li>{!! \Session::get('message') !!}</li>
                    </ul>
                </div>
            @endif
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="mb-sm-0">Company Name</label>
                            <input type="text"
                                   class="form-control form-control-sm form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="ABC Holdings Pvt Ltd." wire:model="company_name">
                            @error('company_name') <p class="text-danger">{{$message}}</p> @enderror
                        </div>

                        <div class="form-group">
                            <label class="mb-sm-0">Mother Company Name</label>
                            <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="ABC Holdings Pvt Ltd." wire:model="mother_company_name">
                            @error('mother_company_name') <p class="text-danger">{{$message}}</p> @enderror
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label class="mb-sm-0">Company Type</label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           id="exampleInputEmail1"
                                           placeholder="Company Type" wire:model="company_type">
                                    @error('company_type') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="mb-sm-0">Owner Full Name</label>
                            <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="Werasuriya" wire:model="owner_full_name">
                            @error('owner_full_name') <p class="text-danger">{{$message}}</p> @enderror
                        </div>

                        <div class="form-group">
                            <label class="mb-sm-0">Director Full Name</label>
                            <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="Werasuriya" wire:model="director_name">
                            @error('director_name') <p class="text-danger">{{$message}}</p> @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="mb-sm-0">Business Registration Number</label>
                                        <input type="text"
                                               class="form-control form-control-sm  form-control-border border-width-2"
                                               id="exampleInputPassword1"
                                               placeholder="Enter BR" wire:model="br_number">
                                        @error('br_number') <p class="text-danger">{{$message}}</p> @enderror
                                    </div>
                                    <div class="form-group">
                                        <label class="mb-sm-0">Occupation</label>
                                        <input type="text"
                                               class="form-control form-control-sm  form-control-border border-width-2"
                                               id="exampleInputPassword1"
                                               placeholder="Enter Occupation" wire:model="occupation">
                                        @error('occupation') <p class="text-danger">{{$message}}</p> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="mb-sm-0">Email</label>
                            <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="Enter email" wire:model="email">
                            @error('email') <p class="text-danger">{{$message}}</p> @enderror
                        </div>
                        <div class="form-group">
                            <label class="mb-sm-0">Contact</label>
                            <div class="row">
                                <div class="col-sm-2">
                                    <label>
                                        <input type="text"
                                               class="form-control form-control-sm  form-control-border border-width-2"
                                               placeholder="contact 1"
                                               wire:model="contact_1">
                                    </label>
                                    @error('contact_1') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                                <div class="col-sm-2">
                                    <label>
                                        <input type="text"
                                               class="form-control form-control-sm  form-control-border border-width-2"
                                               placeholder="contact 2"
                                               wire:model="contact_2">
                                    </label>
                                    @error('contact_2') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="mb-sm-0">Address</label>
                            <div class="row">
                                <div class="col-sm-1">
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="no."
                                           wire:model="no">
                                    @error('no') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                                <div class="col-sm-3">
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="street1"
                                           wire:model="street_1">
                                    @error('street_1') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                                <div class="col-sm-3">
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="street2"
                                           wire:model="street_2">
                                    @error('street_2') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                                <div class="col-sm-3">
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="city"
                                           wire:model="city">
                                    @error('city') <p class="text-danger">{{$message}}</p> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="mb-sm-0">Web URL</label>
                            <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                                   id="exampleInputEmail1"
                                   placeholder="Enter web url" wire:model="web_url">
                            @error('wen_url') <p class="text-danger">{{$message}}</p> @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="mb-sm-0">Lawyer Office / Branch</label>
                    <div class="col-sm-10" wire:ignore>
                        <select
                            class="custom-select @if($offices->count() === 0 || $errors->first('offices')) is-invalid @endif"
                            id="office" data-placeholder="Choose anything" data-allow-clear="1"
                            @if(is_null($offices)) disabled @endif>
                            @if($offices->count() !== 0)
                                <option></option>
                                @foreach($offices as $office)
                                    <option value="{{ $office->id }}">{{ $office->office_name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    @error('office') <p class="text-danger">{{$message}}</p> @enderror
                </div>

                <hr>
                <div class="row">

                    <div class="card-footer">
                        <button class="btn btn-success" wire:click.prevent="ClientCompanyDetailsInsert()">Insert
                        </button>
                        <button class="btn btn-warning" wire:click.prevent="ClientCompanyDetailsUpdate()">Update
                        </button>
                    </div>

                </div>


            </div>

        </div>
    </div>
</main>

@push('styles')
    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('select2/select2-bootstrap4.css') }}">

@endpush

@push('scripts')
    <script src="{{ asset('select2/select2.min.js') }}"></script>
    <script>
        $('#office').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an Office'
        });

        $('#office').on('change', function () {
        @this.office
            = $(this).val();
        });

    </script>
@endpush
