@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Junior Lawyer Account</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Junior Lawyer Account</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection


<main>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group mt-3">
                    <div class="row">
                        <div class="col-sm-10" wire:ignore>
                            <select
                                class="custom-select @if(count($users) === 0 || $errors->first('keywords')) is-invalid @endif"
                                id="keywords" data-placeholder="Choose anything" data-allow-clear="1"
                                @if(is_null($users)) disabled @endif>
                                @if(count($users) > 0)
                                    <option></option>
                                    @foreach($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->f_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group-append">
                                <button wire:click.prevent="asd()" type="submit" class="btn btn-default">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    @if(count($users) === 0)
                        <label>You don't have any Employers.
                        </label>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @if($select_user_id !== null)
        <div class="col-7 d-flex align-items-stretch flex-column">
            <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                    User Details
                </div>
                <div class="card-body pt-0">
                    <h2 class="lead">
                        <b>{{$selected_user->f_name . ' ' . $selected_user->m_name . ' ' . $selected_user->l_name}}</b><span
                            class="ml-2 small text-muted">- [{{$selected_user->user_role->user_type}}]</span>
                    </h2>
                    <div class="row">
                        <div class="col-sm-8">
                            <ul class="mb-0 fa-ul text-muted mt-2">
                                <li class="small"><span class="fa-li"><i
                                            class="fas fa-sm fa-address-card"></i></span><b> NIC
                                        :</b> {{$selected_user->nic}}
                                </li>
                                <li class="small"><span class="fa-li"><i
                                            class="fas fa-sm fa-address-card"></i></span><b> Licence No
                                        :</b> {{$selected_user->license_no}}
                                </li>
                                <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                    <b>Phone # :</b>
                                    +94 {{$selected_user->contact_1.' / +94 '.$selected_user->contact_2}}
                                </li>
                                <li class="small"><span class="fa-li"><i
                                            class="fas fa-sm fa-building"></i></span> <b>Address :</b> {{$selected_user->no . ',' . $selected_user->street_1 . ',' . $selected_user->street_2 . '' .
            $selected_user->city}}
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-center">
                                {{--                                <img class="profile-user-img img-fluid img-circle" src="{{asset('avatar/avatar3.png')}}"--}}
                                {{--                                     alt="img-fluid">--}}
                                <img class="img-circle" style="background-repeat:no-repeat;
      background-size:cover;
      width:100px;
      height:100px;" src="{{asset('storage/'.$selected_user->image)}}"
                                     alt="User profile picture">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="text-right">
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="fas fa-eye"></i> View User Details
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">User Account Create Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body  col-7 d-flex align-items-stretch flex-column">
            <div class="form-group">
                <label class="mb-sm-0">Email/ User Name<code> *</code></label>
                <input type="text"
                       class="form-control form-control-sm form-control-border border-width-2"
                       wire:model="email">
                @error('email') <p class="text-danger">{{$message}}</p> @enderror
            </div>
            <div class="form-group">
                <label class="mb-sm-0">Password<code> *</code></label>
                <input type="password"
                       class="form-control form-control-sm form-control-border border-width-2"
                       wire:model="password">
                @error('password') <p class="text-danger">{{$message}}</p> @enderror
            </div>
            <div class="form-group">
                <label class="mb-sm-0">Confirm Password<code> *</code></label>
                <input type="password"
                       class="form-control form-control-sm form-control-border border-width-2"
                       wire:model="confirm_password">
                @error('confirm_password') <p class="text-danger">{{$message}}</p> @enderror
            </div>
            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-primary" wire:click.prevent="createUserAccountJuniorLawyer()">Save
                    </button>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>

@push('styles')
    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('select2/select2-bootstrap4.css') }}">

@endpush

@push('scripts')
    <script src="{{ asset('select2/select2.min.js') }}"></script>
    <script>
        $('#keywords').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#keywords').on('change', function () {
        @this.select_user_id
            = $(this).val();
        });

        $('#keywords').val(null).trigger('change');
    </script>
@endpush


