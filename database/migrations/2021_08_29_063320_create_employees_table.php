<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name');
            $table->string('m_name');
            $table->string('l_name');
            $table->string('nic')->unique();
            $table->string('etf_number')->unique()->nullable();
            $table->string('email');
            $table->string('gender');
            $table->string('occupation');
            $table->integer('contact_1');
            $table->integer('contact_2')->nullable();
            $table->string('no');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->string('city');
            $table->string('marriage_status');
            $table->string('image');
            $table->boolean('is_active')->default(2);

            $table->unsignedBigInteger('office_id');
            $table->unsignedInteger('user_role_id');
            $table->unsignedBigInteger('user_id')->nullable();

            $table->foreign('user_role_id')->references('id')->on('user_roles');
            $table->foreign('office_id')->references('id')->on('offices');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
