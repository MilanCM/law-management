<?php

namespace App\Http\Livewire\Lawcase;

use App\Models\LawCaseMedia;
use App\Models\TemporaryFile;
use App\Models\Witness;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\LawCase;
use App\Models\Customer;
use App\Models\LawCategory;
use App\Models\JuniorLawyer;
use Livewire\WithFileUploads;
use App\Models\ClientCompany;
use function Illuminate\Support\Facades\File;


class LawcaseRegistry extends Component
{
    use WithFileUploads;
    use \Livewire\WithPagination;

    public $parent_category;
    public $categories = [];
    public $arr_model = [];
    public $url;

    public $case_number;
    public $case_location;
    public $case_open_date;
    public $case_law_field;
    public $case_assigned_client_id;
    public $case_assigned_lawyers = [];
    public $case_assigned_lawyer_id;
    public $case_registered_attorney_id;

    public $selected_case_id;
    public $selected_client = [];
    public $selected_assigned_lawyer;
    public $selected_assigned_lawyer_initial_name;
    public $selected_registered_attorney_initial_name;
    public $selected_registered_attorney;
    public $arr_client = [];
    public $arr_witness = [];

    public $case_description;
    public $case_comment;
    public $case_witness_f_name;
    public $case_witness_m_name;
    public $case_witness_l_name;
    public $case_witness_family_name;
    public $case_witness_nic;
    public $case_witness_relationship;
    public $case_witness_email;
    public $case_witness_contact_1;
    public $case_witness_contact_2;
    public $case_witness_no;
    public $case_witness_street_1;
    public $case_witness_street_2;
    public $case_witness_city;
    public $images;

    public $case_search_key;
    protected $paginationTheme = 'bootstrap';

// startInsertCaseDetails
    public
    function InsertCaseDetails()
    {
        $this->validate([
            'case_number' => 'required|string|max:20|unique:law_cases',
            'case_location' => 'required|string|max:20',
            'case_open_date' => 'required|date',
            'case_law_field' => 'required|string',
            'case_assigned_client_id' => 'required',
            'case_assigned_lawyer_id' => 'required',
            'case_registered_attorney_id' => 'required',
            'case_description' => 'required|string',
            'case_comment' => 'required|string',
        ]);
        if ($this->case_law_field != null) {
            $law_field_id = LawCategory::where('law_field', $this->case_law_field)->first();

            $law_case = new LawCase();
            $law_case->case_number = $this->case_number;
            $law_case->court_location = $this->case_location;
            $law_case->case_opened_date = $this->case_open_date;
            $law_case->description = $this->case_description;
            $law_case->comment = $this->case_comment;
            $law_case->is_active = 1;
            $law_case->category_id = $law_field_id['id'];
            if ($this->case_assigned_client_id !== null) {
                if (explode('/', $this->case_assigned_client_id)[1] == 'person') {
                    $law_case->client_id = explode('/', $this->case_assigned_client_id)[0];
                } elseif (explode('/', $this->case_assigned_client_id)[1] == 'company') {
                    $law_case->client_company_id = explode('/', $this->case_assigned_client_id)[0];
                }
            }
            $law_case->assigned_lawyer_id = $this->case_assigned_lawyer_id;
            $law_case->registered_attorney_id = $this->case_registered_attorney_id;
            $law_case->save();

// Media save
            $uploaded_images = TemporaryFile::where('folder', 'case_media_folder')->get();
            if ($uploaded_images) {
                foreach ($uploaded_images as $media) {
                    $moved_path = 'uploads/law_case/' . $law_case['id'] . '/' . $media->filename;
                    Storage::move('avatars/tmp/case_media_folder/' . $media->filename, $moved_path);
                    $law_case_media = new LawCaseMedia();
                    $law_case_media->file_type = $media->extension;
                    $law_case_media->file_path = $media->filename;
                    $law_case_media->is_active = 1;
                    $law_case_media->law_case_id = $law_case['id'];
                    $law_case_media->law_case_upgrade_id = null;
                    $law_case_media->save();
                    $media->delete();
                }
                Storage::deleteDirectory('avatars');
            }

// witnesses save
            if (!empty($this->arr_witness)) {
                foreach ($this->arr_witness as $witness) {
                    $law_case_witness = new Witness();
                    $law_case_witness->f_name = $witness['f_name'];
                    $law_case_witness->m_name = $witness['m_name'];
                    $law_case_witness->l_name = $witness['l_name'];
                    $law_case_witness->sur_name = $witness['family_name'];
                    $law_case_witness->nic = $witness['nic'];
                    $law_case_witness->contact_1 = $witness['contact_1'];
                    $law_case_witness->contact_2 = $witness['contact_2'];
                    $law_case_witness->email = $witness['email'];
                    $law_case_witness->no = $witness['no'];
                    $law_case_witness->street_1 = $witness['street_1'];
                    $law_case_witness->street_2 = $witness['street_2'];
                    $law_case_witness->city = $witness['city'];
                    $law_case_witness->relationship = $witness['relationship'];
                    $law_case_witness->is_active = 1;
                    $law_case_witness->law_case_id = $law_case['id'];
                    $law_case_witness->save();
                }
            }
            unset($this->arr_witness[0]);
            return redirect(request()->header('Referer'));
            $this->ClearLawCaseFields();
            $this->mount();
        }//    endLawFieldIf
    }//    endInsertCaseDetails

// startAddWitness
    public
    function AddWitness()
    {
        $this->validate([
            'case_witness_f_name' => 'required|string',
            'case_witness_l_name' => 'required|string',
            'case_witness_m_name' => 'required|string',
            'case_witness_family_name' => 'required|string',
            'case_witness_nic' => 'required|string',
            'case_witness_relationship' => 'required|string',
            'case_witness_email' => 'required|email',
            'case_witness_contact_1' => 'required',
//            'case_witness_contact_2' => 'digits:10',
            'case_witness_no' => 'required|string',
            'case_witness_street_1' => 'required|string',
//            'case_witness_street_2' => 'string',
            'case_witness_city' => 'required|string',
        ]);


        $arr = ['f_name' => $this->case_witness_f_name,
            'l_name' => $this->case_witness_l_name,
            'm_name' => $this->case_witness_m_name,
            'family_name' => $this->case_witness_family_name,
            'nic' => $this->case_witness_nic,
            'relationship' => $this->case_witness_relationship,
            'email' => $this->case_witness_email,
            'contact_1' => $this->case_witness_contact_1,
            'contact_2' => $this->case_witness_contact_2,
            'no' => $this->case_witness_no,
            'street_1' => $this->case_witness_street_1,
            'street_2' => $this->case_witness_street_2,
            'city' => $this->case_witness_city];

        if (empty($this->arr_witness)) {
            array_push($this->arr_witness, $arr);
        } else {
            $value = array_search($this->case_witness_nic, array_column($this->arr_witness, 'nic'));
            if ($value !== false) {
                $this->arr_witness[$value]['f_name'] = $this->case_witness_f_name;
                $this->arr_witness[$value]['l_name'] = $this->case_witness_l_name;
                $this->arr_witness[$value]['m_name'] = $this->case_witness_m_name;
                $this->arr_witness[$value]['family_name'] = $this->case_witness_family_name;
                $this->arr_witness[$value]['nic'] = $this->case_witness_nic;
                $this->arr_witness[$value]['relationship'] = $this->case_witness_relationship;
                $this->arr_witness[$value]['email'] = $this->case_witness_email;
                $this->arr_witness[$value]['contact_1'] = $this->case_witness_contact_1;
                $this->arr_witness[$value]['contact_2'] = $this->case_witness_contact_2;
                $this->arr_witness[$value]['no'] = $this->case_witness_no;
                $this->arr_witness[$value]['street_1'] = $this->case_witness_street_1;
                $this->arr_witness[$value]['street_2'] = $this->case_witness_street_2;
                $this->arr_witness[$value]['city'] = $this->case_witness_city;
            } else {
                array_push($this->arr_witness, $arr);
            }
        }
        $this->ClearWitnessFields();
    }//    endAddWitness

// startWitnessRemoveArray
    public
    function witnessRemoveArray($id)
    {
        unset($this->arr_witness[$id]);
    }//    endWitnessRemoveArray

// startWitnessEditArray
    public
    function witnessEditArray($id)
    {
        $this->case_witness_f_name = $this->arr_witness[$id]['f_name'];
        $this->case_witness_m_name = $this->arr_witness[$id]['m_name'];
        $this->case_witness_l_name = $this->arr_witness[$id]['l_name'];
        $this->case_witness_family_name = $this->arr_witness[$id]['family_name'];
        $this->case_witness_nic = $this->arr_witness[$id]['nic'];
        $this->case_witness_relationship = $this->arr_witness[$id]['relationship'];
        $this->case_witness_email = $this->arr_witness[$id]['email'];
        $this->case_witness_contact_1 = $this->arr_witness[$id]['contact_1'];
        $this->case_witness_contact_2 = $this->arr_witness[$id]['contact_2'];
        $this->case_witness_no = $this->arr_witness[$id]['no'];
        $this->case_witness_street_1 = $this->arr_witness[$id]['street_1'];
        $this->case_witness_street_2 = $this->arr_witness[$id]['street_2'];
        $this->case_witness_city = $this->arr_witness[$id]['city'];
    }//    endWitnessEditArray

// startLoadSelectedCaseDetails
    public
    function LoadSelectedCaseDetails($case)
    {
        $this->selected_case_id = $case['id'];
        $law_field = LawCategory::where('id', $case['category_id'])->first()->law_field;
        $this->selected_client = [];
        $this->arr_witness = [];
        if ($law_field) {
            $this->case_law_field = $law_field;
            $this->case_comment = $case['comment'];
            $this->case_number = $case['case_number'];
            $this->case_location = $case['court_location'];
            $this->case_description = $case['description'];
            $this->case_open_date = $case['case_opened_date'];
            $this->case_assigned_lawyer_id = $case['assigned_lawyer_id'];
            $this->case_registered_attorney_id = $case['registered_attorney_id'];
            if ($case['client_id'] !== null) {
                $this->case_assigned_client_id = $case['client_id'] . '/' . 'person';
            } elseif ($case['client_company_id'] !== null) {
                $this->case_assigned_client_id = $case['client_company_id'] . '/' . 'company';
            }
            $witnesses = Witness::where('law_case_id', $case['id'])->get();
            foreach ($witnesses as $witness) {
                $arr = [
                    'f_name' => $witness['f_name'],
                    'l_name' => $witness['l_name'],
                    'm_name' => $witness['m_name'],
                    'family_name' => $witness['sur_name'],
                    'nic' => $witness['nic'],
                    'relationship' => $witness['relationship'],
                    'email' => $witness['email'],
                    'contact_1' => $witness['contact_1'],
                    'contact_2' => $witness['contact_2'],
                    'no' => $witness['no'],
                    'street_1' => $witness['street_1'],
                    'street_2' => $witness['street_2'],
                    'city' => $witness['city']];

                array_push($this->arr_witness, $arr);
            }
        }
        $this->images = LawCaseMedia::where('law_case_id', $case['id'])
            ->where('law_case_upgrade_id', null)->get();

    }//    endLoadSelectedCaseDetails

// startUpdateCaseDetails
    public
    function UpdateCaseDetails()
    {
//        dd($this->arr_witness);
        if ($this->selected_case_id !== null) {
            $selected_case = LawCase::find($this->selected_case_id);
//            dd($selected_case);
            $this->validate([
//                'case_number' => 'required|string|max:20|unique:law_cases',
                'case_location' => 'required|string|max:20',
                'case_open_date' => 'required|date',
                'case_law_field' => 'required|string',
                'case_assigned_client_id' => 'required',
                'case_assigned_lawyer_id' => 'required',
                'case_registered_attorney_id' => 'required',
                'case_description' => 'required|string',
                'case_comment' => 'required|string',
            ]);

            $law_field_id = LawCategory::where('law_field', $this->case_law_field)->first();
// updateLawCaseTable
            $selected_case->case_number = $this->case_number;
            $selected_case->court_location = $this->case_location;
            $selected_case->case_opened_date = $this->case_open_date;
            $selected_case->description = $this->case_description;
            $selected_case->comment = $this->case_comment;
            $selected_case->is_active = 1;
            $selected_case->category_id = $law_field_id['id'];
            if ($this->case_assigned_client_id !== null) {
                if (explode('/', $this->case_assigned_client_id)[1] == 'person') {
                    $selected_case->client_id = explode('/', $this->case_assigned_client_id)[0];
                } elseif (explode('/', $this->case_assigned_client_id)[1] == 'company') {
                    $selected_case->client_company_id = explode('/', $this->case_assigned_client_id)[0];
                }
            }
            $selected_case->assigned_lawyer_id = $this->case_assigned_lawyer_id;
            $selected_case->registered_attorney_id = $this->case_registered_attorney_id;
            $selected_case->save();

// updateLawCaseMediaTable
            $uploaded_images = TemporaryFile::where('folder', 'case_media_folder')->get();
            if ($uploaded_images) {
                foreach ($uploaded_images as $media) {
                    $moved_path = 'uploads/law_case/' . $selected_case['id'] . '/' . $media->filename;
                    Storage::move('avatars/tmp/case_media_folder/' . $media->filename, $moved_path);
                    $law_case_media = new LawCaseMedia();
                    $law_case_media->file_type = $media->extension;
                    $law_case_media->file_path = $media->filename;
                    $law_case_media->is_active = 1;
                    $law_case_media->law_case_id = $selected_case['id'];
                    $law_case_media->law_case_upgrade_id = null;
                    $law_case_media->save();
                    $media->delete();
                }
                Storage::deleteDirectory('avatars');
            }

// updateWitnessesTable
            if (!empty($this->arr_witness)) {
                foreach ($this->arr_witness as $witness) {
                    $case_witness = Witness::where('law_case_id', $selected_case['id'])
                        ->where('nic', $witness['nic'])->first();
                    if ($case_witness) {
                        $case_witness->f_name = $witness['f_name'];
                        $case_witness->m_name = $witness['m_name'];
                        $case_witness->l_name = $witness['l_name'];
                        $case_witness->sur_name = $witness['family_name'];
                        $case_witness->nic = $witness['nic'];
                        $case_witness->contact_1 = $witness['contact_1'];
                        $case_witness->contact_2 = $witness['contact_2'];
                        $case_witness->email = $witness['email'];
                        $case_witness->no = $witness['no'];
                        $case_witness->street_1 = $witness['street_1'];
                        $case_witness->street_2 = $witness['street_2'];
                        $case_witness->city = $witness['city'];
                        $case_witness->relationship = $witness['relationship'];
                        $case_witness->save();
                    } else {
                        $law_case_witness = new Witness();
                        $law_case_witness->f_name = $witness['f_name'];
                        $law_case_witness->m_name = $witness['m_name'];
                        $law_case_witness->l_name = $witness['l_name'];
                        $law_case_witness->sur_name = $witness['family_name'];
                        $law_case_witness->nic = $witness['nic'];
                        $law_case_witness->contact_1 = $witness['contact_1'];
                        $law_case_witness->contact_2 = $witness['contact_2'];
                        $law_case_witness->email = $witness['email'];
                        $law_case_witness->no = $witness['no'];
                        $law_case_witness->street_1 = $witness['street_1'];
                        $law_case_witness->street_2 = $witness['street_2'];
                        $law_case_witness->city = $witness['city'];
                        $law_case_witness->relationship = $witness['relationship'];
                        $law_case_witness->is_active = 1;
                        $law_case_witness->law_case_id = $selected_case['id'];
                        $law_case_witness->save();
                    }
                }
            }
            $this->arr_witness = [];
            $this->ClearLawCaseFields();
            $this->mount();
        }
    }//    endUpdateCaseDetails

// startRemoveMediaFile
    public
    function removeMediaFile($image)
    {
        LawCaseMedia::find($image['id'])->delete();
        Storage::delete('uploads/law_case/' . $image['law_case_id'] . '/' . $image['file_path']);
        $case = LawCase::find($image['law_case_id']);
        $this->LoadSelectedCaseDetails($case);
    }//     startRemoveMediaFile

// startRollBack
    public
    function rollBack()
    {
        if (is_null($this->parent_category['parent_id'])) {
            $this->mount();
        } else {
            $this->categories = LawCategory::with('children')->withCount('children')
                ->where('parent_id', $this->parent_category['parent_id'])->get();
            $this->parent_category = LawCategory::with('children')->withCount('children')
                ->where('id', $this->parent_category['parent_id'])->first();
        }
    }//    endRollBack

// startMountMethod
    public
    function mount()
    {
        if (Storage::exists('avatars')) {
            Storage::deleteDirectory('avatars');
            TemporaryFile::where('folder', 'case_media_folder')->delete();
        }

        $this->categories = LawCategory::with('children')->withCount('children')
            ->whereNull('parent_id')->orderBy('law_field', 'ASC')
            ->get();

        $clients = Customer::where('is_active', '1')->get();
        foreach ($clients as $client) {
            $arr = [
                'id' => $client['id'],
                'f_name' => $client['f_name'],
                'nic' => $client['nic'],
                'client_type' => 'person',
            ];
            array_push($this->arr_model, $arr);
        }
        $companies = ClientCompany::where('is_active', '1')->get();
        foreach ($companies as $company) {
            $arr = [
                'id' => $company['id'],
                'f_name' => $company['company_name'],
                'nic' => $company['br_number'],
                'client_type' => 'company',
            ];
            array_push($this->arr_model, $arr);
        }

        $this->case_assigned_lawyers = JuniorLawyer::where('is_active', '1')->get();
        $this->parent_category = [];
    }//    endMountMethod

// startFillMainCategory
    public
    function fillMainCategory($catch_main_category)
    {
        $this->parent_category = $catch_main_category;
        if (isset($catch_main_category['children_count']) && $catch_main_category['children_count'] > 0) {
            $this->categories = LawCategory::with('children')->withCount('children')
                ->where('parent_id', $catch_main_category['id'])->get();
        }
        $this->case_law_field = $this->parent_category['law_field'];
    }//    endFillMainCategory

// startClearWitnessFields
    public
    function ClearWitnessFields()
    {
        $this->case_witness_f_name = null;
        $this->case_witness_m_name = null;
        $this->case_witness_l_name = null;
        $this->case_witness_family_name = null;
        $this->case_witness_nic = null;
        $this->case_witness_relationship = null;
        $this->case_witness_email = null;
        $this->case_witness_contact_1 = null;
        $this->case_witness_contact_2 = null;
        $this->case_witness_no = null;
        $this->case_witness_street_1 = null;
        $this->case_witness_street_2 = null;
        $this->case_witness_city = null;
    }//    endClearWitnessFields

// startClearWitnessFields
    public
    function ClearLawCaseFields()
    {
        $this->case_number = null;
        $this->case_location = null;
        $this->case_open_date = null;
        $this->case_law_field = null;
        $this->case_assigned_client_id = null;
        $this->case_assigned_lawyers = null;
        $this->case_assigned_lawyer_id = null;
        $this->case_registered_attorney_id = null;
        $this->case_comment = null;
        $this->case_description = null;
        $this->arr_witness = [];
    }//    endClearWitnessFields

// startRenderMethod
    public
    function render()
    {
        if ($this->case_assigned_lawyer_id !== null) {
            $this->selected_assigned_lawyer = JuniorLawyer::find($this->case_assigned_lawyer_id);
            if ($this->selected_assigned_lawyer !== null) {
                if (!$this->selected_assigned_lawyer['f_name'] == '') {
                    $initial_family_name = [];
                    $middle_names = [];
                    if (!$this->selected_assigned_lawyer['surname'] == '') {
                        foreach (explode(' ', $this->selected_assigned_lawyer['surname']) as $family_name) {
                            array_push($initial_family_name, mb_substr($family_name, 0, 1) . ' ');
                        }
                    }
                    if (!$this->selected_assigned_lawyer['m_name'] == '') {
                        foreach (explode(' ', $this->selected_assigned_lawyer['m_name']) as $middle_name) {
                            array_push($middle_names, mb_substr($middle_name, 0, 1) . ' ');
                        }
                    }
                    $this->selected_assigned_lawyer_initial_name = strtoupper(implode('',
                            $initial_family_name) . mb_substr($this->selected_assigned_lawyer['f_name'], 0, 1)
                        . ' ' . implode('', $middle_names) . ' ' . $this->selected_assigned_lawyer['l_name']);
                }
            }
        }
        if ($this->case_registered_attorney_id !== null) {
            $this->selected_registered_attorney = JuniorLawyer::find($this->case_registered_attorney_id);
            if ($this->selected_registered_attorney !== null) {
                if (!$this->selected_registered_attorney['f_name'] == '') {
                    $initial_family_name = [];
                    $middle_names = [];
                    if (!$this->selected_registered_attorney['surname'] == '') {
                        foreach (explode(' ', $this->selected_registered_attorney['surname']) as $family_name) {
                            array_push($initial_family_name, mb_substr($family_name, 0, 1) . ' ');
                        }
                    }
                    if (!$this->selected_registered_attorney['m_name'] == '') {
                        foreach (explode(' ', $this->selected_registered_attorney['m_name']) as $middle_name) {
                            array_push($middle_names, mb_substr($middle_name, 0, 1) . ' ');
                        }
                    }
                    $this->selected_registered_attorney_initial_name = strtoupper(implode('',
                            $initial_family_name) . mb_substr($this->selected_registered_attorney['f_name'], 0, 1)
                        . ' ' . implode('', $middle_names) . ' ' . $this->selected_registered_attorney['l_name']);
                }
            }
        }
        if ($this->case_assigned_client_id !== null) {
            if (explode('/', $this->case_assigned_client_id)[1] == 'person') {
                $selected_client = Customer::find(explode('/', $this->case_assigned_client_id)[0]);
                $this->arr_client = [
                    'f_name' => $selected_client['initial_name'],
                    'contact_1' => $selected_client['contact_1'],
                    'nic' => $selected_client['nic'],
                ];
                array_push($this->selected_client, $this->arr_client);
            } elseif (explode('/', $this->case_assigned_client_id)[1] == 'company') {
                $selected_client = ClientCompany::find(explode('/', $this->case_assigned_client_id)[0]);
                $this->arr_client = [
                    'f_name' => $selected_client['company_name'],
                    'contact_1' => $selected_client['contact_1'],
                    'nic' => $selected_client['br_number'],
                ];
                array_push($this->selected_client, $this->arr_client);
            }
        }
        $cases = LawCase::where('case_number', 'like', '%' . $this->case_search_key . '%')
            ->orwhere('court_location', 'like', '%' . $this->case_search_key . '%')->paginate(4);
//        dd($cse);
        return view('livewire.lawcase.lawcase-registry', ['arr_model' => $this->arr_model,
            'case_assigned_lawyers' => $this->case_assigned_lawyers, 'selected_client' => $this->selected_client,
            'selected_assigned_lawyer' => $this->selected_assigned_lawyer,
            'selected_registered_attorney' => $this->selected_registered_attorney,
            'cases' => $cases])->layout('layouts.main');
    }//    endRenderMethod
}
