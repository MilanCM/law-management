<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawCaseMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('law_case_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('file_type');
            $table->string('file_path');
            $table->boolean('is_active')->default(1);
            $table->unsignedBigInteger('law_case_id');
            $table->unsignedBigInteger('law_case_upgrade_id')->nullable();

            $table->foreign('law_case_id')->references('id')->on('law_cases');
            $table->foreign('law_case_upgrade_id')->references('id')->on('case_update_details');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('law_case_media');
    }
}
