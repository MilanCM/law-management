<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->string('mother_Company')->nullable();
            $table->string('company_type');
            $table->string('owner_full_name');
            $table->string('director_name')->nullable();
            $table->string('occupation');
            $table->string('no');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->string('city');
            $table->integer('contact_1');
            $table->integer('contact_2')->nullable();
            $table->string('email');
            $table->string('br_number')->unique();
            $table->string('web_url')->nullable();
            $table->boolean('is_active')->default(2);

            $table->unsignedBigInteger('office_id');

            $table->foreign('office_id')->references('id')->on('offices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_companies');
    }
}
