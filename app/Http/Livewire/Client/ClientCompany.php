<?php

namespace App\Http\Livewire\Client;

use App\Models\Office;
use Livewire\Component;

class ClientCompany extends Component
{
    public $client_company_search_key = '';
    public $company_name = '';
    public $mother_company_name = '';
    public $company_type = '';
    public $owner_full_name = '';
    public $director_name = '';
    public $occupation = '';
    public $no = '';
    public $street_1 = '';
    public $street_2 = '';
    public $city = '';
    public $contact_1 = '';
    public $contact_2 = '';
    public $email = '';
    public $br_number = '';
    public $web_url = '';
    public $selected_client_company_id = '';
    public $office;
    public $offices;

// startClientCompanyInsert
    public
    function ClientCompanyDetailsInsert()
    {
        $company = new \App\Models\ClientCompany();
        $company->company_name = $this->company_name;
        $company->mother_Company = $this->mother_company_name;
        $company->company_type = $this->company_type;
        $company->owner_full_name = $this->owner_full_name;
        $company->director_name = $this->director_name;
        $company->occupation = $this->occupation;
        $company->contact_1 = $this->contact_1;
        $company->contact_2 = $this->contact_2;
        $company->no = $this->no;
        $company->street_1 = $this->street_1;
        $company->street_2 = $this->street_2;
        $company->city = $this->city;
        $company->email = $this->email;
        $company->br_number = $this->br_number;
        $company->web_url = $this->web_url;
        $company->office_id = 2;
        $company->save();
        $this->ClearCompanyDataFields();
    }//    endClientCompanyInsert

// startLoadSelectedClientCompanyDetails
    public
    function LoadSelectedClientDetails($selected_client_company)
    {
//        dd($selected_client_company);
        $this->selected_client_company_id = $selected_client_company['id'];
        $this->company_name = $selected_client_company['company_name'];
        $this->mother_company_name = $selected_client_company['mother_Company'];
        $this->company_type = $selected_client_company['company_type'];
        $this->owner_full_name = $selected_client_company['owner_full_name'];
        $this->director_name = $selected_client_company['director_name'];
        $this->occupation = $selected_client_company['occupation'];
        $this->no = $selected_client_company['no'];
        $this->street_1 = $selected_client_company['street_1'];
        $this->street_2 = $selected_client_company['street_2'];
        $this->city = $selected_client_company['city'];
        $this->contact_1 = $selected_client_company['contact_1'];
        $this->contact_2 = $selected_client_company['contact_2'];
        $this->email = $selected_client_company['email'];
        $this->br_number = $selected_client_company['br_number'];
        $this->web_url = $selected_client_company['web_url'];
    }//    endLoadSelectedClientDetails

//    startClientCompanyDetailsUpdate
    public
    function ClientCompanyDetailsUpdate()
    {
        $company = \App\Models\ClientCompany::find($this->selected_client_company_id);
        $company->company_name = $this->company_name;
        $company->mother_Company = $this->mother_company_name;
        $company->company_type = $this->company_type;
        $company->owner_full_name = $this->owner_full_name;
        $company->director_name = $this->director_name;
        $company->occupation = $this->occupation;
        $company->contact_1 = $this->contact_1;
        $company->contact_2 = $this->contact_2;
        $company->no = $this->no;
        $company->street_1 = $this->street_1;
        $company->street_2 = $this->street_2;
        $company->city = $this->city;
        $company->email = $this->email;
        $company->br_number = $this->br_number;
        $company->web_url = $this->web_url;
        $company->save();
        $this->ClearCompanyDataFields();
    }//    endClientCompanyDetailsUpdate

// startDeactivateSelectedClientCompanyDetails
    public
    function DeactivateSelectedClientCompanyDetails($selected_client_company)
    {
        $client_company_active = \App\Models\ClientCompany::find($selected_client_company['id']);
        $client_company_active->is_active = '0';
        $client_company_active->save();
        $this->ClearCompanyDataFields();
    }//    endDeactivateSelectedClientCompanyDetails

// startActivateSelectedClientCompanyDetails
    public
    function ActivateSelectedClientCompanyDetails($selected_client_company)
    {
        $client_company_active = \App\Models\ClientCompany::find($selected_client_company['id']);
        $client_company_active->is_active = '1';
        $client_company_active->save();
        $this->ClearCompanyDataFields();
    }//    endActivateSelectedClientCompanyDetails

// startClearCompanyDataFields
    public
    function ClearCompanyDataFields()
    {
        $this->company_name = '';
        $this->mother_company_name = '';
        $this->company_type = '';
        $this->owner_full_name = '';
        $this->director_name = '';
        $this->occupation = '';
        $this->no = '';
        $this->street_1 = '';
        $this->street_2 = '';
        $this->city = '';
        $this->contact_1 = '';
        $this->contact_2 = '';
        $this->email = '';
        $this->br_number = '';
        $this->web_url = '';
    }//    endClearCompanyDataFields

// srartRenderMethod
    public
    function render()
    {
        $this->offices = Office::where('is_active', '1')->get();
        $client_companies = \App\Models\ClientCompany::where('br_number', 'like', '%' . $this->client_company_search_key . '%')
            ->orwhere('company_name', 'like', '%' . $this->client_company_search_key . '%')->paginate(5);
        return view('livewire.client.client-company', ['client_companies' => $client_companies,'offices'=>$this->offices])->layout('layouts.main');
    }//    endRenderMethod
}
