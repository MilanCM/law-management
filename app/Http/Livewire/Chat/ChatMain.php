<?php

namespace App\Http\Livewire\Chat;

use App\Events\MessageSent;
use App\Models\Conversation;
use App\Models\Message;
use App\Models\User;
use Dflydev\DotAccessData\Data;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use function Nette\Utils\Strings;

class ChatMain extends Component
{
    use WithFileUploads;

    public $auth_id;
    public $conversations;
    public $users;
    public $message = 'Hi, How are you';
    public $receiver_instance;
    public $name;
    public $user_id;

    public $messages;
    public $messages_count;
    public $paginateVar = 10;
    public $receiver;
    public $selectedConversation;
    public $body;
    public $height;
    public $createMessage;

    public $photos = [];
    public $url;

    protected $listeners = ['loadMore', 'updateHeight', 'resetChat', 'myFunctionName'];

//    public function getListeners()
//    {
//        $auth_id = auth()->user()->id;
//        return ["echo-private:chat.{$auth_id},MessageSent" => 'broadcastMessageReceived', 'loadMore',
//            'updateHeight', 'dispatchMessageSent'];
//    }

    public function removeMe($index)
    {
        array_splice($this->photos, $index, 1);
    }

    public function removeMessage($message)
    {
        $chat = Message::where('conversation_id', $message['conversation_id'])->get();
        $this->messages->forget($chat->where('sender_id', $this->auth_id)->count());
        Message::find($message['id'])->delete();

        $conversation = Conversation::find($message['conversation_id']);
        $last_msg = Message::where('conversation_id', $message['conversation_id'])->get();

        $this->messages = Message::where('conversation_id', $this->selectedConversation->id)
            ->skip($this->messages_count - $this->paginateVar)
            ->take($this->paginateVar)->get();
        if ($chat->count() == 1) {
            Conversation::find($message['conversation_id'])->delete();
            $this->resetChat();
            $this->mount();
        } else {
            $conversation->last_time_message = $last_msg->last()->created_at;
            $conversation->save();
            $this->mount();
        }

    }

    public function resetChat()
    {
        $this->selectedConversation = null;
        $this->receiver = null;
    }

    public function myFunctionName()
    {
        if ($this->receiver != null) {
            $unread_messages = Message::where('receiver_id', auth()->id())->where('read', 0)->get();
            $unmc = $unread_messages->count();
//            dd($unmc);
            if ($unmc != 0) {
//                foreach ($unread_messages as $unread_message) {
//                    if ($this->receiver != null) {
//                        if ($this->receiver->id == $unread_message->sender_id) {
//                            dd('if');
//                            $this->loadConversation($unread_message->conversation, $unread_message->sender_id);
//                            $this->loadConversation($this->selectedConversation, $this->receiver->id);
//                        }
//                    }
//                }
            }
// else {
////            error_log($unread_message);
//            }
        }
    }

//    function broadcastMessageReceived($event)
//    {
//        dd($event);
//    }

    public
    function CheckConversation($receiverId)
    {
        $checkedConversation = Conversation::where('receiver_id', auth()->user()->id)->where('sender_id', $receiverId)
            ->orWhere("receiver_id", $receiverId)->where('sender_id', auth()->user()->id)->get();

        if (count($checkedConversation) == 0) {
            $createChat = Conversation::create(['receiver_id' => $receiverId, 'sender_id' => auth()->user()->id, 'last_time_message' => null]);
            $createMessage = Message::create(['conversation_id' => $createChat->id, 'sender_id' => auth()->user()->id, 'receiver_id' => $receiverId, 'body' => $this->message]);

            $createChat->last_time_message = $createMessage->created_at;
            $createChat->save();
            $this->loadConversation($createChat, $receiverId);

        } elseif (count($checkedConversation) >= 1) {

        }
        $this->mount();
    }

    public
    function loadConversation(Conversation $conversation, $user_id)
    {
        $this->receiver = User::find($user_id);
        $this->selectedConversation = $conversation;

        $this->messages_count = Message::where('conversation_id', $this->selectedConversation->id)->count();
        $this->messages = Message::where('conversation_id', $this->selectedConversation->id)
            ->skip($this->messages_count - $this->paginateVar)
            ->take($this->paginateVar)->get();

        $this->checkReadStatus($conversation, $this->receiver);
        $this->dispatchBrowserEvent('chatSelected');
        $this->delete_tmp();

    }

    public
    function checkReadStatus(Conversation $conversation, User $user)
    {
        $read = Message::where('conversation_id', $this->selectedConversation->id)->where('receiver_id', auth()->id())
            ->where('read', 0)->get();
        if (count($read) != 0) {
            foreach ($read as $msg) {
                $msg->read = 1;
                $msg->save();
            }
        }
    }

    public
    function delete_tmp()
    {
        if (Storage::exists('livewire-tmp')) {
            Storage::deleteDirectory('livewire-tmp');
        }
    }

    public
    function sendMessage()
    {
        if (count($this->photos) > 0) {
            foreach ($this->photos as $photo) {
                $this->url = Storage::disk('public')->put('uploads/MessageMedia/' . auth()->id() . '/' . $this->receiver->id, $photo);
                $this->createMessage = Message::create([
                    'conversation_id' => $this->selectedConversation->id,
                    'sender_id' => auth()->id(),
                    'receiver_id' => $this->receiver->id,
                    'media_file' => $this->url,
                    'type' => explode('-.', $photo->getFileName())[1],
                ]);

                $this->selectedConversation->last_time_message = $this->createMessage->created_at;
                $this->selectedConversation->save();

                $newMessage = Message::find($this->createMessage->id);
                $this->messages->push($newMessage);
            }
        } elseif ($this->body != null) {
            $this->createMessage = Message::create([
                'conversation_id' => $this->selectedConversation->id,
                'sender_id' => auth()->id(),
                'receiver_id' => $this->receiver->id,
                'body' => $this->body,
            ]);

            $this->selectedConversation->last_time_message = $this->createMessage->created_at;
            $this->selectedConversation->save();

//            $newMessage = Message::find($this->createMessage->id);
//            $this->messages->push($newMessage);
            $this->loadConversation($this->selectedConversation,$this->receiver->id);
            $this->render();
        } else {
            return null;
            $this->delete_tmp();
        }

        $this->delete_tmp();
        $this->reset('body', 'photos');
        $this->dispatchBrowserEvent('rowChatBottom');
    }

    public
    function loadMore()
    {
        $this->paginateVar = $this->paginateVar + 10;
        $this->messages_count = Message::where('conversation_id', $this->selectedConversation->id)->count();
        $this->messages = Message::where('conversation_id', $this->selectedConversation->id)
            ->skip($this->messages_count - $this->paginateVar)
            ->take($this->paginateVar)->get();
        $this->dispatchBrowserEvent('updateHeight', ($this->height));
    }

    public
    function updateHeight($height)
    {
        $this->height = $height;
    }

    public
    function getChatUserInstance(Conversation $conversation, $request)
    {
        $this->auth_id = auth()->id();
        if ($conversation->sender_id == $this->auth_id) {
            $this->receiver_instance = User::firstWhere('id', $conversation->receiver_id);
        } else {
            $this->receiver_instance = User::firstWhere('id', $conversation->sender_id);
        }
        $this->user_id = $this->receiver_instance->id;
        if (isset($request)) {
            return $this->receiver_instance->$request;
        }
    }

    public
    function mount()
    {
        $this->auth_id = auth()->id();
        $this->conversations = Conversation::where('sender_id', $this->auth_id)->orWhere('receiver_id', $this->auth_id)
            ->orderBy('last_time_message', 'DESC')->get();
        $this->delete_tmp();
    }

    public
    function render()
    {
        $this->users = User::where('id', "!=", auth()->user()->id)->get();
        return view('livewire.chat.chat-main')->layout('layouts.main');
    }
}
