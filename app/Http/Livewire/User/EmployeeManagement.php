<?php

namespace App\Http\Livewire\User;

use App\Models\Employee;
use App\Models\Office;
use Dflydev\DotAccessData\Data;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;

class EmployeeManagement extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $image;
    public $fname;
    public $lname;
    public $mname;
    public $nic;
    public $email;
    public $gender;
    public $occupation;
    public $office;
    public $offices;
    public $contact1;
    public $contact2;
    public $no;
    public $street1;
    public $street2;
    public $city;
    public $marriage_status;
    public $selected_employee_id;
    public $employee_search_key;
    protected $paginationTheme = 'bootstrap';
    public $url;

// startInsertEmployeeDetails
    public function insertEmployeeDetails()
    {
        $this->validate([
            'office'=> 'required',
            'image' => 'image|mimes:png,jpg,pdf|max:1024', // 1MB Max
        ]);

        $this->url =  Storage::disk('public')->put( 'uploads/user', $this->image);
//        $this->url = Storage::url($this->image->store('photos'));

        $employee = new Employee();
        $employee->f_name = $this->fname;
        $employee->m_name = $this->mname;
        $employee->l_name = $this->lname;
        $employee->nic = $this->nic;
        $employee->email = $this->email;
        $employee->gender = $this->gender;
        $employee->occupation = $this->occupation;
        $employee->contact_1 = $this->contact1;
        $employee->contact_2 = $this->contact2;
        $employee->no = $this->no;
        $employee->street_1 = $this->street1;
        $employee->street_2 = $this->street2;
        $employee->city = $this->city;
        $employee->marriage_status = $this->marriage_status;
        $employee->image = $this->url;
        $employee->office_id = $this->office;
        $employee->user_role_id = 3;
        $employee->save();
        $this->clearFieldEmployeeDetails();
    }//    endInsertEmployeeDetails

// startLoadSelectedEmployeeDetails
    public function loadSelectedEmployeeDetails($selected_employee)
    {
        $this->selected_employee_id = $selected_employee['id'];
        $this->fname = $selected_employee['f_name'];
        $this->lname = $selected_employee['l_name'];
        $this->mname = $selected_employee['m_name'];
        $this->nic = $selected_employee['nic'];
        $this->email = $selected_employee['email'];
        $this->gender = $selected_employee['gender'];
        $this->occupation = $selected_employee['occupation'];
        $this->contact1 = $selected_employee['contact_1'];
        $this->contact2 = $selected_employee['contact_2'];
        $this->no = $selected_employee['no'];
        $this->street1 = $selected_employee['street_1'];
        $this->street2 = $selected_employee['street_2'];
        $this->city = $selected_employee['city'];
        $this->marriage_status = $selected_employee['marriage_status'];
        $this->image = $selected_employee['image'];
//        if(Storage::exists('upload/test.png')){
//            Storage::delete('upload/test.png');
//            /*
//                Delete Multiple File like this way
//                Storage::delete(['upload/example.png', 'upload/example2.png']);
//            */
//        }

    }//    endLoadSelectedEmployeeDetails

// startUpdateEmployeeDetails
    public function updateEmployeeDetails()
    {
        $selected_employee = Employee::find($this->selected_employee_id);
        $selected_employee->f_name = $this->fname;
        $selected_employee->m_name = $this->mname;
        $selected_employee->l_name = $this->lname;
        $selected_employee->nic = $this->nic;
        $selected_employee->email = $this->email;
        $selected_employee->gender = $this->gender;
        $selected_employee->occupation = $this->occupation;
        $selected_employee->contact_1 = $this->contact1;
        $selected_employee->contact_2 = $this->contact2;
        $selected_employee->no = $this->no;
        $selected_employee->street_1 = $this->street1;
        $selected_employee->street_2 = $this->street2;
        $selected_employee->city = $this->city;
        $selected_employee->marriage_status = $this->marriage_status;
        $selected_employee->image = $this->image;
        $selected_employee->image = 'image path';
        $selected_employee->save();
        $this->clearFieldEmployeeDetails();
    }//    endUpdateEmployeeDetails

// startActivateEmployeeDetails
    public function activateEmployeeDetails($selected_employee)
    {
    $activate_employee = Employee::find($selected_employee['id']);
    $activate_employee->is_active = 1;
    $activate_employee->save();
    $this->clearFieldEmployeeDetails();
}//    endActivateEmployeeDetails

// startDeactivateEmployeeDetails
    public function deactivateEmployeeDetails($selected_employee)
    {
        $deactivate_employee = Employee::find($selected_employee['id']);
        $deactivate_employee->is_active = 0;
        $deactivate_employee->save();
        $this->clearFieldEmployeeDetails();
}//    endDeactivateEmployeeDetails

// startClearFieldEmployee
    public function clearFieldEmployeeDetails()
    {
        $this->fname = '';
        $this->lname = '';
        $this->mname = '';
        $this->nic = '';
        $this->email = '';
        $this->gender = '';
        $this->occupation = '';
        $this->contact1 = '';
        $this->contact2 = '';
        $this->no = '';
        $this->street1 = '';
        $this->street2 = '';
        $this->city = '';
        $this->marriage_status = '';
        $this->selected_employee_id= null;
//        $this->mount();
    }//    endClearFieldEmployee

    public function mount()
    {
//        $this->offices = Office::where('is_active', '1')->get();
    }

// startRenderMethod
    public function render()
    {
        $this->offices = Office::where('is_active', '1')->get();
        $employees = Employee::where('id', 'like', '%' . $this->employee_search_key . '%')
            ->orwhere('f_name', 'like', '%' . $this->employee_search_key . '%')
            ->orwhere('nic', 'like', '%' . $this->employee_search_key . '%')->paginate(5);
        return view('livewire.user.employee-management', ['employees' => $employees, 'offices'=>$this->offices])->layout('layouts.main');;
    }//    endRenderMethod
}
