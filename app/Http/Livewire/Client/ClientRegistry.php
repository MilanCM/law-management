<?php

namespace App\Http\Livewire\Client;

use App\Models\Customer;
use App\Models\Office;
use Livewire\Component;
use Livewire\WithPagination;
use function Composer\Autoload\includeFile;

class ClientRegistry extends Component
{
    public $fname = '';
    public $lname = '';
    public $mnames = '';
    public $surname = '';
    public $initial_name = '';
    public $full_name = '';
    public $nic = '';
    public $gender = '';
    public $marriage_status = '';
    public $nationality = '';
    public $email = '';
    public $contact1 = '';
    public $contact2 = '';
    public $no = '';
    public $street1 = '';
    public $street2 = '';
    public $city = '';
    public $occupation = '';
    public $company_name = '';
    public $company_email = '';
    public $company_contact1 = '';
    public $company_contact2 = '';
    public $office;
    public $offices;

    public $client_search_key;
    public $active;
    public $deActive;

    public $selected_client_person_id = '';
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

// startClientInsert
    public function ClientPersonDetailsInsert()
    {
        $client = new Customer();
        $client->f_name = $this->fname;
        $client->m_name = $this->mnames;
        $client->l_name = $this->lname;
        $client->sur_name = $this->surname;
        $client->initial_name = $this->initial_name;
        $client->full_name = $this->full_name;
        $client->nic = $this->nic;
        $client->no = $this->no;
        $client->street_1 = $this->street1;
        $client->street_2 = $this->street2;
        $client->city = $this->city;
        $client->occupation = $this->occupation;
        $client->nationality = $this->nationality;
        $client->contact_1 = $this->contact1;
        $client->contact_2 = $this->contact2;
        $client->email = $this->email;
        $client->company_name = $this->company_name;
        $client->company_contact_1 = $this->company_contact1;
        $client->company_contact_2 = $this->company_contact2;
        $client->company_email = $this->company_email;
        $client->gender = $this->gender;
        $client->marriage_status = $this->marriage_status;
        $client->office_id = 1;
        $client->save();

    }//    endClientInsert

// startLoadSelectedClientDetails
    public function LoadSelectedClientDetails($selected_client_person)
    {
        $this->selected_client_person_id = $selected_client_person['id'];
        $this->fname = $selected_client_person['f_name'];
        $this->mnames = $selected_client_person['m_name'];
        $this->lname = $selected_client_person['l_name'];
        $this->surname = $selected_client_person['sur_name'];
        $this->initial_name = $selected_client_person['initial_name'];
        $this->full_name = $selected_client_person['full_name'];
        $this->nic = $selected_client_person['nic'];
        $this->no = $selected_client_person['no'];
        $this->street1 = $selected_client_person['street_1'];
        $this->street2 = $selected_client_person['street_2'];
        $this->city = $selected_client_person['city'];
        $this->occupation = $selected_client_person['occupation'];
        $this->nationality = $selected_client_person['nationality'];
        $this->contact1 = $selected_client_person['contact_1'];
        $this->contact2 = $selected_client_person['contact_2'];
        $this->email = $selected_client_person['email'];
        $this->company_name = $selected_client_person['company_name'];
        $this->company_contact1 =$selected_client_person['company_contact_1'];
        $this->company_contact2 =$selected_client_person['company_contact_2'];
        $this->company_email = $selected_client_person['company_email'];
        $this->gender = $selected_client_person['gender'];
        $this->marriage_status = $selected_client_person['marriage_status'];
}//    endLoadSelectedClientDetails

//    startClientPersonDetailsUpdate
    public function ClientPersonDetailsUpdate()
    {
        $client = Customer::find($this->selected_client_person_id);
        $client->f_name = $this->fname;
        $client->m_name = $this->mnames;
        $client->l_name = $this->lname;
        $client->sur_name = $this->surname;
        $client->initial_name = $this->initial_name;
        $client->full_name = $this->full_name;
        $client->nic = $this->nic;
        $client->no = $this->no;
        $client->street_1 = $this->street1;
        $client->street_2 = $this->street2;
        $client->city = $this->city;
        $client->occupation = $this->occupation;
        $client->nationality = $this->nationality;
        $client->contact_1 = $this->contact1;
        $client->contact_2 = $this->contact2;
        $client->email = $this->email;
        $client->company_name = $this->company_name;
        $client->company_contact_1 = $this->company_contact1;
        $client->company_contact_2 = $this->company_contact2;
        $client->company_email = $this->company_email;
        $client->gender = $this->gender;
        $client->marriage_status = $this->marriage_status;
        $client->save();
        $this->ClearPersonDataFields();
}//    endClientPersonDetailsUpdate

// startDeactivateSelectedClientDetails
    public function DeactivateSelectedClientDetails($selected_client_person)
    {
        $client_active = Customer::find($selected_client_person['id']);
        $client_active->is_active = '0';
        $client_active->save();
        $this->ClearPersonDataFields();
}//    endDeactivateSelectedClientDetails

// startActivateSelectedClientDetails
    public function ActivateSelectedClientDetails($selected_client_person)
    {
        $client_active = Customer::find($selected_client_person['id']);
        $client_active->is_active = '1';
        $client_active->save();
        $this->ClearPersonDataFields();
}//    endActivateSelectedClientDetails

// startClearPersonDataFields
    public function ClearPersonDataFields()
    {
        $this->fname = '';
        $this->lname = '';
        $this->mnames = '';
        $this->surname = '';
        $this->initial_name = '';
        $this->full_name = '';
        $this->nic = '';
        $this->gender = '';
        $this->marriage_status = '';
        $this->nationality = '';
        $this->email = '';
        $this->contact1 = '';
        $this->contact2 = '';
        $this->no = '';
        $this->street1 = '';
        $this->street2 = '';
        $this->city = '';
        $this->occupation = '';
        $this->company_name = '';
        $this->company_email = '';
        $this->company_contact1 = '';
        $this->company_contact2 = '';
    }//    endClearPersonDataFields

// startRenderMethod
    public function render()
    {
        $this->initial_name = 'W.A.S.S.Werasuriya';
        $this->full_name = 'Werasuriya Arachchilage Saman Sampath Werasuriya';
        if (!$this->fname == '') {
            $initial_family_name = [];
            $middle_names = [];
            if (!$this->surname == '') {
                foreach (explode(' ', $this->surname) as $family_name) {
                    array_push($initial_family_name, mb_substr($family_name, 0, 1) . ' ');
                }
            }
            if (!$this->mnames == '') {
                foreach (explode(' ', $this->mnames) as $middle_name) {
                    array_push($middle_names, mb_substr($middle_name, 0, 1) . ' ');
                }
            }
            $this->initial_name = strtoupper(implode('', $initial_family_name) . mb_substr($this->fname, 0, 1) . ' ' . implode('', $middle_names) . ' ' . $this->lname);
            $this->full_name = strtoupper($this->surname . ' ' . $this->fname . ' ' . $this->mnames . ' ' . $this->lname);
        }

        $this->offices = Office::where('is_active', '1')->get();
        $customers = Customer::where('id', 'like', '%' . $this->client_search_key . '%')
            ->orwhere('f_name', 'like', '%' . $this->client_search_key . '%')
            ->orwhere('nic', 'like', '%' . $this->client_search_key . '%')->paginate(5);
        return view('livewire.client.client-registry', ['customers' => $customers,'offices'=>$this->offices])->layout('layouts.main');
    }//    startRenderMethod
}
