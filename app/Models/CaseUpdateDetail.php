<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CaseUpdateDetail extends Model
{
    use HasFactory;

    public function law_case()
    {
        return $this->hasOne(LawCase::class, 'id', 'law_case_id');
    }
}
