<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('f_name');
            $table->string('m_name');
            $table->string('l_name');
            $table->string('sur_name');
            $table->string('initial_name');
            $table->string('full_name');
            $table->string('nic')->unique();
            $table->string('no');
            $table->string('street_1');
            $table->string('street_2')->nullable();
            $table->string('city');
            $table->string('occupation');
            $table->string('nationality');
            $table->integer('contact_1');
            $table->integer('contact_2')->nullable();
            $table->string('email')->nullable();
            $table->string('company_name');
            $table->integer('company_contact_1');
            $table->integer('company_contact_2')->nullable();
            $table->string('company_email')->nullable();
            $table->string('gender');
            $table->string('marriage_status');
            $table->boolean('is_active')->default(2);

            $table->unsignedBigInteger('office_id');

            $table->foreign('office_id')->references('id')->on('offices');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
