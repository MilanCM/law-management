<?php

namespace App\Http\Livewire\Utilities;

use Livewire\Component;

class WebResources extends Component
{
    public function render()
    {
        return view('livewire.utilities.web-resources')->layout('layouts.main');
    }
}
