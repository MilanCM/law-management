<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TemporaryFile;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public $folder_name = 'case_media_folder';

    public function store(Request $request)
    {
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $filename = uniqid() . '-' . now()->timestamp . '.' . $file->extension();
//            $file->storeAs('avatars/tmp/' . $this->folder_name, $filename);
            Storage::putFileAs('avatars/tmp/' . $this->folder_name, $file, $filename, 'public');

            TemporaryFile::create([
                'folder' => $this->folder_name,
                'filename' => $filename,
                'extension' => $file->extension()
            ]);
            return $filename;
        }
        return '';
    }

    public function delete(Request $request)
    {
        Storage::delete('avatars/tmp/' . $this->folder_name . '/' . $request->getContent());
        $file_delete = TemporaryFile::where('folder', $this->folder_name)
            ->where('filename', $request->getContent())->first();
        $file_delete->delete();
    }
}
