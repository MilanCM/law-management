<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lawyers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lawyer_title')->nullable();
            $table->string('f_name')->nullable();
            $table->string('m_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('surname')->nullable();
            $table->string('nic')->nullable();
            $table->string('license_no')->unique();
            $table->integer('contact_1')->nullable();
            $table->integer('contact_2')->nullable();
            $table->string('email');
            $table->string('gender')->nullable();
            $table->string('no')->nullable();
            $table->string('street_1')->nullable();
            $table->string('street_2')->nullable();
            $table->string('city')->nullable();
            $table->string('marriage_status')->nullable();
            $table->string('image')->nullable();
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('user_role_id')->default('1');

            $table->foreign('user_role_id')->references('id')->on('user_roles');
            $table->foreign('category_id')->references('id')->on('lawyer_categories');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lawyers');
    }
}
