@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Employee Registry</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Employee Registry</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>
    <div class="container">
        <div class="row">
            <div class="card-tools">
                <input type="text" name="table_search" class="form-control float-right"
                       placeholder="Search" style="width: 350px;" wire:model="employee_search_key">
            </div>
            <div class="input-group-append">
                <button type="submit" class="btn btn-default">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Employee List</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
            <table class="table text-nowrap">
                <thead style="background-color: rgba(185,202,209,0.62)">
                <tr>
                    <th>ID</th>
                    <th>#</th>
                    <th>User</th>
                    <th>Gender</th>
                    <th>Address</th>
                    <th>Date</th>
                    <th>NIC</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($employees as $employee)
                    <tr wire:click="loadSelectedEmployeeDetails({{$employee}})">
                        <td>{{$employee->id}}</td>
                        <td>
                            <img class="img-circle elevation-2" width="40px" height="40px"
                                 src="{{asset('storage/'.$employee->image)}}">

                        </td>
                        <td>{{$employee->f_name}} {{$employee->l_name}}</td>
                        <td>{{$employee->gender}}</td>
                        <td>{{$employee->no}}, {{$employee->city}}</td>
                        {{--                                        <td>{{$employee->enroll_date}}</td>--}}
                        <td><span class="tag tag-success">{{$employee->nic}}</span></td>
                        @if($employee->is_active == 1)
                            <td><span class="badge badge-success">Active</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-xs "
                                        wire:click="deactivateEmployeeDetails({{$employee}})"><i class="fa fa-trash"
                                                                                                 aria-hidden="true"></i>
                                </button>
                            </td>

                        @else
                            <td><span class="badge badge-danger">Deactive</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-success btn-xs "
                                        wire:click="activateEmployeeDetails({{$employee}})"><i
                                        class="fa fa-trash-restore"
                                        aria-hidden="true"></i>
                                </button>
                            </td>
                        @endif
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="mt-2">
        {{$employees->links()}}
    </div>
    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Registry Form</h3>
        </div>
        <!-- /.card-header -->


        <!-- form start -->
        @if (\Session::has('message'))
            <script>
                $(window).scrollTop(10);
            </script>
            <div class="alert alert-success alert-block" role="alert" id="alert">
                <button type="button" class="close" data-dismiss="alert">X</button>
                <span>{!! \Session::get('message') !!}</span>
            </div>
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">First Name<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2"
                               placeholder="Saman" wire:model="fname">
                        @error('fname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Middle Name / Names</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Sampath" wire:model="mname">
                        @error('mname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Last Name</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Werasuriya" wire:model="lname">
                        @error('lname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">NIC Number</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               id="exampleInputPassword1"
                               placeholder="Enter NIC" wire:model="nic">
                        @error('nic') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Gender </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="gender"
                                id="exampleSelectBorderWidth2">
                            <option></option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Marriage Status </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="marriage_status"
                                id="exampleSelectBorderWidth2">
                            <option></option>
                            <option>Single</option>
                            <option>Married</option>
                            <option>Widow</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label class="mb-sm-0">Email</label>
                        <input type="email" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter email" wire:model="email">
                        @error('email') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Contact</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 1"
                                           wire:model="contact1">
                                </label>
                                @error('contact1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 2"
                                           wire:model="contact2">
                                </label>
                                @error('contact2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Address</label>
                        <div class="row">
                            <div class="col-sm-1">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="no."
                                       wire:model="no">
                                @error('no') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street1"
                                       wire:model="street1">
                                @error('street1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street2"
                                       wire:model="street2">
                                @error('street2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="city"
                                       wire:model="city">
                                @error('city') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Occupation</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter Occupation" wire:model="occupation">
                        @error('occupation') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Office / Branch</label>
                        <div class="col-sm-10" wire:ignore>
                            <select
                                class="custom-select @if($offices->count() === 0 || $errors->first('offices')) is-invalid @endif"
                                id="office" data-placeholder="Choose anything" data-allow-clear="1"
                                @if(is_null($offices)) disabled @endif>
                                @if($offices->count() !== 0)
                                    <option></option>
                                    @foreach($offices as $office)
                                        <option value="{{ $office->id }}">{{ $office->office_name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        @error('office') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div>
                <input type="file" wire:model="image">
                @if($selected_employee_id)
                    <img class="profile-user-img img-fluid" src="{{asset('storage/'.$image)}}"
                         alt="img-fluid">
                @else
                    @if ($image)
                        <div>
                            <img class="profile-user-img img-fluid" src="{{ $image->temporaryUrl() }}"
                                 alt="img-fluid">
                        </div>
                    @endif
                    @error('image') <span class="error">{{ $message }}</span> @enderror
                @endif
            </div>
            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-primary" wire:click.prevent="insertEmployeeDetails()">Submit</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="updateEmployeeDetails()">Update</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>

    </div>
</main>

@push('styles')
    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('select2/select2-bootstrap4.css') }}">

@endpush

@push('scripts')
    <script src="{{ asset('select2/select2.min.js') }}"></script>
    <script>
        $('#office').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an Office'
        });

        $('#office').on('change', function () {
        @this.office
            = $(this).val();
        });

    </script>
@endpush

