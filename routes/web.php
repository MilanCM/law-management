<?php

use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin;
use App\Http\Livewire\Client;
use App\Http\Livewire\User;
use App\Http\Livewire\Lawyer;
use App\Http\Livewire\Appointment;
use App\Http\Livewire\Document;
use App\Http\Livewire\Utilities;
use App\Http\Livewire\Settings;
use App\Http\Livewire\Account;
use App\Http\Livewire\LawCase;
use App\Http\Livewire\Chat;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', Admin\Dashboard::class)->name('home');

Route::group(['prefix' => 'administrator', 'as' => 'administrator.'], function () {
    Route::get('settings', Admin\Settings::class)->name('settings');
});

//Client
Route::group(['prefix' => 'client', 'as' => 'client.'], function () {
    Route::get('client_registry', Client\ClientRegistry::class)->name('client_registry');
    Route::get('client_company', Client\ClientCompany::class)->name('client_company');
    Route::get('client_view', Client\ViewClient::class)->name('client_view');
});
//User
Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
    Route::get('emp_management', User\EmployeeManagement::class)->name('emp_management');
    Route::get('manage_account', User\ManageAccount::class)->name('manage_account');
    Route::get('user_view', User\ViewUser::class)->name('user_view');
});
//LawCases
Route::group(['prefix' => 'lawcase', 'as' => 'lawcase.'], function () {
    Route::get('lawcase_registry', LawCase\LawcaseRegistry::class)->name('lawcase_registry');
    Route::get('lawcase_upgrade', LawCase\LawcaseUpgrade::class)->name('lawcase_upgrade');
    Route::get('lawcase_view', LawCase\LawcaseView::class)->name('lawcase_view');
    Route::get('manage_attachment', LawCase\ManageAttachment::class)->name('manage_attachment');
    Route::get('manage_category', LawCase\ManageAttachment::class)->name('manage_category');
});
//Lawyers
Route::group(['prefix' => 'lawyer', 'as' => 'lawyer.'], function () {
    Route::get('senior_lawyer_registry', Lawyer\SeniorLawyerRegistry::class)->name('senior_lawyer_registry');
    Route::get('junior_lawyer_registry', Lawyer\JuniorLawyerRegistry::class)->name('junior_lawyer_registry');
    Route::get('lawyer_office_registry', Lawyer\LawyerOfficeRegistry::class)->name('lawyer_office_registry');
});
//Appointment
Route::group(['prefix' => 'appointment', 'as' => 'appointment.'], function () {
    Route::get('task_management', Appointment\TaskManagement::class)->name('task_management');
    Route::get('appointment_management', Appointment\AppointmentManagement::class)->name('appointment_management');
});
//Document
Route::group(['prefix' => 'document', 'as' => 'document.'], function () {
    Route::get('template_document', Document\TemplateFormat::class)->name('template_document');
});
//Utilities
Route::group(['prefix' => 'utilities', 'as' => 'utilities.'], function () {
    Route::get('law_fields', Utilities\LawFields::class)->name('law_fields');
    Route::get('book_resource', Utilities\BookResources::class)->name('book_resource');
    Route::get('web_resource', Utilities\WebResources::class)->name('web_resource');
});
//Settings
Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
    Route::get('privileges_management', Settings\PrivilegesManagement::class)->name('privileges_management');

});
//Account
Route::group(['prefix' => 'account', 'as' => 'account.'], function () {
    Route::get('client_payment', Account\ClientPaymentManagement::class)->name('client_payment');
    Route::get('expenses_management', Account\ExpensesManagement::class)->name('expenses_management');
    Route::get('salary_management', Account\SalaryManagement::class)->name('salary_management');
});
//chat
Route::group(['prefix' => 'chat', 'as' => 'chat.'], function () {
    Route::get('chat{key?}', Chat\ChatMain::class)->name('chat');
});

Route::post('upload', [\App\Http\Controllers\UploadController::class, 'store']);
Route::delete('upload', [\App\Http\Controllers\UploadController::class, 'delete']);
