@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Case Upgrade</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Case Upgrade</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>
    <div class="card-body">
        <div class="col-sm-10" wire:ignore>
            <select
                class="custom-select @if(sizeof($all_cases) === 0 || $errors->first('all_cases'))
                    is-invalid @endif" id="selected_law_case_tag" data-placeholder="Choose anything"
                data-allow-clear="1"
                @if(is_null($all_cases)) disabled @endif>
                @if(sizeof($all_cases) !== 0)
                    <option></option>
                    @foreach($all_cases as $case)
                        <option
                            value="{{ $case['id']}}">{{ $case['case_number']}} -
                            @if($case->client_person !== null){{$case->client_person['f_name']}}
                            . {{$case->client_person['l_name']}}- {{$case->client_person['nic']}}
                            @elseif($case->client_company !==
                            null){{$case->client_company['company_name']}} -
                            <small>{{$case->client_company['br_number']}}</small>
                            @endif
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>

    <div class="row">
        @if(sizeof($upgraded_cases) !== 0)

            <div class="col-md-5 d-flex align-items-stretch flex-column">
                {{--            <div class="card bg-light d-flex flex-fill">--}}

                <div class="card card-info mt-3">
                    <div class="card-header">
                        <h3 class="card-title">Upgraded Cases List</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body table-responsive p-0">
                        <table class="table-striped table-sm table">
                            <thead style="background-color: rgba(185,202,209,0.62)">
                            <tr>
                                {{--                    <th>#</th>--}}
                                <th>Date of Trial</th>
                                <th>Court Location</th>
                                <th>Next Date of Trial</th>

{{--                                <th></th>--}}
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($upgraded_cases as $upgraded_case)
                                <tr>
                                    {{--                        <td>{{$key+1}}</td>--}}
                                    <td><small>{{$upgraded_case['date_of_trial']}}</small></td>
                                    <td><small>{{$upgraded_case['court_location']}}</small></td>
                                    <td><small>{{$upgraded_case['next_date_of_trial']}}</small></td>

                                    @if($upgraded_case->is_active == 1)
{{--                                        <td><span class="badge badge-success mt-3">Active</span></td>--}}
                                        <td>
                                            <button type="button" class="btn btn-outline-danger btn-xs"
                                                    wire:click="DeactivateSelectedCaseDetails({{$upgraded_case}})"><i
                                                    class="fa fa-trash"
                                                    aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-info btn-xs"
                                                    wire:click="LoadSelectedCaseDetails({{$upgraded_case}})"><i
                                                    class="fa fa-edit"
                                                    aria-hidden="true"></i>
                                            </button>

                                        </td>

                                    @else
{{--                                        <td><span class="badge badge-danger">Deactive</span></td>--}}
                                        <td>
                                            <button type="button" class="btn btn-outline-success btn-xs"
                                                    wire:click="ActivateSelectedCaseDetails({{$upgraded_case}})"><i
                                                    class="fa fa-trash-restore"
                                                    aria-hidden="true"></i>
                                            </button>
                                            <button type="button" class="btn btn-outline-info btn-xs"
                                                    wire:click="LoadSelectedCaseDetails({{$upgraded_case}})"><i
                                                    class="fa fa-edit"
                                                    aria-hidden="true"></i>
                                            </button>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <!-- /.card-body -->
                </div>
                <div class="mt-2">
                    {{$upgraded_cases->links()}}
                </div>
                {{--            </div>--}}
            </div>
        @endif
        <div class="col-md-7 d-flex align-items-stretch flex-column">
            <div class="card bg-light d-flex flex-fill">
                <div class="card-header text-muted border-bottom-0">
                    Case Details
                </div>
                @if($searched_law_case)
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="lead"><b>{{$searched_law_case['case_number']}}</b></h2>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-calendar-day"></i></span>
                                        <b> Case Open Date: </b>{{$searched_law_case['case_opened_date']}}
                                    </li>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-location-arrow"></i></span><b>Court
                                            Location: </b>{{$searched_law_case['court_location']}}
                                    </li>
                                    <div class="form-group"></div>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-user"></i></span><b>Assigned
                                            Lawyer: </b>{{$searched_law_case->assigned_lawyer['f_name']}}
                                        . {{$searched_law_case->assigned_lawyer['l_name']}}
                                    </li>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-address-card"></i></span><b> Licence No. : </b>
                                        {{$searched_law_case->assigned_lawyer['license_no']}}
                                    </li>
                                    <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                        Phone #: {{$searched_law_case->assigned_lawyer['contact_1']}}
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-6 mt-sm-4">
                                @if($searched_law_case->client_person !== null)
                                    <p class="text-muted text-sm mt-2">
                                        <b>Client: </b> {{$searched_law_case->client_person['initial_name']}}</p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i
                                                    class="fas fa-sm fa-address-card"></i></span>
                                            NIC: {{$searched_law_case->client_person['nic']}}
                                        </li>
                                        <li class="small"><span class="fa-li"><i
                                                    class="fas fa-sm fa-building"></i></span>
                                            Address: {{$searched_law_case->client_person['no']}}
                                            , {{$searched_law_case->client_person['street_1']}}@if($searched_law_case->client_person['street_2'] !== null || $searched_law_case->client_person['street_2'] !== '')
                                                , {{$searched_law_case->client_person['street_2']}}@endif
                                            , {{$searched_law_case->client_person['city']}}
                                        </li>
                                        <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                            Phone #: + 94 {{$searched_law_case->client_person['contact_1']}}
                                        </li>
                                    </ul>
                                @elseif($searched_law_case->client_company !==null)
                                    <p class="text-muted text-sm mt-2">
                                        <b>Client: </b> {{$searched_law_case->client_company['company_name']}}</p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i
                                                    class="fas fa-sm fa-address-card"></i></span> NIC/
                                            BR: {{$searched_law_case->client_company['br_number']}}
                                        </li>
                                        <li class="small"><span class="fa-li"><i
                                                    class="fas fa-sm fa-building"></i></span>
                                            Address: {{$searched_law_case->client_company['no']}}
                                            , {{$searched_law_case->client_company['street_1']}}@if($searched_law_case->client_company['street_2'] !== null || $searched_law_case->client_company['street_2'] !== '')
                                                , {{$searched_law_case->client_company['street_2']}}@endif
                                            , {{$searched_law_case->client_company['city']}}
                                        </li>
                                        <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                            Phone #: + 94 {{$searched_law_case->client_company['contact_1']}}
                                        </li>
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2 class="lead"><b>CRIM- 2022/08</b></h2>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-calendar-day"></i></span>
                                        <b> Case Open Date: </b>2022/08/01
                                    </li>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-location-arrow"></i></span><b>Court Location: </b>Colombo
                                        Court
                                    </li>
                                    <div class="form-group"></div>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-user"></i></span><b>Assigned Lawyer: </b>Saman
                                        Kumara
                                    </li>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-address-card"></i></span><b> Licence No. : </b>
                                        LW-20220803
                                    </li>
                                    <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                        Phone #: + 94 747845856
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-6 mt-sm-4">
                                <p class="text-muted text-sm mt-2"><b>Client: </b> Saman Kumara</p>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-address-card"></i></span> NIC/ BR: 971478524V
                                    </li>
                                    <li class="small"><span class="fa-li"><i
                                                class="fas fa-sm fa-building"></i></span> Address: 222/2, Hill St,
                                        Colombo
                                    </li>
                                    <li class="small"><span class="fa-li"><i class="fas fa-sm fa-phone"></i></span>
                                        Phone #: + 94 747845856
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card-footer">
                    <div class="text-right">
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="fas fa-eye"></i> View Case Details
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Case Upgrade Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            @if (\Session::has('message'))
                <script>
                    $(window).scrollTop(10);
                </script>
                <div class="alert alert-success alert-block" role="alert" id="alert">
                    <button type="button" class="close" data-dismiss="alert">X</button>
                    <span>{!! \Session::get('message') !!}</span>
                </div>
            @endif

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Court Location<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2 @error('location') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="High Court" wire:model="location">
                        @error('location') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Date of Trial<code> *</code></label>
                        <input type="date"
                               class="form-control form-control-sm form-control-border border-width-2 @error('date') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="2022/08/02" wire:model="date">
                        @error('date') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Next Date of Trial<code> *</code></label>
                        <input type="date"
                               class="form-control form-control-sm form-control-border border-width-2 @error('next_date') is-invalid @enderror"
                               id="exampleInputEmail1"
                               placeholder="2022/08/02" wire:model="next_date">
                        @error('next_date') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="form-group">
                        <label for="inputDescription">Description</label>
                        <textarea id="inputDescription"
                                  class="form-control form-control-sm form-control-border border-width-2 @error('description') is-invalid @enderror"
                                  rows="6"
                                  wire:model="description"></textarea>
                        @error('description') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputDescription">Comments</label>
                        <textarea id="inputComment"
                                  class="form-control form-control-sm form-control-border border-width-2 @error('comment') is-invalid @enderror"
                                  rows="4"
                                  wire:model="comment"></textarea>
                        @error('comment') <span
                            class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group" wire:ignore>
                    <input type="file" name="avatar" id="avatar" multiple accept="">
                </div>


                @if($images_upg)
                    <div class="d-flex flex-wrap">
                        @foreach($images_upg as $image)
                            <div class="card mr-2 d-flex flex-column justify-content-between">
                                @if($image['file_type'] === 'jpg' || $image['file_type'] === 'png' ||$image['file_type'] === 'jpeg')
                                    <img style="width: 150px; height: 150px"
                                         src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}"
                                         alt="...">
                                    <a class="badge badge-pill badge-danger text-center"
                                       wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>
                                @elseif($image['file_type'] === 'ogg'||$image['file_type'] === 'mp3'|| $image['file_type'] === 'm4a')
                                    <audio controls  style="width: 150px; height: 150px">
                                        <source
                                            src="{{asset('storage/uploads/law_case/'.$image['law_case_id'].'/'.$image['file_path'])}}">
                                    </audio>
                                    <a class="badge badge-pill badge-danger text-center"
                                       wire:click.prevent="removeMediaFile({{$image}})">DELETE</a>
                                @elseif($image['file_type']=='pptx' ||$image['file_type']=='pdf' ||$image['file_type']=='docx' ||$image['file_type']=='txt')
                                    <iframe
                                        src="https://drive.google.com/file/d/19ViaXuJzhlIyMMZkgeLx1eFD4-xoAaFk/view"
                                        frameborder="0">
                                    </iframe>
                                @endif
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>


            <div class="col-12 d-flex align-items-stretch flex-column mt-3">
                <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0 text-bold">
                        <u> Witnesses Details</u>
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">First Name<code> *</code></label>
                                    <input type="text"
                                           class="form-control form-control-sm form-control-border border-width-2 @error('witness_f_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Saman" wire:model="witness_f_name">
                                    @error('witness_f_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">Middle Name / Names</label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('witness_m_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Sampath" wire:model="witness_m_name">
                                    @error('witness_m_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="mb-sm-0">Last Name</label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('witness_l_name') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Werasuriya" wire:model="witness_l_name">
                                    @error('witness_l_name') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label class="mb-sm-0">Family Name</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_family_name') is-invalid @enderror bg-transparent"
                                                   id="exampleInputEmail1"
                                                   placeholder="Werasuriya Arachchilage"
                                                   wire:model="witness_family_name">
                                            @error('witness_family_name') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label class="mb-sm-0">NIC Number</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_nic') is-invalid @enderror bg-transparent"
                                                   id="exampleInputPassword1"
                                                   placeholder="Enter NIC" wire:model="witness_nic">
                                            @error('witness_nic') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>

                                        <div class="col-sm-3">
                                            <label class="mb-sm-0">Relationship</label>
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_relationship') is-invalid @enderror bg-transparent"
                                                   id="exampleInputPassword1"
                                                   placeholder="Enter NIC" wire:model="witness_relationship">
                                            @error('witness_relationship') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <label class="mb-sm-0">Email</label>
                                    <input type="email"
                                           class="form-control form-control-sm  form-control-border border-width-2 @error('witness_email') is-invalid @enderror bg-transparent"
                                           id="exampleInputEmail1"
                                           placeholder="Enter email" wire:model="witness_email">
                                    @error('witness_email') <span
                                        class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                </div>
                                <div class="form-group">
                                    <label class="mb-sm-0">Contact</label>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>
                                                <input type="text"
                                                       class="form-control form-control-sm  form-control-border border-width-2 @error('witness_contact_1') is-invalid @enderror bg-transparent"
                                                       placeholder="contact 1"
                                                       wire:model="witness_contact_1">
                                            </label>
                                            @error('witness_contact_1') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-2">
                                            <label>
                                                <input type="text"
                                                       class="form-control form-control-sm  form-control-border border-width-2 @error('witness_contact_2') is-invalid @enderror bg-transparent"
                                                       placeholder="contact 2"
                                                       wire:model="witness_contact_2">
                                            </label>
                                            @error('witness_contact_2') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="mb-sm-0">Address</label>
                                    <div class="row">
                                        <div class="col-sm-1">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_no') is-invalid @enderror bg-transparent"
                                                   placeholder="no."
                                                   wire:model="witness_no">
                                            @error('witness_no') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_street_1') is-invalid @enderror bg-transparent"
                                                   placeholder="street1"
                                                   wire:model="witness_street_1">
                                            @error('witness_street_1') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_street_2') is-invalid @enderror bg-transparent"
                                                   placeholder="street2"
                                                   wire:model="witness_street_2">
                                            @error('witness_street_2') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text"
                                                   class="form-control form-control-sm  form-control-border border-width-2 @error('witness_city') is-invalid @enderror bg-transparent"
                                                   placeholder="city"
                                                   wire:model="witness_city">
                                            @error('witness_city') <span
                                                class="error invalid-feedback"><strong>{{$message}}</strong></span> @enderror
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <button wire:click.prevent="ClearWitnessFields()" class="btn btn-sm btn-outline-dark">
                                <i class="fas fa-eraser"></i> Reset
                            </button>
                            <button wire:click.prevent="AddWitness()" class="btn btn-sm btn-primary">
                                <i class="fas fa-user"></i> Add Witness
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            @if(sizeof($arr_witness) !== 0)
                <div>
                    <table class="table table-striped projects">
                        <thead>
                        <tr>
                            <th style="width: 1%">
                                #
                            </th>
                            <th style="width: 30%">
                                Witness Details
                            </th>
                            <th style="width: 20%">
                                Contact Details
                            </th>
                            <th style="width: 15%">
                                Address
                            </th>
                            <th style="width: 8%" class="text-center">
                                NIC
                            </th>
                            <th style="width: 20%">
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($arr_witness as $key=> $witness)

                            <tr>
                                <td>
                                    {{$key+1}}
                                </td>
                                <td>
                                    <a>
                                        {{$witness['f_name']}} {{$witness['m_name']}} {{$witness['l_name']}}
                                    </a>
                                    <br>
                                    <small>
                                        {{$witness['family_name']}}
                                    </small>
                                </td>
                                <td>
                                    <small> {{$witness['contact_1']}} @if($witness['contact_2'] !== null)
                                            / {{$witness['contact_2']}}@endif</small><br>
                                    <small>
                                        {{$witness['email']}}
                                    </small>
                                </td>
                                <td>
                                    <small>{{$witness['no']}}, {{$witness['street_1']}}, </small><br>
                                    <small></small>@if($witness['street_2'] !== null){{$witness['street_2']}}
                                    ,@endif{{$witness['city']}}</small>
                                </td>
                                <td class="project-state">
                                    <small>
                                        {{$witness['nic']}}
                                    </small>
                                </td>
                                <td class="project-actions text-right">
                                    <button class="btn btn-info btn-sm" wire:click="witnessEditArray({{$key}})"><i
                                            class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-danger btn-sm" wire:click="witnessRemoveArray({{$key}})">
                                        <i
                                            class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-primary" wire:click.prevent="insertUpgradeCaseDetails()">Submit
                    </button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="itemUpdate()">Update</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger" wire:click.prevent="itemDelete()">Delete</button>
                </div>
            </div>
        </div>
    </div>
</main>

{{--@push('styles')--}}
{{--    <link rel="stylesheet" href="{{ asset('select2/select2.min.css') }}">--}}
{{--    <link rel="stylesheet" href="{{ asset('select2/select2-bootstrap4.css') }}">--}}
{{--    <!-- file fond css -->--}}
{{--    <link href="{{ asset('vendor/filepond/filepond.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.css') }}" rel="stylesheet">--}}
{{--@endpush--}}

@push('scripts')
{{--    <script src="{{ asset('select2/select2.min.js') }}"></script>--}}
{{--    <!-- filefond js -->--}}
{{--    <script src="{{ asset('vendor/filepond/filepond.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/filepond/filepond.jquery.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/filepond/filepond-plugin-image-preview.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/filepond/filepond-plugin-file-validate-size.min.js') }}"></script>--}}
{{--    <script src="{{ asset('vendor/filepond/filepond-plugin-file-encode.min.js') }}"></script>--}}
    <script>
        $('#selected_law_case_tag').select2({
            theme: 'bootstrap4'
            , width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style'
            , placeholder: 'Please select an user'
        });

        $('#selected_law_case_tag').on('change', function () {
        @this.searched_case_id
            = $(this).val();
            @this.reset_al()
        });
        /**
         * file fond
         */
        $.fn.filepond.registerPlugin(
            // previews dropped images
            // FilePondPluginImagePreview,
            // validates the size of the file
            FilePondPluginFileValidateSize,
            // encodes the file as base64 data
            FilePondPluginFileEncode
        );
        //
        const inputElement = document.querySelector('input[type="file"]');
        // // Create a FilePond instance
        const pond = FilePond.create(inputElement);

        FilePond.setOptions({
            server: {
                url: '../upload/',
                revert: '../upload/',
                headers: {
                    'X-CSRF-TOKEN': '{{csrf_token()}}'
                }
            }
        });

        // this.addEventListener('pondReset', e => {
        //     pond.removeFiles();
        // });

    </script>
@endpush
