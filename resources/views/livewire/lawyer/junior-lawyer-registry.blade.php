@section('header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Junior Lawyer</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                <li class="breadcrumb-item active">Junior Lawyer</li>
            </ol>
        </div><!-- /./col -->
    </div><!-- /row -->
@endsection

<main>

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <input type="text" name="table_search"
                       class="form-control form-control-border border-width-2 bg-transparent"
                       placeholder="Search" wire:model="j_lw_search_key">
            </div>
        </div>
    </div>

    <div class="row">
    </div>

    <div class="card mt-2 card-info mt-3">
        <div class="card-header">
            <h3 class="card-title">Junior Lawyer List</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0" style="max-height: 200px;">
            <table class="table-striped table-sm table">
                <thead style="background-color: rgba(185,202,209,0.62)">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Lawyer License No</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($j_lawyers as $j_lawyer)
                    <tr wire:click="loadJuniorLawyerData({{$j_lawyer}})">
                        <td>{{$j_lawyer->id}}</td>
                        <td>{{$j_lawyer->f_name." ".$j_lawyer->l_name}}</td>
                        <td>{{$j_lawyer->license_no}}</td>
                        <td>{{$j_lawyer->contact_1}}</td>
                        <td>{{$j_lawyer->no}}, {{$j_lawyer->street_1}}, {{$j_lawyer->street_2}}
                            , {{$j_lawyer->city}}</td>
                        @if($j_lawyer->is_active == 1)
                            <td><span class="badge badge-success">Active</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-danger btn-xs "
                                        wire:click="JuniorLawyersDelete({{$j_lawyer->id}})"><i class="fa fa-lock"
                                                                                               aria-hidden="true"></i>
                                </button>
                            </td>

                        @else
                            <td><span class="badge badge-danger">Deactive</span></td>
                            <td>
                                <button type="button" class="btn btn-outline-success btn-xs "
                                        wire:click="JuniorLawyersActive({{$j_lawyer->id}})"><i class="fa fa-lock-open"
                                                                                               aria-hidden="true"></i>
                                </button>
                            </td>
                        @endif


                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <!-- /.card-body -->
    </div>
    <div class="mt-2">
        {{$j_lawyers->links()}}
    </div>

    <div class="card card-secondary">
        <div class="card-header">
            <h3 class="card-title">Junior Lawyer Registry Form</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">First Name<code> *</code></label>
                        <input type="text"
                               class="form-control form-control-sm form-control-border border-width-2"
                               placeholder="Saman" wire:model="fname">
                        @error('fname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Middle Name / Names</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Sampath" wire:model="mname">
                        @error('mname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Last Name</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Werasuriya" wire:model="lname">
                        @error('lname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-sm-6">
                    <div class="form-group">
                        <label class="mb-sm-0">Family Name</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Werasuriya Arachchilage" wire:model="surname">
                        @error('surname') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">Licence Number</label>
                        <input type="text"
                               class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="LW-20220801" wire:model="license_no">
                        @error('license_no') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Lawyer Type </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="lawyer_type">
                            <option></option>
                            @foreach($lawyer_categories as $lawyer_category)
                                <option>{{$lawyer_category['category_name']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label class="mb-sm-0">NIC Number</label>
                        <input type="text" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter NIC" wire:model="nic">
                        @error('nic') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Gender </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="gender">
                            <option></option>
                            <option>Male</option>
                            <option>Female</option>
                            <option>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 ml-4">
                    <div class="form-group">
                        <label class="mb-sm-0">Marriage Status </label>
                        <select class="form-control form-control-sm form-control-border border-width-2"
                                wire:model="marriage_status">
                            <option></option>
                            <option>Single</option>
                            <option>Married</option>
                            <option>Widow</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group">
                        <label class="mb-sm-0">Email</label>
                        <input type="email" class="form-control form-control-sm  form-control-border border-width-2"
                               placeholder="Enter email" wire:model="email">
                        @error('email') <p class="text-danger">{{$message}}</p> @enderror
                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Contact</label>
                        <div class="row">
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 1"
                                           wire:model="contact1">
                                </label>
                                @error('contact1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-2">
                                <label>
                                    <input type="text"
                                           class="form-control form-control-sm  form-control-border border-width-2"
                                           placeholder="contact 2"
                                           wire:model="contact2">
                                </label>
                                @error('contact2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="mb-sm-0">Address</label>
                        <div class="row">
                            <div class="col-sm-1">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="no."
                                       wire:model="no">
                                @error('no') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street1"
                                       wire:model="street1">
                                @error('street1') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="street2"
                                       wire:model="street2">
                                @error('street2') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                            <div class="col-sm-3">
                                <input type="text"
                                       class="form-control form-control-sm  form-control-border border-width-2"
                                       placeholder="city"
                                       wire:model="city">
                                @error('city') <p class="text-danger">{{$message}}</p> @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="mb-sm-0">User Image</label><br>
                        <input type="file" wire:model="image">
                        @if($selected_junior_lawyer_id)
                            <img class="profile-user-img img-fluid" src="{{asset('storage/'.$image)}}"
                                 alt="img-fluid">
                        @else
                            @if ($image)
                                <div>
                                    <img class="profile-user-img img-fluid" src="{{ $image->temporaryUrl() }}"
                                         alt="img-fluid">
                                </div>
                            @endif
                            @error('image') <span class="error">{{ $message }}</span> @enderror
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="card-footer">
                    <button class="btn btn-primary" wire:click.prevent="JuniorLawyerInsert()">Insert</button>
                </div>
                <div class="card-footer">
                    <button class="btn btn-warning" wire:click.prevent="JuniorLawyerUpdate()">Update</button>
                </div>
            </div>
        </div>

    </div>
</main>
