<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLawCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('law_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('case_number')->unique();
            $table->string('court_location');
            $table->date('case_opened_date');
            $table->string('description');
            $table->string('comment');
            $table->boolean('is_active')->default(1);

            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('assigned_lawyer_id');
            $table->unsignedBigInteger('registered_attorney_id');
            $table->unsignedBigInteger('client_id')->nullable();
            $table->unsignedBigInteger('client_company_id')->nullable();

            $table->foreign('category_id')->references('id')->on('law_categories');
            $table->foreign('assigned_lawyer_id')->references('id')->on('junior_lawyers');
            $table->foreign('registered_attorney_id')->references('id')->on('junior_lawyers');
            $table->foreign('client_id')->references('id')->on('customers');
            $table->foreign('client_company_id')->references('id')->on('client_companies');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('law_cases');
    }
}
