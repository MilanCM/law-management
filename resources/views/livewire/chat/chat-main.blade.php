<div class="chat_main_div">
    <div class="chat_container">
        <div class="chat_list_container">
            <div class="chatlist_header">

                <div class="title">
                    Chat
                </div>

                <div class="img_container">
                    <img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff&name={{auth()->user()->name}}"
                         alt="">
                </div>
            </div>

            <div class="chatlist_body mt-2">
                {{--                wire:poll="myFunctionName"--}}
                @if(count($conversations) > 0)
                    @foreach($conversations as $conversation)
                        <div class="chatlist_item"
                             wire:click="loadConversation({{$conversation}},{{$this->getChatUserInstance($conversation,$name='id') }})">
                            <div class="chatlist_img_container">
                                <img
                                    src="https://ui-avatars.com/api/?background=random&name={{$this->getChatUserInstance($conversation,$name='name')}}">
                            </div>
                            <div class="chatlist_info">
                                <div class="top_row">
                                    <div
                                        class="list_username">{{$this->getChatUserInstance($conversation,$name='name')}}</div>
                                    <span class="date">

{{--                                        {{$conversation->messages->last()->created_at->shortAbsoluteDiffForHumans()}}--}}
                                    </span>
                                </div>
                                <div class="bottom_row">
                                    <div class="message_body text-truncate">
                                        <small>
                                            {{--                                            {{$conversation->messages->last()->body}}--}}
                                        </small>
                                    </div>
                                    @if(count($conversation->messages->where('read', 0)->where('receiver_id', auth()->id())))
                                        <div class="unread_count badge rounded-pill text-light">
                                            {{$conversation->messages->where('read', 0)->where('receiver_id', auth()->id())->count()}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="mt-4 ml-4">
                        <label class='text-info align-center'>
                            Yo Have no Conversations
                        </label>
                    </div>
                @endif

                <hr class="bg-info mr-1 ml-1">
                @foreach($users as $user)
                    @if(\App\Models\Conversation::where('receiver_id','=',$user->id)->where('sender_id',auth()->id())
                        ->orWhere('receiver_id','=',auth()->id())->where('sender_id',$user->id)->first() == null)
                        <div class="chatlist_item" wire:click='CheckConversation({{$user->id}})'>
                            <div class="chatlist_img_container">
                                <img src="https://ui-avatars.com/api/?background=random&name={{$user->name}}">
                                {{--                                @if($user->user_role_id == '1')--}}
                                {{--                                    <img--}}
                                {{--                                        src="{{'/storage/'.\App\Models\Lawyer::where('user_id' , $user->id)->value('image')}}"--}}
                                {{--                                        alt="">--}}
                                {{--                                @elseif($user->user_role_id == '2')--}}
                                {{--                                    <img--}}
                                {{--                                        src="{{'/storage/'.\App\Models\JuniorLawyer::where('user_id' , $user->id)->value('image')}}"--}}
                                {{--                                        alt="">--}}
                                {{--                                @elseif($user->user_role_id == '3')--}}
                                {{--                                    <img--}}
                                {{--                                        src="{{'/storage/'.\App\Models\Employee::where('user_id' , $user->id)->value('image')}}"--}}
                                {{--                                        alt="">--}}
                                {{--                                @endif--}}
                            </div>
                            <div class="chatlist_info">
                                <div class="top_row">
                                    <div class="list_username">{{$user->name}} <small>Law</small></div>
                                </div>
                                <div class="bottom_row">
                                    <div class="message_body text-truncate">
                                        <small> Say Hi to {{$user->name}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        @if($selectedConversation)
            <div class="chat_box_container">
                <div class="chatbox_header">
                    <div class="return">
                        <i class="fas fa-arrow-left"></i>
                    </div>
                    <div class="img_container">
                        @if($receiver->user_role_id == '1')
                            <img
                                src="{{'/storage/'.\App\Models\Lawyer::where('user_id' , $receiver->id)->value('image')}}"
                                alt="">
                        @elseif($receiver->user_role_id == '2')
                            <img
                                src="{{'/storage/'.\App\Models\JuniorLawyer::where('user_id' , $receiver->id)->value('image')}}"
                                alt="">
                        @elseif($receiver->user_role_id == '3')
                            <img
                                src="{{'/storage/'.\App\Models\Employee::where('user_id' , $receiver->id)->value('image')}}"
                                alt="">
                        @endif
                    </div>
                    <div class="name">
                        {{$receiver->name}}
                    </div>
                    <div class="info">
                        <div class="info_item">
                            <i class="fas fa-phone"></i>
                        </div>
                        <div class="info_item">
                            <i class="fas fa-image"></i>
                        </div>
                        <div class="info_item">
                            <i class="fas fa-info-circle"></i>
                        </div>
                    </div>
                </div>
                <div class="chatbox_body">
                    @foreach($messages as $message)
                        <div
                            wire:key{{$message->id}} class="msg_body {{auth()->id() == $message->sender_id? 'msg_body_me' : 'msg_body_receiver'}}">
                            @if($message->type)
                                @if($message['type'] === 'jpg' || $message['type'] === 'png' ||$message['type'] === 'jpeg')
                                    <div class="img_container">
                                        <img src="{{asset('/storage/'.$message->media_file)}}" alt="">
                                    </div>
                                @elseif($message['type'] === 'ogg'||$message['type'] === 'mp3'|| $message['type'] === 'm4a')
                                    <audio controls>
                                        <source
                                            src="{{asset('storage/'.$message->media_file)}}">
                                    </audio>
                                @elseif($message['type']=='pptx' ||$message['type']=='pdf' ||$message['type']=='docx' ||$message['type']=='txt')
                                    <iframe src="{{asset('storage/'.$message->media_file)}}" frameborder="0"
                                            style="width:100%;min-height:640px;"></iframe>

                                @endif
                                <div class="msg_body_footer">
                                    @if($message->user->id === auth()->id())
                                        <div class="delete">
                                            <i wire:click.prevent="removeMessage({{$message}})" class="bi bi-trash"></i>
                                        </div>
                                    @endif
                                    <div class="date">
                                        {{$message->created_at->format('h:i a')}}
                                    </div>
                                    <div class="read">
                                        @if($message->user->id === auth()->id())
                                            @if($message->read == 0)
                                                <i class="bi bi-check2 status_tick"></i>
                                            @else
                                                <i class="bi bi-check2-all  text-primary status_tick"></i>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @else
                                {{$message->body}}
                                <div class="msg_body_footer">
                                    @if($message->user->id === auth()->id())
                                        <div class="delete">
                                            <i wire:click.prevent="removeMessage({{$message}})" class="bi bi-trash"></i>
                                        </div>
                                    @endif
                                    <div class="date">
                                        {{$message->created_at->format('h:i a')}}
                                    </div>
                                    <div class="read">
                                        @if($message->user->id === auth()->id())
                                            @if($message->read == 0)
                                                <i class="bi bi-check2 status_tick"></i>
                                            @else
                                                <i class="bi bi-check2-all  text-primary status_tick"></i>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
                <script>
                    $('.chatbox_body').scroll(function () {
                        var top = $('.chatbox_body').scrollTop();
                        if (top == 0) {
                            window.livewire.emit('loadMore');
                        }
                    });
                </script>


                @if($photos)
                    <div class="img_previewer ml-1">
                        <div class="d-flex flex-wrap">
                            @foreach($photos as $photo)
                                <div class="card mr-2 d-flex flex-column mt-2 mb-2 ml-2 justify-content-between">
                                                                        <img style="width: 80px; height: 80px"
                                                                             src="{{$photo->temporaryUrl()}}"
                                                                             alt="...">
                                    {{--                                    @dump($photo->getFilename())--}}
{{--                                    <audio controls style="width: 150px; height: 150px">--}}
{{--                                        <source--}}
{{--                                            src="{{asset('storage/livewire-tmp/'. $photo->getFilename())}}">--}}
{{--                                    </audio>--}}
                                    <a class="badge badge-pill badge-danger text-center"
                                       wire:click.prevent="removeMe({{$loop->index}})">DELETE</a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif

                <form wire:submit.prevent='sendMessage' action="">
                    <div class="chatbox_footer">

                        <div class="custom_form_group row bg-transparent">
                            <input wire:model='body' type="text" id="sendMessage" class="control col-10"
                                   placeholder="Write message">
                            <div class="upload-btn-wrapper col-1">
                                <button class="btn"><i class="bi bi-paperclip"></i></button>
                                <input type="file" class="myfile" wire:model="photos" multiple/>
                            </div>
                            <button type="submit" class="submit col-1"><i class="bi bi-send fa-2x"></i> Send</button>
                        </div>
                    </div>


                </form>
            </div>

        @endif
    </div>


    {{--        <script>--}}
    {{--            // //let el= $('#chatBody');--}}
    {{--            // let el = document.querySelector('#chatBody');--}}
    {{--            // window.addEventListener('scroll', (event) => {--}}
    {{--            //     // handle the scroll event--}}
    {{--            //     alert('aasd');--}}

    {{--            // });--}}
    {{--            $(document).on('scroll','#chatBody',function() {--}}
    {{--                alert('aasd');--}}

    {{--                var top = $('.chatbox_body').scrollTop();--}}
    {{--                if (top == 0) {--}}

    {{--                    window.livewire.emit('loadmore');--}}
    {{--                }--}}


    {{--            });--}}

    {{--        </script>--}}
</div>
@push('styles')
    <link href="{{ asset('css/chat.css') }}" rel="stylesheet">

@endpush
@push('scripts')
    <script>

        window.setInterval(function () {
            // @this.myFunctionName();
            //
            window.livewire.emit('myFunctionName')
            // //     var i = 0;
            // //     alert(i++)
        }, 1000);

        window.addEventListener('rowChatBottom', event => {
            $('.chatbox_body').scrollTop($('.chatbox_body')[0].scrollHeight);
        });
        window.addEventListener('chatSelected', event => {
            if (window.innerWidth < 768) {
                $('.chat_list_container').hide();
                $('.chat_box_container').show();
            }

            $('.chatbox_body').scrollTop($('.chatbox_body')[0].scrollHeight);
            let height = $('.chatbox_body')[0].scrollHeight;
            window.livewire.emit('updateHeight', {
                height: height,
            });
        });

        window.addEventListener('updateHeight', event => {
            let old = event.detail.height;
            let newHeight = $('.chatbox_body')[0].scrollHeight;

            let height = $('.chatbox_body').scrollTop(newHeight - old);
            window.livewire.emit('updateHeight', {height: height,});
        });

        $(window).resize(function () {
            if (window.innerWidth > 768) {
                $('.chat_list_container').show();
                $('.chat_box_container').show();
            }
        });

        $(document).on('click', '.return', function () {
            $('.chat_list_container').show();
            $('.chat_box_container').hide();

            window.livewire.emit('resetChat')
        });

    </script>

@endpush
