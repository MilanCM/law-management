<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JuniorLawyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('junior_lawyers')->insert([
            'f_name' => 'Sagara',
            'm_name' => 'Sampath',
            'l_name' => 'Mahanama',
            'surname' => 'Mahanama Arachchilage',
            'nic' => '978475125V',
            'license_no' => 'JLW2022',
            'contact_1' => '0774875859',
            'contact_2' => '0745241526',
            'email' => 'sagara@gmail.com',
            'no' => '1/22',
            'street_1' => 'main Street',
            'street_2' => 'Second lane',
            'city' => 'Galgamuwa',
            'gender' => 'Male',
            'marriage_status' => 'Single',
            'image' => 'uploads/user/contact image.jpg',
            'is_active' => '1',
//            'user_id' => '1',
            'category_id' => '3',
            'user_role_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('junior_lawyers')->insert([
            'f_name' => 'Kamal',
            'm_name' => 'Sagara',
            'l_name' => 'Gunarathne',
            'surname' => 'Gunarathne Arachchilage',
            'nic' => '978471452V',
            'license_no' => 'JLW202208',
            'contact_1' => '0774456859',
            'contact_2' => '0747485526',
            'email' => 'kamal@gmail.com',
            'no' => '12',
            'street_1' => 'main Street',
            'street_2' => 'first lane',
            'city' => 'Mawanella',
            'gender' => 'Male',
            'marriage_status' => 'Single',
            'image' => 'uploads/user/contact image.jpg',
            'is_active' => '1',
//            'user_id' => '1',
            'category_id' => '2',
            'user_role_id' => '2',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
