<?php

namespace App\Http\Livewire\Appointment;

use Livewire\Component;

class TaskManagement extends Component
{
    public function render()
    {
        return view('livewire.appointment.task-management')->layout('layouts.main');
    }
}
